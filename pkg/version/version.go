package version

import "gitlab.com/suriwatch/suriwatch/pkg/client/model"

var (
	Version = "v1.0"

	BuildMetadata = "unreleased"
	GitCommit     = ""
	GitTreeState  = ""

	BuildDate = ""
)

func GetVersion() *model.VersionInfo {

	return &model.VersionInfo{
		Version:       Version,
		BuildMetadata: BuildMetadata,
		GitCommit:     GitCommit,
		GitTreeState:  GitTreeState,

		BuildDate: BuildDate,
	}
}
