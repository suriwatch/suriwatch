// +build !integration_tests

package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSample(t *testing.T) {

	var config = Config{}
	config.Read("_testcases/config01.toml")

	assert.Equal(t, config.ConfigStorage.Provider, ProviderType(ELASTIC))
	assert.Equal(t, config.ConfigStorage.Elastic.Url, "http://localhost:9200")
	assert.Equal(t, config.DataStorage.Provider, ProviderType(INFLUXDB))
	assert.Equal(t, config.DataStorage.Influxdb.Url, "http://localhost:8086/")

}
