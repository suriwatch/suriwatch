package config

import (
	"bytes"
	"encoding/json"

	"github.com/BurntSushi/toml"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

var config = NewConfig()

/**
 * Constructor for probeDat
 */
func Get() *Config {
	return config

}

/**
 * Constructor for probeDat
 */
func NewConfig() *Config {
	return &Config{
		API: API{
			Port:    8081,
			Enabled: true,
		},
		Executor: Executor{
			Enabled: true,
		},
		HelloService: HelloService{
			Enabled: true,
		},
	}
}

// Represents database server and credentials
type Config struct {
	AppInfo       model.AppInfo
	API           API               `toml:"api"`
	Executor      Executor          `toml:"executor"`
	HelloService  HelloService      `toml:"hello_service"`
	Node          Node              `toml:"node"`
	Selectors     map[string]string `toml:"selectors"`
	ConfigStorage ConfigStorage     `toml:"config"`
	DataStorage   DataStorage       `toml:"data"`
	debug         bool              `toml:"debug"`
}

type ProviderType string

const (
	ELASTIC  ProviderType = "elastic"
	INFLUXDB              = "influxdb"
)

type API struct {
	Port    int  `toml:"port"`
	Enabled bool `toml:"enabled"`
}

type HelloService struct {
	Enabled bool `toml:"enabled"`
}
type Executor struct {
	Enabled bool `toml:"enabled"`
}

type Node struct {
	Name string
	Mode string
}

type ConfigStorage struct {
	Provider ProviderType
	Elastic  Elastic
	Influxdb Influxdb
}

type DataStorage struct {
	Provider ProviderType
	Elastic  Elastic
	Influxdb Influxdb
}

type Elastic struct {
	Url   string
	Index string
	Debug bool
}

type Influxdb struct {
	Url      string
	Database string
}

func (c Config) String() string {
	buffer := new(bytes.Buffer)
	enc := json.NewEncoder(buffer)
	enc.SetIndent("", "\t")
	if err := enc.Encode(c); err != nil {
		panic(err)
	}
	return buffer.String()
}

// Read and parse the configuration file
func (c *Config) Read(filePath string) {
	if _, err := toml.DecodeFile(filePath, &c); err != nil {
		log.Fatal(err)
	}
}
