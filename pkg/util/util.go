package util

import (
	"bytes"
	"encoding/json"
	log "github.com/sirupsen/logrus"
)

func StructToString(obj interface{}) string {
	out, err := json.Marshal(obj)
	if err != nil {
		panic(err)
	}
	return string(out)
}

// Serialise an objet to json
func Serialize(object interface{}) (*bytes.Buffer, error) {

	out, err := json.Marshal(object)
	if err != nil {

		log.Errorf("Fail to marshal request: %s", err)
		return nil, err
	}

	return bytes.NewBufferString(string(out)), nil
}
