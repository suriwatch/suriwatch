package event

import (
	"math"

	"github.com/pkg/errors"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/record"
)

type ResultEventDetector struct {
	resultChan      chan *model.ProbeResult
	statusEventChan chan *model.StatusChangeEvent
	eventRecorder   *record.EventRecorder
	lastStatus      map[model.ProbeId]model.ProbeResult
	lastEvent       map[model.ProbeId]model.StatusChangeEvent

	eventDao dao.EventDAO
	probeDao dao.ProbeDAO
}

/**
 * Constructor for probe
 */
func NewResultEventDetector(provider *dao.Provider) *ResultEventDetector {

	// Initialise provider

	return &ResultEventDetector{
		eventRecorder:   record.NewEventRecorder(provider),
		resultChan:      make(chan *model.ProbeResult, 100),
		statusEventChan: make(chan *model.StatusChangeEvent, 100),
		lastStatus:      make(map[model.ProbeId]model.ProbeResult),
		lastEvent:       make(map[model.ProbeId]model.StatusChangeEvent),
		eventDao:        provider.GetEventDao(),
		probeDao:        provider.GetProbeDao(),
	}
}

/**
 * Execute a probe and give the result.
 */
func (m *ResultEventDetector) Verify(entity model.ProbeResult) {

	m.resultChan <- &entity
}

/**
 * Execute a probe and give the result.
 */
func (m *ResultEventDetector) Start() {

	// start the event recorder
	m.eventRecorder.Start()

	// launch change detection process
	go m.process()
}

// Detect new status change event
func (m *ResultEventDetector) process() {

	for res := range m.resultChan {

		if prev, found := m.lastStatus[res.ProbeId]; found {
			if prev.Status != res.Status {
				log.Infof("Probe %s StatusChange from %s to %s ", string(res.ProbeId), string(prev.Status), string(res.Status))

				// Close previous event
				m.closeEventPeriod(res)

				// Start current period event
				m.startEventPeriod(res)

			}
		} else {

			lastEvent, err := m.getLastEvent(res.ProbeId)

			if err != nil {
				log.Error("Fail to get last event", err)

			} else {

				// Is an event still opened ? should we use it ?
				if lastEvent != nil && lastEvent.End == nil {

					log.Infof("Event probe RECOVERY  : %s", string(res.ProbeId))

					if string(lastEvent.Status) != string(res.Status) {

						log.Infof("Probe %s StatusChange from %s to %s ", string(res.ProbeId), string(prev.Status), string(res.Status))

						// Close previous event
						m.closeEventPeriod(res)

						// Start current period event
						m.startEventPeriod(res)
					}
				} else {
					log.Infof("Event %s", lastEvent)

					if lastEvent == nil {
						log.Infof("No Event found to recover for %s. Starting a new one", string(res.ProbeId))
					} else {
						log.Infof("No Event opened found to recover for %s. Starting a new one", string(res.ProbeId))
					}

					// Start new event
					m.startEventPeriod(res)
				}
			}

		}
		m.lastStatus[res.ProbeId] = *res
	}
}

/**
 * Give the last probe event
 */
func (m *ResultEventDetector) getLastEvent(probeId model.ProbeId) (*model.StatusChangeEvent, error) {

	// if is in cache
	if event, found := m.lastEvent[probeId]; found {
		return &event, nil
	} else {

		probe, err := m.probeDao.GetByID(probeId)
		if err != nil {
			log.Errorf("Fail to get probe by Id [%s]", probeId)
			return nil, errors.Wrap(err, "Fail to probe for last event")
		}

		size := 1
		query := model.SearchEventQuery{}

		if probe.Mode == model.AGENT {
			nodeName := config.Get().Node.Name
			query.Node = &nodeName
		}

		mode := probe.Mode
		query.Mode = &mode

		query.ProbeId = &probeId
		query.Size = &size
		query.Sort = []model.SortValue{{
			Name:  "start",
			Order: model.DESC,
		}}

		events, err := m.eventDao.Search(query)
		if err != nil {
			return nil, err
		}

		if len(events) > 0 {
			return events[0], nil
		} else {
			return nil, nil
		}
	}

}

func (m *ResultEventDetector) startEventPeriod(result *model.ProbeResult) {

	event := model.StatusChangeEvent{}
	event.Id = xid.New().String()
	event.ProbeId = result.ProbeId
	event.Start = result.Date
	event.Reason = &result.Infos
	event.Mode = string(result.Mode)
	event.Node = config.Get().Node.Name
	event.Labels = result.Labels
	status := string(result.Status) // last status
	event.Status = model.EventStatus(status)

	m.lastEvent[result.ProbeId] = event
	m.eventRecorder.Record(event)

}

func (m *ResultEventDetector) closeEventPeriod(result *model.ProbeResult) error {

	log.Debugf("Starting closes status for %s ", result.ProbeId)

	event, _ := m.getLastEvent(result.ProbeId)
	if event != nil {

		event.End = &result.Date

		var duration = int(math.Round(event.End.Sub(event.Start).Seconds()))
		event.Duration = &duration
		m.eventRecorder.Record(*event)

	} else {
		log.Errorf("Enable to find event to close for %s ", result.ProbeId)
	}
	return nil
}

/**
 * Stop the recorder
 */
func (m *ResultEventDetector) Stop() {

	m.eventRecorder.Stop()
	//TODO
	// 	close(m.recordChan)
}
