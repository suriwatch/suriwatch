package executor

import (
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

/**

Documentation :
****************

https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts/
http://tleyden.github.io/blog/2016/11/21/tuning-the-go-http-client-library-for-load-testing/
https://medium.com/@deeeet/trancing-http-request-latency-in-golang-65b2463f548c
https://golang.org/src/net/http/transport.go

*/

type HTTPStatusProbeExecutor struct {
}

func (m *HTTPStatusProbeExecutor) Execute(probe Probe) ProbeResult {

	log.Infof("Executing http status probe [%s]", probe.Name)

	var probeRes = m.initResult(probe)

	return probeRes

}

func (m *HTTPStatusProbeExecutor) initHTTPRequest(probe Probe) (*http.Request, error) {

	// Body initialisation
	var body io.Reader = nil
	var request = probe.HttpProbeConfig.Request

	if request.Content != "" {

		body = strings.NewReader(request.Content)
	} else {
		log.Warningf("No Request body for : %s", probe.Name)
	}

	req, err := http.NewRequest(request.Verb, request.URL, body)
	req.Close = true

	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", "Suriwatch/1.0")

	// Setting headers
	for _, header := range request.Headers {
		if header.Name == "Host" {
			req.Host = header.Value
		}
		req.Header.Add(header.Name, header.Value)
	}
	return req, nil

}

func (m *HTTPStatusProbeExecutor) initResult(probe Probe) ProbeResult {
	probeRes := ProbeResult{
		ProbeId: probe.Id,
		Name:    probe.Name,
		Date:    time.Now(),
		Infos:   "",
		Type:    probe.Type,
		Labels:  probe.Labels,
		Mode:    probe.Mode,
	}
	probeRes.Node = config.Get().Node.Name
	probeRes.Host, _ = os.Hostname()

	// get the target host
	url, _ := url.Parse(probe.HttpProbeConfig.Request.URL)
	if url != nil {
		probeRes.Target = url.Host
	}

	return probeRes
}

/**
 * Validate that response body match with received response
 */
func (m *HTTPStatusProbeExecutor) checkBody(expectedResponse HTTPResponse, response string) bool {

	if expectedResponse.Keyword != nil && *expectedResponse.Keyword != "" {
		return strings.Contains(response, *expectedResponse.Keyword)
	} else {
		return true
	}
}
