package executor

import (
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/tcnksm/go-httpstat"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

/**

Documentation :
****************

https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts/
http://tleyden.github.io/blog/2016/11/21/tuning-the-go-http-client-library-for-load-testing/
https://medium.com/@deeeet/trancing-http-request-latency-in-golang-65b2463f548c
https://golang.org/src/net/http/transport.go

*/

type HTTPProbeExecutor struct {
}

func (m *HTTPProbeExecutor) Execute(probe Probe) ProbeResult {

	log.Infof("Executing http probe [%s]", probe.Name)

	var probeRes = m.initResult(probe)

	tr := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   10 * time.Second,
			KeepAlive: 10 * time.Second,
			DualStack: true,
		}).DialContext,
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	defer tr.CloseIdleConnections()

	// Setting default timeout
	var requestTimeout = 30

	var request = probe.HttpProbeConfig.Request
	var expectedResponse = probe.HttpProbeConfig.Response

	if request.Timeout == 0 {
		requestTimeout = request.Timeout
	}

	httpClient := http.Client{
		Timeout:   time.Duration(requestTimeout) * time.Second,
		Transport: tr,
	}

	req, err := m.initHTTPRequest(probe)
	if err != nil {
		log.Errorf("Fail to create http request : %s", err)
		probeRes.Infos = err.Error()
		probeRes.Status = KO
		return probeRes
	}

	// Create a httpstat powered context
	var result httpstat.Result
	ctx := httpstat.WithHTTPStat(req.Context(), &result)
	req = req.WithContext(ctx)

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Errorf("Fail to execute http request : %s", getErr)
		probeRes.Infos = getErr.Error()
		probeRes.Status = KO
		return probeRes
	}
	defer res.Body.Close()

	//end := time.Now()

	// Show the results
	probeRes.HTTPStats = HTTPStats{
		DNSLookup:        int(result.DNSLookup / time.Millisecond),
		TCPConnection:    int(result.TCPConnection / time.Millisecond),
		TLSHandshake:     int(result.TLSHandshake / time.Millisecond),
		ServerProcessing: int(result.ServerProcessing / time.Millisecond),
		ContentTransfert: int(result.ContentTransfer(time.Now()) / time.Millisecond),
		Total:            int(result.Total(time.Now()) / time.Millisecond),
	}

	probeRes.Duration = int(result.Total(time.Now()) / time.Millisecond)

	bodyBytes, _ := ioutil.ReadAll(res.Body)

	if m.checkStatusCode(expectedResponse, res.StatusCode) {

		if m.checkBodyRequired(expectedResponse) {
			if m.checkBody(expectedResponse, string(bodyBytes)) {
				probeRes.Status = OK
				probeRes.Infos = fmt.Sprintf("Expected Keyword [%s] found.", expectedResponse.Keyword)
			} else {
				probeRes.Status = KO
				message := string(bodyBytes)
				probeRes.Message = &message // remember the response payload for analysis
				probeRes.Infos = fmt.Sprintf("Expected Keyword [%s] not found.", expectedResponse.Keyword)
				log.Warningf("Probe [%s] fail with keyword [%s] not found", probe.Name, expectedResponse.Keyword)
			}
		} else {
			probeRes.Status = OK
			probeRes.Infos = fmt.Sprintf("Valid HTTP code [%d] received.", res.StatusCode)
		}

	} else {
		probeRes.Status = KO
		message := string(bodyBytes)
		probeRes.Message = &message // remember the response payload for analysis
		probeRes.Infos = "Invalid status code " + strconv.Itoa(res.StatusCode)

		log.Warningf("Probe [%s] fail with status code : [%s]", probe.Name, strconv.Itoa(res.StatusCode))
		log.Infof("Response body : [%s]", string(bodyBytes))
	}

	return probeRes

}

func (m *HTTPProbeExecutor) initHTTPRequest(probe Probe) (*http.Request, error) {

	// Body initialisation
	var body io.Reader = nil
	var request = probe.HttpProbeConfig.Request

	if request.Content != "" {

		body = strings.NewReader(request.Content)
	} else {
		log.Warningf("No Request body for : %s", probe.Name)
	}

	req, err := http.NewRequest(request.Verb, request.URL, body)
	req.Close = true

	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", "Suriwatch/1.0")

	// Setting headers
	for _, header := range request.Headers {
		if header.Name == "Host" {
			req.Host = header.Value
		}
		req.Header.Add(header.Name, header.Value)
	}

	return req, nil

}

func (m *HTTPProbeExecutor) initResult(probe Probe) ProbeResult {
	probeRes := ProbeResult{
		ProbeId: probe.Id,
		Name:    probe.Name,
		Date:    time.Now(),
		Infos:   "",
		Type:    probe.Type,
		Labels:  probe.Labels,
		Mode:    probe.Mode,
	}
	probeRes.Node = config.Get().Node.Name
	probeRes.Host, _ = os.Hostname()

	// get the target host
	url, _ := url.Parse(probe.HttpProbeConfig.Request.URL)
	if url != nil {
		probeRes.Target = url.Host
	}

	return probeRes
}

func (m *HTTPProbeExecutor) checkStatusCode(expectedResponse HTTPResponse, receive int) bool {

	if expectedResponse.HttpCode != nil {
		return receive == *expectedResponse.HttpCode
	} else {
		return receive >= 200 && receive < 300
	}
}

/**
 * Validate that response body match with received response
 */
func (m *HTTPProbeExecutor) checkBody(expectedResponse HTTPResponse, response string) bool {

	if expectedResponse.Keyword != nil {
		return strings.Contains(response, *expectedResponse.Keyword)
	} else {
		return true
	}
}

/*
*
 * Validate that response body match with received response
*/
func (m *HTTPProbeExecutor) checkBodyRequired(expectedResponse HTTPResponse) bool {

	return expectedResponse.Keyword != nil && *expectedResponse.Keyword != ""
}
