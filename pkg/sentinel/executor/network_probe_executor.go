package executor

import (
	"fmt"

	"net"
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type NetworkProbeExecutor struct {
}

func (m *NetworkProbeExecutor) Execute(probe Probe) ProbeResult {

	log.Infof("Executing network probe [%s]", probe.Name)
	var res = m.initResult(probe)

	var request = probe.NetworkProbeConfig.Request

	var target = request.Target + ":" + strconv.Itoa(request.Port)
	var protocol = request.Protocol

	// Setting default protocol if not set
	if request.Protocol == "" {
		protocol = "tcp"
	}

	// Setting default timeout
	var requestTimeout = 10
	if request.Timeout == 0 {
		requestTimeout = request.Timeout
	}

	ln, err := net.DialTimeout(protocol, target, time.Duration(requestTimeout)*time.Second)

	if err != nil {
		res.Infos = fmt.Sprintf("Can't listen on port %d: %s\n", request.Port, err)
		res.Status = KO
		return res
	}

	err = ln.Close()
	if err != nil {
		res.Infos = fmt.Sprintf("Couldn't stop listening on port %d: %s\n", request.Port, err)
		res.Status = KO
		return res
	}

	res.Status = OK

	return res

}

func (m *NetworkProbeExecutor) initResult(probe Probe) ProbeResult {
	probeRes := ProbeResult{
		ProbeId: probe.Id,
		Name:    probe.Name,
		Date:    time.Now(),
		Type:    probe.Type,
		Labels:  probe.Labels,
	}
	probeRes.Node = config.Get().Node.Name
	probeRes.Host, _ = os.Hostname()

	return probeRes
}
