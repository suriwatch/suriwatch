package tsdb_executor

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/condition"

	_ "gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/tsdb/prometheus"
	"os"
	"time"
)


func New(provider *dao.Provider) TSDBProbeExecutor{

	return TSDBProbeExecutor{
		provider: provider,
	}

}
type TSDBProbeExecutor struct {

	provider *dao.Provider

}

func (m *TSDBProbeExecutor) Execute(probe model.Probe) model.ProbeResult {

	logrus.Infof("Executing tsdb probe [%s]", probe.Name)
	var res = m.initResult(probe)


	var alertEvaluator = condition.New(m.provider.GetDataSourceDao())

	resp, err := alertEvaluator.Eval(probe.TSDBProbeConfig.Condition)
	if err != nil {
		var errorMsg = err.Error()
		res.Message = &errorMsg
		//res.Status = model.FAIL
		res.Status = model.KO
		logrus.WithError(err).Errorf("Fail to evaluate expression")
		return res
	}

	if resp.Success {
		res.Message = &resp.Message
		res.Status = model.OK
	}else{
		res.Message = &resp.Message
		res.Status = model.KO
	}

	return res

}


func (m *TSDBProbeExecutor) initResult(probe model.Probe) model.ProbeResult {
	probeRes := model.ProbeResult{
		ProbeId: probe.Id,
		Name:    probe.Name,
		Date:    time.Now(),
		Type:    probe.Type,
		Labels:  probe.Labels,
	}
	probeRes.Node = config.Get().Node.Name
	probeRes.Host, _ = os.Hostname()

	return probeRes
}
