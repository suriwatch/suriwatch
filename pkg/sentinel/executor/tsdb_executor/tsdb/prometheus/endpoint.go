package prometheus

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/api"
	apiv1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	"github.com/sirupsen/logrus"
	swmodel "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/tsdb"
)

func init() {
	logrus.Infof("Registering prometheus")
	tsdb.RegisterQueryEndpoint("prometheus", NewEndpoint)
}

//
// Endpoint for prometheus
//
type Endpoint struct {

	// Prometheus datasource config
	dsInfo *swmodel.DataSource
}

// Provide a new prometheus  endpoint
func NewEndpoint(dsInfo *swmodel.DataSource) (tsdb.QueryEndpoint, error) {

	var ep = &Endpoint{
		dsInfo: dsInfo,
	}

	return ep, nil
}

//
// Execute elastic query
//
func (ep *Endpoint) Query(ctx tsdb.Context, query *tsdb.TsdbQuery) (*tsdb.Response, error) {

	client, err := ep.getClient()
	if err != nil {
		fmt.Print(err)
	}

	var ctxt = context.Background()

	var timeRange = apiv1.Range{
		Start: query.TimeRange.Start,
		End:   query.TimeRange.End,
		Step:  query.TimeRange.Step,
	}
	value, warn, err := client.QueryRange(ctxt, query.Queries[0].Raw, timeRange)

	if warn != nil {
		logrus.Warn(warn)
	}


	var resp = &tsdb.Response{
		Results: make(map[string]*tsdb.QueryResult, 0),
	}

	resp.Results["default"], err =  ep.mapResults(value)
	if err != nil {
		logrus.WithError(err).Error("Fail to map request results")
	}

	return resp, nil

}

func (ep *Endpoint) getName(metric model.Metric) string{
	return metric.String()
}

//
// Map prometheus result to suriwatch result
//
func (ep *Endpoint) mapResults(value model.Value) (*tsdb.QueryResult, error) {

	queryRes := tsdb.NewQueryResult()

	data, ok := value.(model.Matrix)
	if !ok {
		if value != nil {
			return queryRes, errors.New("Unsupported result format: nil")
		}
		return queryRes, fmt.Errorf("Unsupported result format: %s", value.Type().String())
	}

	for _, v := range data {
		series := tsdb.TimeSeries{
			Name: ep.getName(v.Metric),
			Tags: map[string]string{},
		}

		for k, v := range v.Metric {
			series.Tags[string(k)] = string(v)
		}

		for _, k := range v.Values {
			series.Points = append(series.Points, tsdb.NewTimePoint( tsdb.Time(k.Timestamp.Unix()*1000), tsdb.Value(k.Value)))
		}

		queryRes.Series = append(queryRes.Series, &series)
	}

	return queryRes, nil

}


//
// getClient give a prometheus client for the endpoint database
//
func (pe *Endpoint) getClient() (apiv1.API, error) {

	var cfg = api.Config{
		Address: pe.dsInfo.URL,
	}

	client, err := api.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	return apiv1.NewAPI(client), nil
}
