package prometheus

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/tsdb"
	"testing"
	"time"
)

func TestPrometheus(t *testing.T) {

	ds := &tsdb.DataSource{
		Name: "prometheus-infra",
		Type: "prometheus",
		URL:  "https://prometheus.demo",
	}

	query := &tsdb.Query{
		Raw: "kube_pod_container_status_restarts_total{ cluster=\"pr\", namespace=\"namepsapce-demo\", container=\"hello-world\" }",
	}

	var period = time.Minute * 30

	var timeRange = &tsdb.TimeRange{
		Start: time.Now().Add(-period),
		End:   time.Now(),
		Step:  time.Second * 30,
	}

	tsbQuery := &tsdb.TsdbQuery{
		TimeRange: timeRange,
		Queries:   []*tsdb.Query{query},
	}

	endpoint, err := tsdb.GetQueryEndpoint(ds)
	if err != nil {
		logrus.
			WithError(err).
			Error("Fail to obtain endpoint")
		t.Fail()
	}

	var ctxt = tsdb.Context{
		Context: context.Background(),
	}

	response, err:= endpoint.Query(ctxt, tsbQuery)
	if err != nil {
		logrus.
			WithError(err).
			Error("Fail to execute prometheus query")
		t.Fail()
	}

	logrus.Debug(response.Results)

}
