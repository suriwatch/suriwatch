package elastic

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/tsdb"
)

func init() {
	logrus.Infof("Registering elastic tsdb")
	tsdb.RegisterQueryEndpoint("elastic6", NewEndpoint)
}

// Provide a new elastic endpoint
func NewEndpoint(dsInfo *tsdb.DataSource) (tsdb.QueryEndpoint, error) {

	return nil, nil
}

//
// Endpoint for elasticsearch
//
type Endpoint struct {
}

//
// Execute elastic query
//
func (ep *Endpoint) Query(ctx tsdb.Context, ds *tsdb.DataSource, query *tsdb.TsdbQuery) (*tsdb.Response, error) {

	return nil, nil

}
