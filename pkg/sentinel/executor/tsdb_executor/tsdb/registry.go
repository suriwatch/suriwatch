package tsdb

import (
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

func init() {
	logrus.Infof("Initializing tsdb registry")
	registry = make(map[string]GetTsdbQueryEndpointFn)
}

type GetTsdbQueryEndpointFn func(ds *model.DataSource) (QueryEndpoint, error)

// Endpoint for a time serie database
type QueryEndpoint interface {
	Query(ctx Context, query *TsdbQuery) (*Response, error)
}

var registry map[string]GetTsdbQueryEndpointFn

func RegisterQueryEndpoint(endpointId string, fn GetTsdbQueryEndpointFn) {
	registry[endpointId] = fn
}

func GetQueryEndpoint(ds *model.DataSource) (QueryEndpoint, error) {

	fn := registry[ds.Type]
	if fn == nil {
		return nil, errors.New(fmt.Sprintf("Can't resolve endpoint %s", ds.Type))
	}
	return fn(ds)

}
