package tsdb

import (
	"context"
	"fmt"
	"time"
)

type Context struct {
	context.Context
}

type TsdbQuery struct {
	TimeRange *TimeRange
	Queries   []*Query
}


// TSDB Response query
type Response struct {
	Results map[string]*QueryResult
}


type TimeRange struct {
	Start time.Time
	End   time.Time
	Step  time.Duration
}

func (tr TimeRange) String() string {
	return fmt.Sprintf("Start : %s, End : %s, Step : %s", tr.Start, tr.End, tr.Step)
}

type Query struct {
	Name string
	Raw  string
}

type QueryResult struct {
	Name string
	Series []*TimeSeries
}

type Time int64
type Value float64




type TimePoint struct {
	Time  Time
	Value Value
}


func NewQueryResult() *QueryResult{
	return &QueryResult{}
}

func NewTimePoint(time Time, value Value) *TimePoint{

	return &TimePoint{
		Time: time,
		Value: value,
	}
}

type TimeSeries struct {
	Name   string
	Points []*TimePoint
	Tags   map[string]string
}


