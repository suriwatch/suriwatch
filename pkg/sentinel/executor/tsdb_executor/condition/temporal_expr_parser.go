package condition

import (
	"errors"
	"regexp"
	"strconv"
	"time"
)

type TemporalExprParser struct{ }

type DurationExpr struct {
	operator string
	value int
	unit string
}


func (de DurationExpr) toDuration() time.Duration {

	var res time.Duration = 0

	switch de.unit {
	case "m":
		res = time.Minute
	case "s":
		res = time.Second
	}

	res = time.Duration(de.value) * time.Second

	if de.operator== "-"{
		res = -res
	}
	return res

}

type DateExpr struct {
	dateExp string
	duration *DurationExpr
}


func (de DateExpr) toTime() time.Time {

	var res = time.Now()

	if de.duration != nil {
		res = res.Add(de.duration.toDuration())
	}
	return res

}



func (tp *TemporalExprParser) ParseDate(expr string) (*DateExpr, error){

	var err error = nil
	var myExp = regexp.MustCompile(`now\s*((?P<operator>[-+])\s*(?P<duration>\d+)(?P<unit>[m,s]))?`)

	if !myExp.MatchString(expr) {
		return nil, errors.New("Invalid date expr")
	}

	match := myExp.FindStringSubmatch(expr)
	var durationUsed = false

	time := &DateExpr{
		dateExp: "now",
		duration: nil,
	}

	duration := &DurationExpr{
		operator: "+",
	}

	for i, name := range myExp.SubexpNames() {
		if i != 0 && name != "" {
			switch name {
			case "operator":
				if  match[i] != ""{
					durationUsed = true
					duration.operator = match[i]
				}
			case "duration":
				if  match[i] != ""{
					durationUsed = true
					duration.value, err = strconv.Atoi(match[i])
					if err != nil {
						return nil, err
					}
				}
			case "unit":
				if  match[i] != ""{
					durationUsed = true
					duration.unit = match[i]
				}
			}
		}
	}

	if durationUsed {
		time.duration = duration
	}
	return time, nil
}



func (tc *TemporalExprParser) ParseDuration(expr string) (*DurationExpr, error){

	var err error = nil
	var myExp = regexp.MustCompile(`(?P<operator>[-+])?(\s)*(?P<duration>\d+)(?P<unit>[m,s])`)

	match := myExp.FindStringSubmatch(expr)

	duration := &DurationExpr{
		operator: "+",
	}

	for i, name := range myExp.SubexpNames() {
		if i != 0 && name != "" {
			if i<0 || i>= len(match){
				return nil, errors.New("")
			}

			switch name {
			case "operator":
				duration.operator = match[i]
			case "duration":
				duration.value, err = strconv.Atoi(match[i])
				if err != nil {
					return nil, err
				}
			case "unit":
				duration.unit = match[i]
			}
		}
	}

	return duration, nil
}