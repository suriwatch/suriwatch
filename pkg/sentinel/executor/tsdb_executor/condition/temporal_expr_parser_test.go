package condition

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTimeEvaluator01(t *testing.T) {


	timeParser := TemporalExprParser{}

	dateExp, err := timeParser.ParseDate("now-5m")
	if err != nil {
		assert.Fail(t,"Fail to parse expression")
	}

	assert.Equal(t, dateExp.duration.operator, "-", "The operation is not valid.")
	assert.Equal(t, dateExp.duration.value,  5, "The value is not valid.")
	assert.Equal(t, dateExp.duration.unit, "m", "The unit is not valid.")
}

func TestTimeEvaluator02(t *testing.T) {


	timeParser := TemporalExprParser{}

	dateExp, err := timeParser.ParseDate("now")
	if err != nil {
		assert.Fail(t,"Fail to parse expression")
	}

	assert.Equal(t, dateExp.duration, nil, "The operation is not valid.")
}

func TestDurationEvaluator(t *testing.T) {


	timeParser := TemporalExprParser{}

	duration, err := timeParser.ParseDuration("-3m")
	if err != nil {
		assert.Fail(t,"Fail to parse expression")
	}

	assert.Equal(t, duration.operator, "-", "The operation is not valid.")
	assert.Equal(t, duration.value,  3, "The value is not valid.")
	assert.Equal(t, duration.unit, "m", "The unit is not valid.")
}