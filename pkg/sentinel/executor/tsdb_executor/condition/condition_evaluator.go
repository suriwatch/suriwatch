package condition

import (
	"context"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/tsdb"
	"time"
)


func New(dataSourceDAO dao.DataSourceDAO) *ConditionEvaluator{

	return &ConditionEvaluator{
		expParser: &TemporalExprParser{},
		dataSourceDao: dataSourceDAO,
	}
}
type ConditionEvaluator struct{

	expParser *TemporalExprParser
	dataSourceDao dao.DataSourceDAO
}

type EvalResponse struct{

	Success bool
	Message string
}


func (ae ConditionEvaluator) getDataSourceByName(name string) (*model.DataSource, error) {

	query := model.SearchDataSourceQuery{
		Name: name,
	}

	dataSources, err := ae.dataSourceDao.Search(query)
	if err != nil {
		return nil, err
	}

	if len(dataSources) <= 0 {
		return nil, errors.New(fmt.Sprintf("No datasouce whith name %s found", name))
	}
	if len(dataSources) > 1 {
		return nil,  errors.New(fmt.Sprintf("Illegal state. Multiple dataSources whith name %s found", name))
	}

	return &(dataSources[0]), nil
}

func (ae ConditionEvaluator) evalTime(dateExp string)  (time.Time, error){

	var time time.Time

	d, err  := ae.expParser.ParseDate(dateExp)
	if err != nil {
		return time, err
	}

	time =  d.toTime()
	return time, nil
}

func (ae ConditionEvaluator) evalDuration(durationExp string) (time.Duration, error) {

	var duration time.Duration

	d, err  := ae.expParser.ParseDuration(durationExp)
	if err != nil {
		return duration, err
	}

	duration =  d.toDuration()
	return duration, nil
}


func (ae ConditionEvaluator) Eval(condition model.Condition) (*EvalResponse, error){

	// step 01 : execute tsdb query
	result, err := ae.execQuery(condition.Query)
	if err != nil {
		logrus.WithError(err).Error("Fail to execute query")
		return nil, err
	}

	// step 02 : reducing query results
	value, err := ae.execReduce(condition.Reduce, result)
	if err != nil {
		logrus.WithError(err).Error("Fail to reduce values")
		return nil, err
	}

	// step 03 : evaluate
	resp, err :=  ae.execEval(condition.Evaluate, value)

	return resp, err

}


func (ae ConditionEvaluator) execReduce(reduce *model.Reduce, response *tsdb.Response) (*tsdb.Value, error){

	reducer, err := NewReduce(reduce.Type)
	if err != nil {
		return nil, err
	}
	return reducer.Reduce(response), nil
}

func (ae ConditionEvaluator) execEval(evaluate *model.Evaluate, value *tsdb.Value) (*EvalResponse, error){

	evaluator, err := NewEvaluator(evaluate.Type)
	if err != nil {
		return nil, err
	}
	return evaluator.Evaluate(evaluate, value), nil
}


func (ae ConditionEvaluator) getTimeRangeFromQuery(query *model.Query) (*tsdb.TimeRange, error){

	start, err := ae.evalTime(query.From)
	if err != nil{
		return nil, errors.New(fmt.Sprintf("Invalid from date format : %s", query.From))
	}
	end, err := ae.evalTime(query.To)
	if err != nil{
		return nil, errors.New(fmt.Sprintf("Invalid to date format : %s", query.To))
	}
	step, err := ae.evalDuration(query.Step)
	if err != nil{
		return nil, errors.New(fmt.Sprintf("Invalid to duration format : %s", query.To))
	}

	return 	&tsdb.TimeRange{
		Start: start,
		End:   end,
		Step:  step,
	}, nil

}

func (ae ConditionEvaluator) execQuery(query *model.Query) (*tsdb.Response, error) {

	var timeRange, err = ae.getTimeRangeFromQuery(query)
	if err != nil{
		return nil, err
	}

	// build query
	tsbQuery := &tsdb.TsdbQuery{
		TimeRange: timeRange,
		Queries:   []*tsdb.Query{
			{ Name: "req01",  Raw: query.Raw, },
		},
	}

	ds, err := ae.getDataSourceByName(query.DataSource)
	if err != nil {
		return nil, err
	}

	endpoint, err := tsdb.GetQueryEndpoint(ds)
	if err != nil {
		logrus.
			WithError(err).
			Error("Fail to obtain endpoint")
		return nil, err
	}

	var ctxt = tsdb.Context{
		Context: context.Background(),
	}

	logrus.
		WithField("query", tsbQuery.Queries[0].Raw).
		WithField("period", tsbQuery.TimeRange).
		Debug("Executing query")

	response, err:= endpoint.Query(ctxt, tsbQuery)
	if err != nil {
		logrus.
			WithError(err).
			Error("Fail to execute prometheus query")
		return nil, err
	}

	return response, nil

}