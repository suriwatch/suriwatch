package condition

import (
	"errors"
	"fmt"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	tsdb "gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/tsdb"
)



type Reducer interface {

	Reduce(result *tsdb.Response) *tsdb.Value
}

// Reduce a response to a single max value
type MaxReducer struct {}

func (mr *MaxReducer) Reduce(response *tsdb.Response) *tsdb.Value{

	if response == nil {
		return nil
	}
	var value *tsdb.Value

	// for now, only support first response, first serie
	for _, qr := range response.Results {
		for _, s := range qr.Series {
			return mr.ReduceSerie(s)
		}
	}
	return value
}

func (mr *MaxReducer) ReduceSerie(serie *tsdb.TimeSeries) *tsdb.Value{

	var value *tsdb.Value
	for _, p := range serie.Points {
		if value == nil {
			value = &p.Value
		}else if *value < p.Value {
			value = &p.Value
		}
	}
	return value
}


type MinReducer struct {}

func (mr *MinReducer) Reduce(response *tsdb.Response) *tsdb.Value{

	var value *tsdb.Value
	for _, qr := range response.Results {

		for _, s := range qr.Series {
			return mr.ReduceSerie(s)
		}
	}
	return value
}

func (mr *MinReducer) ReduceSerie(serie *tsdb.TimeSeries) *tsdb.Value{

	var value *tsdb.Value
	for _, p := range serie.Points {
		if value == nil {
			value = &p.Value
		}else if *value > p.Value {
			value = &p.Value
		}
	}
	return value
}


type DiffReducer struct {}

func (mr *DiffReducer) Reduce(response *tsdb.Response) *tsdb.Value{

	var value *tsdb.Value
	for _, qr := range response.Results {

		for _, s := range qr.Series {
			for _, p := range s.Points {
				if value == nil {
					value = &p.Value
				}else if *value > p.Value {
					value = &p.Value
				}
			}
		}
	}
	return value
}


func (mr *DiffReducer) ReduceSerie(serie *tsdb.TimeSeries) *tsdb.Value{

	var value *tsdb.Value
	var nbPoints = len(serie.Points)

	if nbPoints >= 2 {
		firstValue := serie.Points[0].Value
		lastValue := serie.Points[nbPoints-1].Value

		val := lastValue - firstValue
		value =  &val

	}
	return value
}


func NewReduce(typ model.ReduceType) (Reducer, error){

	switch typ {
	case "max":
		reducer := &MaxReducer{}
		return reducer, nil
	case "min":
		reducer := &MinReducer{}
		return reducer, nil
	case "diff":
		reducer := &DiffReducer{}
		return reducer, nil
	}
	return nil, errors.New(fmt.Sprintf("Unkown reducer type %s", typ))
}

