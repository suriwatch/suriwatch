package condition

import (
	"github.com/sirupsen/logrus"
	"testing"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)


func TestEvaluator(t *testing.T) {


	var evaluateConfig = make(map[string]interface{})
	evaluateConfig["value"] = -30.0

	var condition = model.Condition{
			Query: &model.Query{
				DataSource : "prometheus-infra",
				From: "now-15m",
				To: "now",
				Step: "30s",
				Raw: "kube_pod_container_status_restarts_total{ cluster=\"pr\", namespace=\"frlm-web-media-prod\", container=\"lmfr-edito-api\" }",
			},
			Reduce: &model.Reduce{
				Type: "diff",
			},
			Evaluate: &model.Evaluate{
				Type: "above",
				Config: evaluateConfig,
			},
		}


	var conditionEvaluator = ConditionEvaluator{}

	resp, err := conditionEvaluator.Eval(condition)
	if err != nil {
		logrus.WithError(err).Errorf("Fail to evaluate expression")
	}

	logrus.Infof("Value : %s", resp.Message)


}