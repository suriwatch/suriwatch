package condition

import (
	"errors"
	"fmt"
	model "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	tsdb "gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor/tsdb"
)

type Evaluator interface {

	Evaluate(evaluate *model.Evaluate, value *tsdb.Value) *EvalResponse
}

// Reduce a response to a single max value
type AboveEvaluator struct {}

func (ae AboveEvaluator) Evaluate(evaluate *model.Evaluate, value *tsdb.Value) *EvalResponse{

	resp  := &EvalResponse{}
			threshold := evaluate.Config["value"].(float64)

	if value == nil {
		resp.Success = false
		resp.Message = fmt.Sprintf("No value found for probe", threshold)
	} else if float64(*value) > threshold {
		resp.Success = false
		resp.Message = fmt.Sprintf("Value %f is not above the given threshold %f", float64(*value) , threshold)
	}else{
		resp.Message = fmt.Sprintf("Value %f is above the given threshold %f", float64(*value) , threshold)
		resp.Success = true
	}

	return resp
}

// Reduce a response to a single max value
type BelowEvaluator struct {}


func (be BelowEvaluator) Evaluate(evaluate *model.Evaluate, value *tsdb.Value) *EvalResponse{

	return &EvalResponse{}

}


func NewEvaluator(typ model.EvaluateType) (Evaluator, error){

	switch typ {
	case "above":
		reducer := &AboveEvaluator{}
		return reducer, nil
	case "below":
		reducer := &BelowEvaluator{}
		return reducer, nil
	}

	return nil, errors.New(fmt.Sprintf("Unkown evaluator type %s", typ))

}


