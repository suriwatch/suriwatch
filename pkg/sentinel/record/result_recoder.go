package record

import (
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
)

type ResultRecorder struct {
	resultDao  dao.ProbeResultDAO
	recordChan chan model.ProbeResult
	stopChan   chan struct{}
	buffer     []model.ProbeResult
	mutex      *sync.Mutex
}

/**
 * Constructor for probe
 */
func NewResultRecorder(provider *dao.Provider) *ResultRecorder {

	// Initialise provider

	return &ResultRecorder{
		resultDao:  provider.GetProbeResultDao(),
		recordChan: make(chan model.ProbeResult, 100),
		stopChan:   make(chan struct{}),
		buffer:     make([]model.ProbeResult, 0),
		mutex:      &sync.Mutex{},
	}
}

/**
 * Execute a probe and give the result.
 */
func (m *ResultRecorder) Record(entity model.ProbeResult) {
	m.recordChan <- entity
}

/**
 * Execute a probe and give the result.
 */
func (m *ResultRecorder) Start() {

	go m.process()
}

func (m *ResultRecorder) process() {

	ticker := time.NewTicker(time.Millisecond * 500)
	// stop ticker at the end
	defer ticker.Stop()

	for {
		select {

		case <-ticker.C:
			log.Debug("Time to bulk results")

			m.mutex.Lock()
			if len(m.buffer) > 0 {
				log.Debugf("Bulking [%d] results", len(m.buffer))
				m.resultDao.SaveBulk(m.buffer)
				m.buffer = m.buffer[:0]
			}

			m.mutex.Unlock()

		case res := <-m.recordChan:

			log.Debug("Buffering result")
			m.mutex.Lock()
			m.buffer = append(m.buffer, res)
			m.mutex.Unlock()

		case <-m.stopChan:

			log.Info("Stopping result recorder")
			return
		}
	}
}

/**
 * Stop the recorder
 */
func (m *ResultRecorder) Stop() {
	close(m.stopChan)
}
