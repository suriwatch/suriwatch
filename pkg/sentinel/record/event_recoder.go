package record

import (
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
)

// https://medium.com/@matryer/stopping-goroutines-golang-1bf28799c1cb
// https://talks.golang.org/2012/10things.slide#12

type EventRecorder struct {
	eventDao   dao.EventDAO
	recordChan chan model.StatusChangeEvent
	stopChan   chan struct{}
	buffer     []model.StatusChangeEvent
	mutex      *sync.Mutex
}

/**
 * Constructor for probe
 */
func NewEventRecorder(provider *dao.Provider) *EventRecorder {

	// Initialise provider

	return &EventRecorder{
		eventDao:   provider.GetEventDao(),
		recordChan: make(chan model.StatusChangeEvent, 100),
		stopChan:   make(chan struct{}),
		buffer:     make([]model.StatusChangeEvent, 0),
		mutex:      &sync.Mutex{},
	}
}

/**
 * Execute a probe and give the result.
 */
func (m *EventRecorder) Record(entity model.StatusChangeEvent) {

	m.recordChan <- entity
}

/**
 * Execute a probe and give the result.
 */
func (m *EventRecorder) Start() {

	go m.process()
}

func (m *EventRecorder) process() {

	ticker := time.NewTicker(time.Millisecond * 500)
	// stop ticker at the end
	defer ticker.Stop()

	for {
		select {

		case <-ticker.C:

			log.Debug("Time to bulk event")

			m.mutex.Lock()
			if len(m.buffer) > 0 {
				log.Debugf("Bulking [%d] events", len(m.buffer))
				m.eventDao.SaveBulk(m.buffer)
				m.buffer = make([]model.StatusChangeEvent, 0)
			}

			m.mutex.Unlock()

		case res := <-m.recordChan:

			log.Debug("Buffering event")
			m.mutex.Lock()
			m.buffer = append(m.buffer, res)
			m.mutex.Unlock()

		case <-m.stopChan:

			log.Info("Stopping event recorder")
			return
		}
	}
}

/**
 * Stop the recorder
 */
func (m *EventRecorder) Stop() {
	close(m.stopChan)
}
