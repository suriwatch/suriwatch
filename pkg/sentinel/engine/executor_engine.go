package engine

import (
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor/tsdb_executor"
	"time"

	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	. "gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/scheduler"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/event"
	. "gitlab.com/suriwatch/suriwatch/pkg/sentinel/executor"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/record"
)

type scheduledProbeID struct {
	id   scheduler.EntryID
	date time.Time
}

type ExecutorEngine struct {

	// DAO configuration
	probeDao ProbeDAO

	// Recorders
	resultRecorder      *record.ResultRecorder
	resultEventDetector *event.ResultEventDetector

	//Probe executors
	httpExecutor    *HTTPProbeExecutor
	networkExecutor *NetworkProbeExecutor
	tsdbExecutor    *tsdb_executor.TSDBProbeExecutor

	scheduler        *scheduler.Scheduler
	labels           map[string]string
	mode             RunMode
	schedulerEntries map[ProbeId]scheduledProbeID
	maxProbes        int

	// Notify a new result
	resultChan chan ProbeResult
}

/**
 * Constructor for probe
 */
func NewExecutorEngine(provider *Provider) *ExecutorEngine {

	// Initialise provider
	tsdbExecutor := tsdb_executor.New(provider)

	return &ExecutorEngine{
		probeDao: provider.GetProbeDao(),

		resultRecorder:      record.NewResultRecorder(provider),
		resultEventDetector: event.NewResultEventDetector(provider),

		networkExecutor:  &NetworkProbeExecutor{},
		httpExecutor:     &HTTPProbeExecutor{},
		tsdbExecutor:	  &tsdbExecutor,

		labels:           make(map[string]string),
		schedulerEntries: make(map[ProbeId]scheduledProbeID),
		maxProbes:        10000,

		// Channels for new result created
		resultChan: make(chan ProbeResult, 1000),
	}
}

func (m *ExecutorEngine) Start(labels map[string]string, mode RunMode) {

	m.labels = labels
	m.mode = mode
	log.Infof("Starting %s engine with labels %s", mode, labels)

	m.scheduler = scheduler.New()

	m.reloadConfig()

	// Scheduling job reload
	m.scheduler.AddFunc(30, func() {
		m.reloadConfig()
	})

	m.scheduler.Start(true)

	// Event recorder
	m.resultRecorder.Start()

	// Start the probe status change detector
	m.resultEventDetector.Start()

	// Probe change status detector
	go m.processResult()

}

func (m *ExecutorEngine) Stop() {

	m.resultRecorder.Stop()
	m.resultEventDetector.Stop()
}

/**
 * Execute a probe and give the result.
 */
func (m *ExecutorEngine) processResult() {

	for res := range m.resultChan {

		// Save the result
		m.resultRecorder.Record(res)

		// Check probe status change
		m.resultEventDetector.Verify(res)

	}
}

func (m *ExecutorEngine) reschedule(probe Probe) {

	m.unschedule(probe.Id)
	m.schedule(probe)
}

/**
 * Schedule the given probe
 */
func (m *ExecutorEngine) unschedule(id ProbeId) {

	scheduledProbeID, exist := m.schedulerEntries[id]

	if exist {
		m.scheduler.Remove(scheduledProbeID.id)
		delete(m.schedulerEntries, id)
	}
}

/**
 * Give the number of scheduled probes
 */
func (m *ExecutorEngine) getProbesCount() int {
	return len(m.schedulerEntries)
}

/**
 * Schedule the given probe
 */
func (m *ExecutorEngine) schedule(probe Probe) {

	var interval = probe.Interval
	if probe.Interval <= 0 {
		interval = 10
		log.Warningf("Scheduling probe [%s] interval is to small : [%d]. Setting it to [%d]", probe.Name, probe.Interval, interval)

	}
	entryId, err := m.scheduler.AddFunc(uint64(interval), func() {
		m.CheckProbe(probe)
	})

	// Recording probe entryId
	if err != nil {
		log.Errorf("Fail to schedule probe [%s] : %s", probe.Name, err)
	} else {

		m.schedulerEntries[probe.Id] = scheduledProbeID{
			entryId,
			probe.Update,
		}
	}
}

func (m *ExecutorEngine) ListProbes() ([]Probe, error) {

	query := SearchProbeQuery{}
	query.Labels = m.labels
	query.Size = &m.maxProbes
	probes, err := m.probeDao.Search(query)
	return probes, err

}

func (m *ExecutorEngine) CheckProbes() []ProbeResult {

	probes, err := m.ListProbes()
	if err != nil {
		log.Error("Fail to list probes", err)
	}
	return m.checkProbes(probes)
}

/**
 * Execute probes and give the result
 */
func (m *ExecutorEngine) checkProbes(probes []Probe) []ProbeResult {

	res := make([]ProbeResult, len(probes))

	for idx, probe := range probes {
		res[idx] = m.CheckProbe(probe)
	}

	return res
}

/**
 * Execute a probe and give the result.
 */
func (m *ExecutorEngine) CheckProbe(probe Probe) ProbeResult {

	log.Infof("Checking probe [%s] with type [%s]", probe.Name, probe.Type)

	result := ProbeResult{}

	if probe.Type == "http" {

		result = m.httpExecutor.Execute(probe)
		m.resultChan <- result

	} else if probe.Type == "network" {

		result := m.networkExecutor.Execute(probe)
		m.resultChan <- result

	} else if probe.Type == "tsdb" {

		result := m.tsdbExecutor.Execute(probe)
		m.resultChan <- result


	} else {
		log.Warningf("Unknown probe type [%s]", probe.Type)
	}

	return result
}

/**
 * Schedule the given probe
 */
func (m *ExecutorEngine) reloadConfig() {
	log.Infof("Reloading configuration")

	mode := string(m.mode)

	query := SearchProbeQuery{}
	query.Labels = m.labels
	query.Size = &m.maxProbes
	query.Mode = &mode
	newProbes, err := m.probeDao.Search(query)

	if err != nil {
		log.Error("Fail to obtain probes configuration. Reloading is aborded")
		return
	}

	newProbesNames := make(map[ProbeId]bool)

	// Check for new / updated probes
	for _, newProbe := range newProbes {

		scheduledProbeID, exist := m.schedulerEntries[newProbe.Id]

		if newProbe.State == STARTED {

			if !exist {
				log.Infof("Scheduling new probe [%s]", newProbe.Id)
				m.schedule(newProbe)
			} else if scheduledProbeID.date.Before(newProbe.Update) {
				log.Infof("Rescheduling probe [%s]", newProbe.Id)
				m.unschedule(newProbe.Id)
				m.schedule(newProbe)
			}
			// Building new probe hashset
			newProbesNames[newProbe.Id] = true
		} else {
			log.Infof("Probe [%s] state is [%s]. Not started...so ignored", newProbe.Name, newProbe.State)
		}

	}

	// Check for removed probes
	for key, _ := range m.schedulerEntries {
		_, exist := newProbesNames[key]

		if !exist {
			log.Infof("Unscheduling probe [%s]", key)
			m.unschedule(key)
		}
	}

}
