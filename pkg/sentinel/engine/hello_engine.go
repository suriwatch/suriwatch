package engine

import (
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	. "gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/scheduler"
)

/**
 * HelloEngine service send periodically a message to signify that
*  sentinel is still alive and give some status information
*/
type HelloEngine struct {

	// DAO configuration
	nodeDao        NodeDAO
	scheduler      *scheduler.Scheduler
	executorEngine *ExecutorEngine

	selectors map[string]string
}

/**
 * Constructor for probeDat
 */
func NewHelloEngine(provider *Provider, selectors map[string]string, executorEngine *ExecutorEngine) *HelloEngine {

	return &HelloEngine{
		nodeDao:        provider.GetNodeDao(),
		selectors:      selectors,
		executorEngine: executorEngine,
	}
}

/**
 * Start the hello service.
 */
func (m *HelloEngine) Start() {

	log.Infof("Starting hello service")

	m.scheduler = scheduler.New()

	// Scheduling job reload
	m.scheduler.AddFunc(30, func() {
		m.sayHello()
	})
	m.scheduler.Start(false)

}

/**
 * Stop the hello service
 */
func (m *HelloEngine) Stop() {

	log.Infof("Stopping hello service")

	m.scheduler.Stop()

}

/**
 * send Hello packet with sentinel informations
 */
func (obj *HelloEngine) sayHello() {

	pingPacket := model.StatusPacket{}
	pingPacket.Host, _ = os.Hostname()
	pingPacket.Node = config.Get().Node.Name
	pingPacket.Date = time.Now()
	pingPacket.StartedAt = config.Get().AppInfo.Started
	pingPacket.Labels = obj.selectors
	pingPacket.Mode = config.Get().Node.Mode
	pingPacket.ProbesCount = obj.executorEngine.getProbesCount()
	obj.nodeDao.SendStatus(pingPacket)
}
