package alerter

var notifierRegistry = make(map[string] *NotifierPlugin)

type NotifierPlugin struct {

	Name string
	Type string

	// Provider a notifier instance
	Factory  func() (Notifier, error)

	// Plugin notifier
	notifier Notifier

}

func (n *NotifierPlugin) Notify() error {

	return nil;
}


type Notifier struct{

}




// RegisterNotifier register an notifier
func RegisterNotifier(plugin *NotifierPlugin) {

	notifierRegistry[plugin.Type] = plugin
}