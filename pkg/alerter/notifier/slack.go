package notifier

import "gitlab.com/suriwatch/suriwatch/pkg/alerter"

type AlertSlackConfig struct {
	alerter.AlertConfig
	Email string
}


