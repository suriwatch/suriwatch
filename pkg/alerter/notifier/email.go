package notifier

import (
	"bytes"
	"gitlab.com/suriwatch/suriwatch/pkg/alerter"
	"log"
	"net/smtp"
)

type AlertMailConfig struct {
	alerter.AlertConfig
	Email string
}




func init() {
	alerter.RegisterNotifier(&alerter.NotifierPlugin{
		Type: "mail",

	})
}



type EmailNotifier struct{


}



func (n *EmailNotifier) Notify() error {

}




func send(){
	// Connect to the remote SMTP server.
	c, err := smtp.Dial("mail.example.com:25")
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	// Set the sender and recipient.
	c.Mail("sender@example.org")
	c.Rcpt("recipient@example.net")
	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		log.Fatal(err)
	}
	defer wc.Close()
	buf := bytes.NewBufferString("This is the email body.")
	if _, err = buf.WriteTo(wc); err != nil {
		log.Fatal(err)
	}
}