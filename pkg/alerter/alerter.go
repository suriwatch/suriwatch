package alerter

import (
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/scheduler"
)

/**
 * Constructor for probe
 */
func NewAlerter(provider *dao.Provider) *Alerter {

	engine := &Alerter{
		eventDao:  provider.GetEventDao(),
		alertDao:  provider.GetAlertConfigDao(),
		scheduler: scheduler.New(),

		schedulerEntries: make(map[model.AlertConfigId]scheduledProbeID),
		lastExecutions:   make(map[model.AlertConfigId]time.Time),
	}
	return engine

}

type Alerter struct {
	eventDao  dao.EventDAO
	alertDao  dao.AlertConfigDAO
	scheduler *scheduler.Scheduler

	schedulerEntries map[model.AlertConfigId]scheduledProbeID
	lastExecutions   map[model.AlertConfigId]time.Time
}

// Start the alerting engine module
func (eng *Alerter) Start() error {

	log.Info("Starting alerter")

	eng.scheduler = scheduler.New()

	eng.reloadConfig()
	// Scheduling job reload
	eng.scheduler.AddFunc(30, func() {
		eng.reloadConfig()
	})

	eng.scheduler.Start(false)

	return nil
}

type scheduledProbeID struct {
	id   scheduler.EntryID
	date time.Time
}

func (eng *Alerter) Stop() {

	eng.scheduler.Stop()
}

/**
 * Execute the alert detection.
 */
func (a *Alerter) CheckAlert(alert model.AlertConfig) {

	log.Infof("Checking alert [%s] with type [%s]", alert.Name, alert.Type)

	fromDate := time.Now().Add(time.Duration(-alert.Interval) * time.Second)
	toDate := time.Now()

	previousExec, exist := a.lastExecutions[alert.Id]
	if exist {
		fromDate = previousExec
	}

	from := strconv.FormatInt(fromDate.UTC().UnixNano(), 10)
	to := strconv.FormatInt(toDate.UTC().UnixNano(), 10)

	query := model.SearchEventQuery{
		Labels: alert.Labels,
		From:   &from,
		To:     &to,
	}
	events, err := a.eventDao.Search(query)

	if err != nil {
		log.Errorf("Fail to evaluate alert [%s] : %s", alert.Name, err)
		return
	}

	for _, event := range events {

		if event.End == nil && DateWithinRange(&event.Start, fromDate, toDate) {
			//alert.NotifType
		}
	}
	a.lastExecutions[alert.Id] = toDate

}

func DateWithinRange(date *time.Time, begin time.Time, end time.Time) bool {

	if date == nil {
		return false
	}

	if date.Before(begin) || date.After(end) {
		return false
	} else {
		return true
	}

}

/**
 * Schedule the given alert configuration
 */
func (a *Alerter) schedule(alertConfig model.AlertConfig) {

	var interval = alertConfig.Interval
	if alertConfig.Interval <= 0 {
		interval = 10
		log.Warningf("Scheduling alert [%s] interval is to small : [%d]. Setting it to [%d]", alertConfig.Name, alertConfig.Interval, interval)

	}
	entryId, err := a.scheduler.AddFunc(uint64(interval), func() {
		a.CheckAlert(alertConfig)
	})

	// Recording probe entryId
	if err != nil {
		log.Errorf("Fail to schedule alert config [%s] : %s", alertConfig.Name, err)
	} else {

		a.schedulerEntries[alertConfig.Id] = scheduledProbeID{
			entryId,
			alertConfig.Update,
		}
	}
}

func (a *Alerter) reschedule(alertConfig model.AlertConfig) {

	a.unschedule(alertConfig.Id)
	a.schedule(alertConfig)
}

/**
 * UnSchedule the given alert
 */
func (m *Alerter) unschedule(id model.AlertConfigId) {

	scheduledProbeID, exist := m.schedulerEntries[id]

	if exist {
		m.scheduler.Remove(scheduledProbeID.id)
		delete(m.schedulerEntries, id)
	}
}

// Reload alerting configuration
func (a *Alerter) reloadConfig() {

	log.Infof("Reloading alert configuration")

	query := model.SearchAlertConfigQuery{}

	newAlerts, err := a.alertDao.Search(query)

	if err != nil {
		log.Error("Fail to obtain alerts configuration. Reloading is aborded")
		return
	}

	newAlertsNames := make(map[model.AlertConfigId]bool)

	// Check for new / updated probes
	for _, newAlertConfig := range newAlerts {

		scheduledProbeID, exist := a.schedulerEntries[newAlertConfig.Id]

		if newAlertConfig.State == model.ALERT_STARTED {

			if !exist {
				log.Infof("Scheduling new alert config [%s]", newAlertConfig.Id)
				a.schedule(newAlertConfig)
			} else if scheduledProbeID.date.Before(newAlertConfig.Update) {
				log.Infof("Rescheduling alert config [%s]", newAlertConfig.Id)
				a.unschedule(newAlertConfig.Id)
				a.schedule(newAlertConfig)
			}
			// Building new probe hashset
			newAlertsNames[newAlertConfig.Id] = true
		} else {
			log.Infof("Alert [%s] state is [%s]. Not started...so ignored", newAlertConfig.Name, newAlertConfig.State)
		}

	}

	// Check for removed probes
	for key, _ := range a.schedulerEntries {

		if _, exist := newAlertsNames[key]; !exist {
			log.Infof("Unscheduling alert config [%s]", key)
			a.unschedule(key)
		}
	}

}
