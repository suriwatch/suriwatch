package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
	"net/http"
)

func RegisterLabelResource(router *mux.Router, provider *dao.Provider) {
	labelHandler := newLabelHandler(provider)

	router.HandleFunc("/labels/{name}", labelHandler.GetLabel).Methods("GET")
	router.HandleFunc("/labels", labelHandler.GetLabels).Methods("GET")
}

type LabelHandler struct {
	config   *config.Config
	labelDao dao.LabelDAO
}

func newLabelHandler(provider *dao.Provider) *LabelHandler {

	return &LabelHandler{
		config:   provider.GetConfig(),
		labelDao: provider.GetLabelDao(),
	}
}

// Give a probe by identifier
//
// GET /probe/{id}
//
// Responses:
// 200: Probe successfully return
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (a LabelHandler) GetLabel(w http.ResponseWriter, r *http.Request) {

	labelKey := "name"
	vars := mux.Vars(r)

	if _, ok := vars[labelKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Label name is mandatory")
	}

	label := vars[labelKey]

	w.Header().Set("Content-Type", "application/json")
	labelRef, err := a.labelDao.GetLabel(label)

	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get label")
		return
	}

	if labelRef == nil {
		util.RespondWithError(w, http.StatusNotFound, "Label with name "+label+" not found")
		return
	}

	json.NewEncoder(w).Encode(labelRef)
}

// Give a probe by identifier
//
// GET /probe/{id}
//
// Responses:
// 200: Probe successfully return
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (a LabelHandler) GetLabels(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	labelsRef, err := a.labelDao.GetLabels(false)

	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get label")
		return
	}

	if labelsRef == nil {
		util.RespondWithError(w, http.StatusNotFound, "Not Label found")
		return
	}

	json.NewEncoder(w).Encode(labelsRef)
}
