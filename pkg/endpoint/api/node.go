package api

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
)

func RegisterNodeResource(router *mux.Router, provider *dao.Provider) {
	nodeHandler := newNodeHandler(provider)
	router.HandleFunc("/nodes/_status", nodeHandler.SearchNodesStatus).Methods("GET")
	router.HandleFunc("/nodes/_search", nodeHandler.SearchNodes).Methods("GET")

}

type NodeHandler struct {
	config  *config.Config
	nodeDao dao.NodeDAO
}

func newNodeHandler(provider *dao.Provider) *NodeHandler {

	return &NodeHandler{
		config:  provider.GetConfig(),
		nodeDao: provider.GetNodeDao(),
	}
}

// Give a probe by identifier
//
// GET /probe/{id}
//
// Responses:
// 200: Probe successfully return
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (obj NodeHandler) SearchNodesStatus(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	to := time.Now()
	var defaultFrom = (to).Add(time.Duration(-5) * time.Minute)
	from := defaultFrom
	size := 250

	nodeStatus, err := obj.nodeDao.SearchStatus(&size, &from, &to)

	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get label")
		return
	}

	if nodeStatus == nil {
		util.RespondWithError(w, http.StatusNotFound, "No node status found")
		return
	}

	json.NewEncoder(w).Encode(nodeStatus)
}

// Recupere la liste des noeud
func (obj NodeHandler) SearchNodes(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	to := time.Now()
	var defaultFrom = (to).Add(time.Duration(-5) * time.Minute)
	from := defaultFrom
	size := 250

	probeId, error := util.GetQueryParamString("probeId", nil, w, r)
	if error != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail process probeId parameter")
		return
	}

	query := model.HistoNodeQuery{}
	query.Size = &size
	query.From = &from
	query.To = &to
	query.ProbeId = probeId

	nodes, err := obj.nodeDao.SearchNodeHisto(query)

	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get nodes")
		return
	}

	if nodes == nil {
		util.RespondWithError(w, http.StatusNotFound, "No node status found")
		return
	}

	json.NewEncoder(w).Encode(nodes)
}
