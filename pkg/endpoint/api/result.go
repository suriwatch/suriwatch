	package api

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
)

func RegisterResultResource(router *mux.Router, provider *dao.Provider) {
	handler := newResultHandler(provider)

	router.HandleFunc("/results/_last", handler.GetLastProbesResult).Methods("GET")
	router.HandleFunc("/results/_search", handler.GetProbeStatus).Methods("GET")
}

type ResultHandler struct {
	config         *config.Config
	probeDao       dao.ProbeDAO
	probeResultDao dao.ProbeResultDAO
}

func newResultHandler(provider *dao.Provider) *ResultHandler {

	return &ResultHandler{
		config:         provider.GetConfig(),
		probeDao:       provider.GetProbeDao(),
		probeResultDao: provider.GetProbeResultDao(),
	}
}

// Give last probes results for the given interval
//
// GET /results/_last
//
//parameters:
// - name: size
//   description: maximum number of probes evaluated
// - name: from
//   description: begin timestamp for the search interval
//   default: to - 10m
// - name: to
//   description: end timestamp for the search interval
//   default: now()
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (obj ResultHandler) GetLastProbesResult(w http.ResponseWriter, r *http.Request) {

	log.Debug("GetLastProbesResult request")

	var defaultSize = DEFAULT_SEARCH_SIZE
	size, error := util.GetQueryParamInt("size", &defaultSize, w, r)
	if error != nil {
		return
	}

	var defaultTo = time.Now()
	to, error := util.GetQueryParamDate("to", &defaultTo, w, r)
	if error != nil {
		return
	}

	var defaultFrom = (*to).Add(time.Duration(-DEFAULT_SEARCH_MINUTES_INTERVAL) * time.Minute)
	from, error := util.GetQueryParamDate("from", &defaultFrom, w, r)
	if error != nil {
		return
	}

	host, error := util.GetQueryParamString("host", nil, w, r)
	if error != nil {
		return
	}

	status, error := util.GetQueryParamProbeStatus("status", nil, w, r)
	if error != nil {
		return
	}

	query, error := util.GetQueryParamString("q", nil, w, r)
	if error != nil {
		return
	}

	probeType, error := util.GetQueryParamProbeType("type", nil, w, r)
	if error != nil {
		return
	}

	var labels = make(map[string]string)
	serializedLabels := r.URL.Query().Get("labels")

	if serializedLabels != "" {
		labels = util.DeserializeLabels(serializedLabels)
	}

	var probeIds []model.ProbeId
	serializedProbeIds := r.URL.Query().Get("probeIds")

	if serializedProbeIds != "" {
		probeIds = util.DeserializeProbeIds(serializedProbeIds)
	}

	w.Header().Set("Content-Type", "application/json")
	criteria := model.SearchLastResultCriteria{
		From:     from,
		To:       to,
		Size:     size,
		Labels:   labels,
		Host:     host,
		Status:   status,
		Query:    query,
		Type:     probeType,
		ProbeIds: probeIds,
	}
	probesResGroup, _ := obj.probeResultDao.GetLastProbesResult(criteria)

	json.NewEncoder(w).Encode(probesResGroup)
}

// Give last probes results for the given interval and grouped
//
// GET /results/_search
//
//parameters:
// - name: size
//   description: maximum number of probes evaluated
// - name: from
//   description: begin timestamp for the search interval
//   default: to - 10m
// - name: to
//   description: end timestamp for the search interval
//   default: now()
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (obj ResultHandler) GetProbeStatus(w http.ResponseWriter, r *http.Request) {

	log.Debug("GetProbeStatus request")

	criteria := model.SearchAggregatedResultCriteria{}
	var error error

	var defaultSize = DEFAULT_SEARCH_SIZE
	criteria.Size, error = util.GetQueryParamInt("size", &defaultSize, w, r)
	if error != nil {
		return
	}

	var defaultTo = time.Now()
	criteria.To, error = util.GetQueryParamDate("to", &defaultTo, w, r)
	if error != nil {
		return
	}

	var defaultFrom = (*criteria.To).Add(time.Duration(-DEFAULT_SEARCH_MINUTES_INTERVAL) * time.Minute)
	criteria.From, error = util.GetQueryParamDate("from", &defaultFrom, w, r)
	if error != nil {
		return
	}

	// Get labels filtering options
	serializedLabels := r.URL.Query().Get("labels")
	if serializedLabels != "" {
		criteria.Labels = util.DeserializeLabels(serializedLabels)
	}

	host, error := util.GetQueryParamString("host", nil, w, r)
	if error != nil {
		return
	}
	criteria.Host = host

	status, error := util.GetQueryParamProbeStatus("status", nil, w, r)
	if error != nil {
		return
	}
	criteria.Status = status

	query, error := util.GetQueryParamString("q", nil, w, r)
	if error != nil {
		return
	}
	criteria.Query = query

	probeType, error := util.GetQueryParamProbeType("type", nil, w, r)
	criteria.Type = probeType

	var probeIds []model.ProbeId
	serializedProbeIds := r.URL.Query().Get("probeIds")

	if serializedProbeIds != "" {
		probeIds = util.DeserializeProbeIds(serializedProbeIds)
	}
	criteria.ProbeIds = probeIds

	// Get grouping options
	var groups []string
	serializedGroups := r.URL.Query().Get("groups")
	if serializedGroups != "" {
		groups = util.DeserializeGroups(serializedGroups)
	}

	w.Header().Set("Content-Type", "application/json")

	prg , _ := obj.probeResultDao.GetAggregatedLastProbesResult(groups, criteria)

	json.NewEncoder(w).Encode(prg)
}
