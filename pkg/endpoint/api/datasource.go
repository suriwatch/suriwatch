package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
)

func RegisterDataSourceResource(router *mux.Router, provider *dao.Provider) {
	handler := newDataSourceHandler(provider)
	router.HandleFunc("/datasources", handler.CreateDataSource).Methods("POST")
	router.HandleFunc("/datasources", handler.SearchDataSource).Methods("GET")
	router.HandleFunc("/datasources/{id}", handler.GetDataSourceById).Methods("GET")
	router.HandleFunc("/datasources/{id}", handler.DeleteDataSource).Methods("DELETE")
	router.HandleFunc("/datasources/{id}", handler.UpdateDataSource).Methods("PUT")
}

type DataSourceHandler struct {
	dataSourceDao dao.DataSourceDAO
}

func newDataSourceHandler(provider *dao.Provider) *DataSourceHandler {

	return &DataSourceHandler{
		dataSourceDao: provider.GetDataSourceDao(),
	}
}

// give the status page configuration
func (a *DataSourceHandler) GetDataSourceById(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	id, err := GetUrlParamString("id", w, r)
	if err != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Data source id is mandatory")
	}

	board, err := a.dataSourceDao.GetByID(model.DataSourceId(id))
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get datasource")
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(board)

}
func (a *DataSourceHandler) CreateDataSource(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	var ds model.DataSource
	b, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(b, &ds )

	// Tolerate probe update...
	if ds .Id == "" {
		ds .Id = model.NewDataSourceId()
		ds .Created = time.Now()
	} else {
		ds .Updated = time.Now()
	}
	
	result, err := a.dataSourceDao.Save(ds)
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to create datasource")
		return
	}

	json.NewEncoder(w).Encode(result)

}

func (a *DataSourceHandler) UpdateDataSource(w http.ResponseWriter, r *http.Request) {

}

func (a *DataSourceHandler) SearchDataSource(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	query := model.SearchDataSourceQuery{}
	dataSources, err := a.dataSourceDao.Search(query)
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to search datasources")
		return
	}
	json.NewEncoder(w).Encode(dataSources)
}


// Delete a probe by identifier
//
// DELETE /probe/{id}
//
// Responses:
// 200: Probe successfully deleted
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (a DataSourceHandler) DeleteDataSource(w http.ResponseWriter, r *http.Request) {

	idKey := "id"
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Datasource id is mandatory")
	}

	id := vars[idKey]

	log.Infof("Deleting datasource [%s]", id)

	w.Header().Set("Content-Type", "application/json")
	probe, err := a.dataSourceDao.GetByID(model.DataSourceId(id))

	if err != nil {
		log.Errorf("Fail to resolve datasource by id [%s]", id)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing datasource")
		return
	}

	// Is the probe exist ?
	if probe == nil {
		util.RespondWithError(w, http.StatusNotFound, "Datasource with id "+id+" not found")
		return
	}

	deleteErr := a.dataSourceDao.DeleteByID(model.DataSourceId(id))

	if deleteErr != nil {
		log.Errorf("Fail to delete datasources [%s] : %s", id, deleteErr)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing datasource")
		return
	}

	util.RespondWithoutContent(w, http.StatusNoContent)
}
