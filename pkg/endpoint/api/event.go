package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
	"gitlab.com/suriwatch/suriwatch/pkg/service"
)

func RegisterEventResource(router *mux.Router, provider *dao.Provider) {
	eventHandler := newEventHandler(provider)
	router.HandleFunc("/events", eventHandler.SearchEvents).Methods("GET")
	router.HandleFunc("/events/_repair", eventHandler.RepairEvents).Methods("GET")
}

type EventHandler struct {
	config       *config.Config
	eventDao     dao.EventDAO
	eventService *service.EventService
}

func newEventHandler(provider *dao.Provider) *EventHandler {

	return &EventHandler{
		config:       provider.GetConfig(),
		eventDao:     provider.GetEventDao(),
		eventService: service.NewEventService(provider),
	}
}

func (obj EventHandler) RepairEvents(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	probeId, error := util.GetQueryParamProbeId("probeId", nil, w, r)
	if error != nil {
		return
	}

	result, error := obj.eventService.ProbeEventRepair(*probeId)

	json.NewEncoder(w).Encode(result)
}

// Give a probe by identifier
//
// GET /probe/{id}
//
// Responses:
// 200: Probe successfully return
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (obj EventHandler) SearchEvents(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	error := errors.New("")
	query := model.SearchEventQuery{}

	var defaultSize = DEFAULT_SEARCH_SIZE
	query.Size, error = util.GetQueryParamInt("size", &defaultSize, w, r)
	if error != nil {
		return
	}

	var defaultFrom = "now-" + strconv.Itoa(DEFAULT_SEARCH_MINUTES_INTERVAL) + "m"
	query.From, error = util.GetQueryParamString("from", &defaultFrom, w, r)
	if error != nil {
		return
	}

	var defaultTo = "now"
	query.To, error = util.GetQueryParamString("to", &defaultTo, w, r)
	if error != nil {
		return
	}

	query.ProbeId, error = util.GetQueryParamProbeId("probeId", nil, w, r)
	if error != nil {
		return
	}

	// Build labels
	serializedLabels := r.URL.Query().Get("labels")
	if serializedLabels != "" {
		query.Labels = util.DeserializeLabels(serializedLabels)
	}

	// Build sort conditions
	serializedSort := r.URL.Query().Get("sort")
	if serializedSort != "" {
		query.Sort = util.DeserializeSort(serializedSort)
	}

	events, err := obj.eventDao.Search(query) //&size, &from, &to)

	if err != nil {
		log.Error("Fail to get event", err)
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get events")
		return
	}

	// If no event found return empty array
	if events == nil {
		json.NewEncoder(w).Encode([]model.StatusChangeEvent{})
	}

	json.NewEncoder(w).Encode(events)
}
