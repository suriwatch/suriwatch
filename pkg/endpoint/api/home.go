package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"net/http"
)

func RegisterHomeResource(router *mux.Router, provider *dao.Provider) {
	homeHandler := newHomeHandler(provider)
	router.HandleFunc("/", homeHandler.GetHome)
}

type HomeHandler struct {
	probeDao       dao.ProbeDAO
	probeResultDao dao.ProbeResultDAO
}

func newHomeHandler(provider *dao.Provider) *HomeHandler {

	return &HomeHandler{
		//probeDao: provider.GetProbeDao(),
		//probeResultDao: provider.GetProbeResultDAO(),
	}
}

func (a *HomeHandler) GetHome(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	var config = config.Get()
	json.NewEncoder(w).Encode(config.AppInfo)

}
