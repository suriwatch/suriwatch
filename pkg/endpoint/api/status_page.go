package api

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
	"gitlab.com/suriwatch/suriwatch/pkg/service"
)

func RegisterStatusPageResource(router *mux.Router, provider *dao.Provider) {
	handler := newStatusPageHandler(provider)
	router.HandleFunc("/status-pages", handler.CreateStatusPage).Methods("POST")
	router.HandleFunc("/status-pages", handler.SearchStatusPage).Methods("GET")
	router.HandleFunc("/status-pages/{id}", handler.GetStatusPageById).Methods("GET")
	router.HandleFunc("/status-pages/{id}", handler.DeleteStatusPage).Methods("DELETE")
	router.HandleFunc("/status-pages/{id}/status", handler.GetStatusPageResult).Methods("GET")
	router.HandleFunc("/status-pages/{id}", handler.UpdateStatusPage).Methods("PUT")
}

type StatusPageHandler struct {
	statusPageSvc *service.StatusPageService
}

func newStatusPageHandler(provider *dao.Provider) *StatusPageHandler {

	return &StatusPageHandler{
		statusPageSvc: service.NewStatusPageService(provider),
	}
}

// give the status page configuration
func (a *StatusPageHandler) GetStatusPageById(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	id, err := GetUrlParamString("id", w, r)
	if err != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Status page id is mandatory")
	}

	board, err := a.statusPageSvc.GetByID(model.StatusPageId(id))
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get statusPage")
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(board)

}
func (a *StatusPageHandler) CreateStatusPage(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	var statusPage model.StatusPage
	b, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(b, &statusPage)

	statusPage.Update = time.Now()

	result, err := a.statusPageSvc.Save(statusPage)
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to create statusPage")
		return
	}

	json.NewEncoder(w).Encode(result)

}

func (a *StatusPageHandler) UpdateStatusPage(w http.ResponseWriter, r *http.Request) {

}

func (a *StatusPageHandler) SearchStatusPage(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	query := model.SearchStatusPageQuery{}
	statusPages, err := a.statusPageSvc.Search(query)
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to search statusPage")
		return
	}
	json.NewEncoder(w).Encode(statusPages)
}

func (a *StatusPageHandler) GetStatusPageResult(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	id, err := GetUrlParamString("id", w, r)
	if err != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Dashboard id is mandatory")
	}

	boardStatus, err := a.statusPageSvc.GetStatus(model.StatusPageId(id))
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get statusPage status")
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(boardStatus)

}

func GetUrlParamString(name string, w http.ResponseWriter, r *http.Request) (string, error) {
	idKey := name
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		return "", errors.New("url parameter %s not found ")
	}

	return vars[idKey], nil

}

// Delete a probe by identifier
//
// DELETE /probe/{id}
//
// Responses:
// 200: Probe successfully deleted
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (a StatusPageHandler) DeleteStatusPage(w http.ResponseWriter, r *http.Request) {

	idKey := "id"
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Probe id is mandatory")
	}

	id := vars[idKey]

	log.Infof("Deleting status page [%s]", id)

	w.Header().Set("Content-Type", "application/json")
	probe, err := a.statusPageSvc.GetByID(model.StatusPageId(id))

	if err != nil {
		log.Errorf("Fail to resolve status page by id [%s]", id)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing probe")
		return
	}

	// Is the probe exist ?
	if probe == nil {
		util.RespondWithError(w, http.StatusNotFound, "Status page with id "+id+" not found")
		return
	}

	deleteErr := a.statusPageSvc.DeleteByID(model.StatusPageId(id))

	if deleteErr != nil {
		log.Errorf("Fail to delete probe [%s] : %s", id, deleteErr)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing probe")
		return
	}

	util.RespondWithoutContent(w, http.StatusNoContent)
}
