package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
)

func RegisterAlertConfigResource(router *mux.Router, provider *dao.Provider) {
	handler := newAlertConfigHandler(provider)
	router.HandleFunc("/alert-config", handler.CreateAlertConfig).Methods("POST")
	router.HandleFunc("/alert-config", handler.SearchAlertConfig).Methods("GET")
	router.HandleFunc("/alert-config/{id}", handler.GetAlertConfigById).Methods("GET")
	router.HandleFunc("/alert-config/{id}", handler.DeleteAlertConfig).Methods("DELETE")

}

type AlertConfigHandler struct {
	alertConfigDao dao.AlertConfigDAO
}

func newAlertConfigHandler(provider *dao.Provider) *AlertConfigHandler {

	return &AlertConfigHandler{
		alertConfigDao: provider.GetAlertConfigDao(),
	}
}

// give the status page configuration
func (a *AlertConfigHandler) GetAlertConfigById(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	id, err := GetUrlParamString("id", w, r)
	if err != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Status page id is mandatory")
	}

	board, err := a.alertConfigDao.GetByID(model.AlertConfigId(id))
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get alertConfig")
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(board)

}
func (a *AlertConfigHandler) CreateAlertConfig(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	var alertConfig model.AlertConfig
	b, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(b, &alertConfig)

	// Tolerate probe update...
	if alertConfig.Id == "" {
		alertConfig.Id = model.NewAlertId()
		alertConfig.Creation = time.Now()
		alertConfig.Update = time.Now()
	} else {
		alertConfig.Creation = time.Now()
	}

	result, err := a.alertConfigDao.Save(alertConfig)
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to create alertConfig")
		return
	}

	json.NewEncoder(w).Encode(result)

}

func (a *AlertConfigHandler) UpdateAlertConfig(w http.ResponseWriter, r *http.Request) {

}

func (a *AlertConfigHandler) SearchAlertConfig(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	query := model.SearchAlertConfigQuery{}
	alertConfigs, err := a.alertConfigDao.Search(query)
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to search alertConfig")
		return
	}
	json.NewEncoder(w).Encode(alertConfigs)
}

// Delete a probe by identifier
//
// DELETE /probe/{id}
//
// Responses:
// 200: Probe successfully deleted
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (a AlertConfigHandler) DeleteAlertConfig(w http.ResponseWriter, r *http.Request) {

	idKey := "id"
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Probe id is mandatory")
	}

	id := vars[idKey]

	log.Infof("Deleting status page [%s]", id)

	w.Header().Set("Content-Type", "application/json")
	probe, err := a.alertConfigDao.GetByID(model.AlertConfigId(id))

	if err != nil {
		log.Errorf("Fail to resolve status page by id [%s]", id)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing probe")
		return
	}

	// Is the probe exist ?
	if probe == nil {
		util.RespondWithError(w, http.StatusNotFound, "Status page with id "+id+" not found")
		return
	}

	deleteErr := a.alertConfigDao.DeleteByID(model.AlertConfigId(id))

	if deleteErr != nil {
		log.Errorf("Fail to delete probe [%s] : %s", id, deleteErr)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing probe")
		return
	}

	util.RespondWithoutContent(w, http.StatusNoContent)
}
