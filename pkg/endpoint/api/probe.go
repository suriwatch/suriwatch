package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
	"gitlab.com/suriwatch/suriwatch/pkg/service"
)

const (
	DEFAULT_SEARCH_SIZE             = 250
	DEFAULT_SEARCH_MINUTES_INTERVAL = 10
	DEFAULT_RATE_DAY_INTERVAL       = 1
)

func RegisterProbeResource(router *mux.Router, provider *dao.Provider) {
	probeHandler := newProbeHandler(provider)

	router.HandleFunc("/probes", probeHandler.SearchProbes).Methods("GET")
	router.HandleFunc("/probes", probeHandler.CreateProbe).Methods("POST")

	router.HandleFunc("/probes/_export", probeHandler.ExportProbes).Methods("GET")
	router.HandleFunc("/probes/_import", probeHandler.ImportProbes).Methods("POST")

	router.HandleFunc("/probes/{id}", probeHandler.UpdateProbe).Methods("PUT")
	router.HandleFunc("/probes/{id}", probeHandler.GetProbe).Methods("GET")
	router.HandleFunc("/probes/{id}", probeHandler.DeleteProbe).Methods("DELETE")
	router.HandleFunc("/probes/{id}/results", probeHandler.GetProbeResults).Methods("GET")
	router.HandleFunc("/probes/{id}/_histogram", probeHandler.GetProbeHistogram).Methods("GET")

	router.HandleFunc("/probes/{id}/availability", probeHandler.GetAvailabilityRate).Methods("GET")

}

type ProbeHandler struct {
	config            *config.Config
	probeDao          dao.ProbeDAO
	probeResultDao    dao.ProbeResultDAO
	eventDao          dao.EventDAO
	statusPageService *service.StatusPageService
}

func newProbeHandler(provider *dao.Provider) *ProbeHandler {

	return &ProbeHandler{
		config:            provider.GetConfig(),
		probeDao:          provider.GetProbeDao(),
		probeResultDao:    provider.GetProbeResultDao(),
		eventDao:          provider.GetEventDao(),
		statusPageService: service.NewStatusPageService(provider),
	}
}

// Give a probe by identifier
//
// GET /probe/{id}
//
// Responses:
// 200: Probe successfully return
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (a ProbeHandler) GetProbe(w http.ResponseWriter, r *http.Request) {

	idKey := "id"
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Probe id is mandatory")
	}

	id := vars[idKey]

	w.Header().Set("Content-Type", "application/json")
	probe, err := a.probeDao.GetByID(ProbeId(id))

	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "Fail to get probe")
		return
	}

	if probe == nil {
		util.RespondWithError(w, http.StatusNotFound, "Probe with ID "+id+" not found")
		return
	}

	json.NewEncoder(w).Encode(probe)
}

// Delete a probe by identifier
//
// DELETE /probe/{id}
//
// Responses:
// 200: Probe successfully deleted
// 404: Bad request
// 404: Probe does not exist
// 500: Fail to delete probe
//
func (a ProbeHandler) DeleteProbe(w http.ResponseWriter, r *http.Request) {

	idKey := "id"
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Probe id is mandatory")
	}

	id := vars[idKey]

	log.Infof("Deleting probe [%s]", id)

	w.Header().Set("Content-Type", "application/json")
	probe, err := a.probeDao.GetByID(ProbeId(id))

	if err != nil {
		log.Errorf("Fail to resolve probe by id %s", id)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing probe")
		return
	}

	// Is the probe exist ?
	if probe == nil {
		util.RespondWithError(w, http.StatusNotFound, "Probe with id "+id+" not found")
		return
	}

	deleteResultsErr := a.probeResultDao.DeleteByProbeId(ProbeId(id))

	if deleteResultsErr != nil {
		log.Errorf("Fail to delete probe [%s] results : %s", id, deleteResultsErr)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing probe")
		return
	}

	deleteErr := a.probeDao.DeleteByID(ProbeId(id))

	if deleteErr != nil {
		log.Errorf("Fail to delete probe [%s] : %s", id, deleteErr)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while removing probe")
		return
	}

	util.RespondWithoutContent(w, http.StatusNoContent)
}

// Create a probe
//
// POST /probe
//
// Responses:
// 200: Probe created
//
func (obj ProbeHandler) CreateProbe(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	var probe Probe
	b, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(b, &probe)

	// Tolerate probe update...
	if probe.Id == "" {
		probe.Id = NewProbeId()
		probe.Creation = time.Now()
	} else {
		probe.Update = time.Now()
	}

	obj.probeDao.Save(probe)

	json.NewEncoder(w).Encode(probe)

}

// Update a probe
//
// PUT /probe/{id}
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (obj ProbeHandler) UpdateProbe(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	idKey := "id"
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Probe id is mandatory")
	}

	id := vars[idKey]
	previousProbe, err := obj.probeDao.GetByID(ProbeId(id))

	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while updating probe")
		return
	}

	if previousProbe == nil {
		util.RespondWithError(w, http.StatusNotFound, "Probe with id "+id+" not found")
		return
	}

	var probe Probe
	b, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(b, &probe)

	probe.Id = previousProbe.Id
	probe.Creation = previousProbe.Creation
	probe.Update = time.Now()
	obj.probeDao.Save(probe)

	j, err := json.Marshal(probe)
	if err != nil {
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while updating probe")
		return
	}

	w.Write(j)

}

// Give a probe execution result
//
// GET /probe/{id}/results
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (a ProbeHandler) GetProbeResults(w http.ResponseWriter, r *http.Request) {

	var defaultSize = DEFAULT_SEARCH_SIZE
	size, error := util.GetQueryParamInt("size", &defaultSize, w, r)
	if error != nil {
		return
	}

	var defaultTo = time.Now()
	to, error := util.GetQueryParamDate("to", &defaultTo, w, r)
	if error != nil {
		return
	}

	var defaultFrom = (*to).Add(time.Duration(-DEFAULT_SEARCH_MINUTES_INTERVAL) * time.Minute)
	from, error := util.GetQueryParamDate("from", &defaultFrom, w, r)
	if error != nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	id := vars["id"]
	probe, err := a.probeResultDao.GetProbeResult(ProbeId(id), *size, from, to)

	if err != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Invalid probe ID")
		return
	}
	json.NewEncoder(w).Encode(probe)
}

// Give a probe results histogram
//
// GET /probe/{id}/_histogram
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (a ProbeHandler) GetProbeHistogram(w http.ResponseWriter, r *http.Request) {

	var defaultFrom = "now-10m"
	from, error := util.GetQueryParamString("from", &defaultFrom, w, r)
	if error != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Invalid 'from' date")
		return
	}

	var defaultTo = "now"
	to, error := util.GetQueryParamString("to", &defaultTo, w, r)
	if error != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Invalid 'to' date")
		return
	}

	interval, error := util.GetQueryParamString("interval", nil, w, r)
	if error != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Invalid interval")
		return
	}

	node, error := util.GetQueryParamString("node", nil, w, r)
	if error != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Invalid node Name")
		return
	}

	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	id := vars["id"]
	probe, err := a.probeResultDao.GetProbeHistogram(ProbeId(id), node, from, to, interval)

	if err != nil {
		log.Errorf("Error getting probe [%s] histogram : %s", id, err)
		util.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Error getting probe [%s] histogram", id))

		return
	}
	json.NewEncoder(w).Encode(probe)
}

// Give all probes matching
//
// GET /probes
//
//parameters:
// - name: labels
//   description: probe labels filters. (ex: app:demo,group:test)
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (obj ProbeHandler) SearchProbes(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	searchQuery := SearchProbeQuery{}
	var probes []Probe
	var err error

	// Build labels
	serializedLabels := r.URL.Query().Get("labels")
	if serializedLabels != "" {
		searchQuery.Labels = util.DeserializeLabels(serializedLabels)
	}

	// Build name query
	probeName := r.URL.Query().Get("name")
	if probeName != "" {
		searchQuery.Name = &probeName
	}

	// Build state query
	serializedState := r.URL.Query().Get("state")
	if serializedState != "" {
		state := ProbeState(serializedState)
		searchQuery.State = &state
	}

	// Build type query
	serializedType := r.URL.Query().Get("type")
	if serializedType != "" {
		pType := ProbeType(serializedType)
		searchQuery.Type = &pType
	}

	var defaultSize = DEFAULT_SEARCH_SIZE
	size, error := util.GetQueryParamInt("size", &defaultSize, w, r)
	if error != nil {
		return
	}
	searchQuery.Size = size

	// Build Fulltext query
	fullTextQuery := r.URL.Query().Get("q")
	if fullTextQuery != "" {
		searchQuery.Query = &fullTextQuery
	}

	probes, err = obj.probeDao.Search(searchQuery)

	if err != nil {
		log.Error("Error while searching probes", err)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while searching probes")
		return
	}

	json.NewEncoder(w).Encode(probes)

}

// Give last probes results for the given interval
//
// GET /results/_last
//
//parameters:
// - name: size
//   description: maximum number of probes evaluated
// - name: from
//   description: begin timestamp for the search interval
//   default: to - 10m
// - name: to
//   description: end timestamp for the search interval
//   default: now()
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (obj ProbeHandler) GetLastProbesResult(w http.ResponseWriter, r *http.Request) {

	log.Debug("GetLastProbesResult request")

	var defaultSize = DEFAULT_SEARCH_SIZE
	size, error := util.GetQueryParamInt("size", &defaultSize, w, r)
	if error != nil {
		return
	}

	var defaultTo = time.Now()
	to, error := util.GetQueryParamDate("to", &defaultTo, w, r)
	if error != nil {
		return
	}

	var defaultFrom = (*to).Add(time.Duration(-DEFAULT_SEARCH_MINUTES_INTERVAL) * time.Minute)
	from, error := util.GetQueryParamDate("from", &defaultFrom, w, r)
	if error != nil {
		return
	}

	host, error := util.GetQueryParamString("host", nil, w, r)
	if error != nil {
		return
	}

	var labels = make(map[string]string)
	serializedLabels := r.URL.Query().Get("labels")

	if serializedLabels != "" {
		labels = util.DeserializeLabels(serializedLabels)
	}

	w.Header().Set("Content-Type", "application/json")
	criteria := SearchLastResultCriteria{
		From:   from,
		To:     to,
		Size:   size,
		Labels: labels,
		Host:   host,
	}
	probes, _ := obj.probeResultDao.GetLastProbesResult(criteria)

	json.NewEncoder(w).Encode(probes)
}

// Give last probes results for the given interval and grouped
//
// GET /results/_search
//
//parameters:
// - name: size
//   description: maximum number of probes evaluated
// - name: from
//   description: begin timestamp for the search interval
//   default: to - 10m
// - name: to
//   description: end timestamp for the search interval
//   default: now()
//
// Responses:
// 200: Probe successfully updated
// 404: Probe not found
// 500: An error occured
//
func (obj ProbeHandler) GetProbeStatus(w http.ResponseWriter, r *http.Request) {

	log.Debug("ProbeHandler GetProbeStatus")

	criteria := SearchAggregatedResultCriteria{}
	var error error

	var defaultSize = DEFAULT_SEARCH_SIZE
	criteria.Size, error = util.GetQueryParamInt("size", &defaultSize, w, r)
	if error != nil {
		return
	}

	var defaultTo = time.Now()
	criteria.To, error = util.GetQueryParamDate("to", &defaultTo, w, r)
	if error != nil {
		return
	}

	var defaultFrom = (*criteria.To).Add(time.Duration(-DEFAULT_SEARCH_MINUTES_INTERVAL) * time.Minute)
	criteria.From, error = util.GetQueryParamDate("from", &defaultFrom, w, r)
	if error != nil {
		return
	}

	// Get labels filtering options
	serializedLabels := r.URL.Query().Get("labels")
	if serializedLabels != "" {
		criteria.Labels = util.DeserializeLabels(serializedLabels)
	}

	// Get grouping options
	var groups []string
	serializedGroups := r.URL.Query().Get("groups")
	if serializedGroups != "" {
		groups = util.DeserializeGroups(serializedGroups)
	}

	w.Header().Set("Content-Type", "application/json")

	probes, _ := obj.probeResultDao.GetAggregatedLastProbesResult(groups, criteria)

	json.NewEncoder(w).Encode(probes)
}

func (obj ProbeHandler) ExportProbes(w http.ResponseWriter, r *http.Request) {

	log.Debug("ExportProbes request")

	w.Header().Set("Content-Type", "application/json")
	//w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=\"export.json\"")

	searchQuery := SearchProbeQuery{}

	size := 1000
	searchQuery.Size = &size
	var probes []Probe
	var err error

	// Build labels
	serializedLabels := r.URL.Query().Get("labels")
	if serializedLabels != "" {
		searchQuery.Labels = util.DeserializeLabels(serializedLabels)
	}

	probes, err = obj.probeDao.Search(searchQuery)

	if err != nil {
		log.Error("Error while searching probes", err)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while searching probes")
		return
	}

	json.NewEncoder(w).Encode(probes)

}

func (obj ProbeHandler) ImportProbes(w http.ResponseWriter, r *http.Request) {

	log.Debug("ImportProbes request")

	w.Header().Set("Content-Type", "application/json")

	var Buf bytes.Buffer
	// in your case file would be fileupload
	file, header, err := r.FormFile("file")
	if err != nil {
		log.Error("Error while getting file", err)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while reading uploaded file")
		return
	}

	defer file.Close()
	name := strings.Split(header.Filename, ".")
	fmt.Printf("File name %s\n", name[0])

	// Copy the file data to my buffer
	io.Copy(&Buf, file)

	// do something with the contents...
	// I normally have a struct defined and unmarshal into a struct, but this will
	// work as an example
	contents := Buf.String()
	fmt.Println(contents)

	// I reset the buffer in case I want to use it again
	// reduces memory allocations in more intense projects
	Buf.Reset()

	// Unserialize probe
	var probes = make([]Probe, 0)

	json.Unmarshal([]byte(contents), &probes)

	fmt.Println(len(probes))
	obj.probeDao.SaveBulk(probes)

	util.RespondWithoutContent(w, http.StatusNoContent)
	return

}

func (obj ProbeHandler) GetAvailabilityRate(w http.ResponseWriter, r *http.Request) {

	idKey := "id"
	vars := mux.Vars(r)

	if _, ok := vars[idKey]; !ok {
		util.RespondWithError(w, http.StatusBadRequest, "Probe id is mandatory")
	}

	id := vars[idKey]
	/*
		var defaultTo = time.Now()
		to, error := getQueryParamDate("to", &defaultTo, w, r)
		if error != nil {
			return
		}

		var defaultFrom = (*to).Add(time.Duration(-DEFAULT_RATE_DAY_INTERVAL) * time.Hour * 24)
		from, error := getQueryParamDate("from", &defaultFrom, w, r)
		if error != nil {
			return
		}*/

	var interval = "1d"
	var from = "now-1d"
	var to = "now"

	availability, err := obj.statusPageService.GetProbeAvailabilityRate(ProbeId(id), &from, &to, &interval)

	if err != nil {
		log.Error("Error while searching probes", err)
		util.RespondWithError(w, http.StatusInternalServerError, "An error occured while searching probes")
		return
	}

	json.NewEncoder(w).Encode(availability)

	return
}
