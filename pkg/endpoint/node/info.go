package node

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type InfosHandler struct {
	AppInfo model.AppInfo
}

func RegisterInfosResource(router *mux.Router, config *config.Config) {
	homeHandler := NewInfosHandler(config)
	router.HandleFunc("/", homeHandler.GeInfos)
}

func NewInfosHandler(config *config.Config) *InfosHandler {

	return &InfosHandler{
		AppInfo: config.AppInfo,
	}
}

func (obj *InfosHandler) GeInfos(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	json.NewEncoder(w).Encode(obj.AppInfo)

}
