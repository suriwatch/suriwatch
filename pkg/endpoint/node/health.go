package node

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

func RegisterHealthResource(router *mux.Router) {
	homeHandler := NewHealthHandler()
	router.HandleFunc("/health", homeHandler.GetInfos)
}

type HealthHandler struct {
}

func NewHealthHandler() *HealthHandler {

	return &HealthHandler{}
}

func (obj *HealthHandler) GetInfos(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	json.NewEncoder(w).Encode("TODO ")

}
