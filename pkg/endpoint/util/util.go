package util

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

func RespondWithoutContent(w http.ResponseWriter, code int) {
	w.WriteHeader(code)
}

func RespondWithError(w http.ResponseWriter, code int, message string) {
	RespondWithJSON(w, code, map[string]string{"error": message})
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func parseTimestamp(strTimestamp string) (*time.Time, error) {

	if len(strTimestamp) > 10 {
		strTimestamp = strTimestamp[0:10]
	}
	timestamp, err := strconv.ParseInt(strTimestamp, 10, 64)
	if err != nil {
		return nil, err
	}

	unixTimeUTC := time.Unix(timestamp, 0) //gives unix time stamp in utc
	fmt.Println(unixTimeUTC.Format(time.RFC1123))
	return &unixTimeUTC, nil

	//ISO-8601
	//unitTimeInRFC3339 :=unixTimeUTC.Format(time.RFC3339)

}

func GetQueryParamProbeType(name string, defaultVal *model.ProbeType, w http.ResponseWriter, r *http.Request) (*model.ProbeType, error) {
	// Build size query
	stringValue := r.URL.Query().Get(name)
	if stringValue != "" {
		probeType := model.ProbeType(stringValue)
		return &probeType, nil
	}
	return defaultVal, nil
}

func GetQueryParamProbeStatus(name string, defaultVal *model.ProbeResultStatus, w http.ResponseWriter, r *http.Request) (*model.ProbeResultStatus, error) {
	// Build size query
	stringValue := r.URL.Query().Get(name)
	if stringValue != "" {
		status := model.ProbeResultStatus(stringValue)
		return &status, nil
	}
	return defaultVal, nil
}

func GetQueryParamProbeId(name string, defaultVal *model.ProbeId, w http.ResponseWriter, r *http.Request) (*model.ProbeId, error) {
	// Build size query
	stringValue := r.URL.Query().Get(name)
	if stringValue != "" {
		probeId := model.ProbeId(stringValue)
		return &probeId, nil
	}
	return defaultVal, nil
}

func GetQueryParamString(name string, defaultVal *string, w http.ResponseWriter, r *http.Request) (*string, error) {
	// Build size query
	stringValue := r.URL.Query().Get(name)
	if stringValue != "" {
		return &stringValue, nil
	}
	return defaultVal, nil
}

func GetQueryParamInt(name string, defaultVal *int, w http.ResponseWriter, r *http.Request) (*int, error) {
	// Build size query
	stringValue := r.URL.Query().Get(name)
	if stringValue != "" {
		val, err := strconv.Atoi(stringValue)
		if err != nil {
			RespondWithError(w, http.StatusBadRequest, "Invalid "+name+" number")
			return nil, err
		}
		return &val, nil
	}
	return defaultVal, nil
}

func GetQueryParamDate(name string, defaultVal *time.Time, w http.ResponseWriter, r *http.Request) (*time.Time, error) {
	// Build size query
	stringValue := r.URL.Query().Get(name)
	if stringValue != "" {
		val, err := parseTimestamp(stringValue)
		if err != nil {
			RespondWithError(w, http.StatusBadRequest, "Invalid "+name+" timestamp")
			return nil, err
		}
		return val, nil
	}
	return defaultVal, nil
}

func DeserializeProbeIds(serializedValues string) []model.ProbeId {

	var res []model.ProbeId
	if serializedValues != "" {
		rawValues := strings.Split(serializedValues, ",")
		for _, rawValue := range rawValues {
			value := strings.TrimSpace(rawValue)
			if value != "" {
				res = append(res, model.ProbeId(value))
			}
		}
	}

	return res
}

func DeserializeGroups(serializedGroups string) []string {

	var labels []string
	if serializedGroups != "" {
		rawLabels := strings.Split(serializedGroups, ",")
		for _, rawLabel := range rawLabels {
			label := strings.TrimSpace(rawLabel)
			if label != "" {
				labels = append(labels, label)
			}
		}
	}

	return labels
}

// deserializeLabels convert serialized string label to map[string]string labels
//
// Param serializedLabels : String serialized format : app:test,type:api
//
func DeserializeLabels(serializedLabels string) map[string]string {
	var labels = make(map[string]string)

	if serializedLabels != "" {
		var splitedSerializedLabels = strings.Split(serializedLabels, ",")
		for _, splitedSerializedLabel := range splitedSerializedLabels {

			var splitedSerializedLabel = strings.Split(splitedSerializedLabel, ":")
			if len(splitedSerializedLabel) == 2 {
				labels[splitedSerializedLabel[0]] = splitedSerializedLabel[1]
			}

		}
	}

	return labels
}

// deserializeLabels convert serialized string label to map[string]string labels
//
// Param serializedLabels : String serialized format : app:test,type:api
//
func DeserializeSort(serializedSort string) []model.SortValue {
	var sorts []model.SortValue

	if serializedSort != "" {
		var splitedSerializedSorts = strings.Split(serializedSort, ",")
		for _, splitedSerializedSort := range splitedSerializedSorts {

			var splitedSerializedSort = strings.Split(splitedSerializedSort, ":")
			if len(splitedSerializedSort) == 2 {

				sort := model.SortValue{
					Name:  splitedSerializedSort[0],
					Order: model.SortOrder(strings.ToLower(splitedSerializedSort[1])),
				}
				sorts = append(sorts, sort)
			}

		}
	}

	return sorts
}
