package sentinel

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	. "gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/util"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/engine"
)

type ProbeHandler struct {
	engine   engine.ExecutorEngine
	probeDao ProbeDAO
}

func newProbeHandler(engine *engine.ExecutorEngine, probeDao ProbeDAO) *ProbeHandler {

	return &ProbeHandler{
		engine:   *engine,
		probeDao: probeDao,
	}
}

func (obj *ProbeHandler) ExecProbe(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["id"]

	w.WriteHeader(http.StatusOK)

	w.Header().Set("Content-Type", "application/json")
	probe, err := obj.probeDao.GetByID(ProbeId(id))
	result := obj.engine.CheckProbe(*probe)

	if err != nil {
		util.RespondWithError(w, http.StatusBadRequest, "Invalid probe ID")
		return
	}
	json.NewEncoder(w).Encode(result)
}

func RegisterNodeResource(router *mux.Router, provider *Provider, executor *engine.ExecutorEngine) {

	probeDao := provider.GetProbeDao()

	handler := newProbeHandler(executor, probeDao)
	router.HandleFunc("/probes/{id}/_exec", handler.ExecProbe).Methods("POST")
}
