package endpoint

import (
	"github.com/gorilla/mux"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"

	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/api"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint/sentinel"
	"gitlab.com/suriwatch/suriwatch/pkg/sentinel/engine"
)

// Register the REST API resources
func RegisterAPI(router *mux.Router, provider *dao.Provider) {

	api.RegisterHomeResource(router, provider)
	api.RegisterLabelResource(router, provider)
	api.RegisterProbeResource(router, provider)
	api.RegisterResultResource(router, provider)
	api.RegisterNodeResource(router, provider)
	api.RegisterEventResource(router, provider)
	api.RegisterStatusPageResource(router, provider)
	api.RegisterAlertConfigResource(router, provider)
	api.RegisterDataSourceResource(router, provider)

}

// Register the REST API resources
func RegisterSentinel(router *mux.Router, provider *dao.Provider, executor *engine.ExecutorEngine) {

	sentinel.RegisterNodeResource(router, provider, executor)

}
