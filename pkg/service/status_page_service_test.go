// +build integration_tests

package service

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

func init() {

}

func TestSplitEventsByInterval(t *testing.T) {

	svc := StatusPageService{}

	start := time.Date(2019, 11, 17, 10, 40, 8, 0, time.UTC)
	end := time.Date(2019, 11, 20, 05, 10, 3, 0, time.UTC)

	times := []time.Time{
		time.Date(2019, 11, 17, 3, 0, 0, 0, time.UTC),
		time.Date(2019, 11, 18, 10, 30, 40, 0, time.UTC),
		time.Date(2019, 11, 20, 4, 7, 2, 0, time.UTC),
		time.Date(2019, 11, 21, 10, 30, 0, 0, time.UTC),
	}

	events := []model.StatusChangeEvent{
		{
			Start:  times[0],
			End:    &times[1],
			Status: model.EVENT_STATUS_KO,
		},
		{
			Start:  times[1],
			End:    &times[2],
			Status: model.EVENT_STATUS_OK,
		},
		{
			Start:  times[2],
			End:    &times[3],
			Status: model.EVENT_STATUS_KO,
		},
	}

	interval := 24 * time.Hour

	buckets := svc.splitEventsByInterval(events, start, end, interval)

	for k, v := range buckets {
		fmt.Println(fmt.Sprintf("FOR : %s", k))
		for _, value := range v {
			fmt.Println(fmt.Sprintf("start=%s, end=%s, status=%s", value.Start, value.End, value.Status))
		}
	}

	//assert.NoError(t, err)
	//assert.NotEmpty(t, keys, "Labels not founds")
}

func TestSplitEventByInterval(t *testing.T) {

	svc := StatusPageService{}

	periods := []time.Time{
		time.Date(2019, 11, 17, 0, 0, 0, 0, time.UTC),
		time.Date(2019, 11, 18, 0, 0, 0, 0, time.UTC),
		time.Date(2019, 11, 19, 0, 0, 0, 0, time.UTC),
		time.Date(2019, 11, 20, 0, 0, 0, 0, time.UTC),
	}

	interval := 24 * time.Hour

	start := time.Date(2019, 11, 17, 10, 40, 8, 0, time.UTC)
	end := time.Date(2019, 11, 20, 05, 10, 3, 0, time.UTC)

	event := model.StatusChangeEvent{
		Start:  start,
		End:    &end,
		Status: model.EVENT_STATUS_KO,
	}

	buckets := make(map[time.Time][]model.StatusChangeEvent)

	svc.splitEventByInterval(event, periods, interval, buckets)

	for k, v := range buckets {
		fmt.Println(fmt.Sprintf("FOR : %s", k))
		for _, value := range v {
			fmt.Println(fmt.Sprintf("start=%s, end=%s", value.Start, value.End))
		}
	}

	//assert.NoError(t, err)
	//assert.NotEmpty(t, keys, "Labels not founds")
}

func TestComputeBucketKeys(t *testing.T) {

	svc := StatusPageService{}

	from := time.Date(2019, 11, 17, 0, 0, 0, 0, time.UTC)
	//from := time.Date(2019, 11, 17, 20, 34, 58, 651387237, time.UTC)
	//to := time.Date(2019, 12, 4, 20, 34, 58, 651387237, time.UTC)
	to := time.Date(2019, 12, 4, 0, 0, 0, 0, time.UTC)

	interval := 24 * time.Hour
	res := svc.computeBucketKeys(from, to, interval)

	for _, value := range res {
		fmt.Println(value)
	}
	//assert.NoError(t, err)
	//assert.NotEmpty(t, keys, "Labels not founds")
}
