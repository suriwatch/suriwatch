package service

import (
	"fmt"
	"sort"
	"time"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/util"
)

type StatusPageService struct {
	statusPageDao dao.StatusPageDAO
	probeDao      dao.ProbeDAO
	eventDao      dao.EventDAO
	resultDao     dao.ProbeResultDAO
}

var DEFAULT_STATUS_PAGE_SIZE = 1000

/**
 * Constructor for probeDat
 */
func NewStatusPageService(provider *dao.Provider) *StatusPageService {

	return &StatusPageService{
		statusPageDao: provider.GetStatusPageDao(),
		probeDao:      provider.GetProbeDao(),
		eventDao:      provider.GetEventDao(),
		resultDao:     provider.GetProbeResultDao(),
	}
}

//Save the given probe
func (s *StatusPageService) GetByID(id model.StatusPageId) (*model.StatusPage, error) {

	bo, error := s.statusPageDao.GetByID(id)
	if error != nil {
		log.Error(error)
		return nil, error
	}

	return bo, nil
}

//Save the given probe
func (s *StatusPageService) Save(board model.StatusPage) (*model.StatusPage, error) {

	// if it's a creation
	if board.Id == "" {
		board.Id = model.NewStatusPageId()
		board.Creation = time.Now()
	}

	board.Update = time.Now()

	bo, error := s.statusPageDao.Save(board)
	if error != nil {
		return nil, error
	}

	return bo, nil
}

func (s *StatusPageService) extractProbeIds(probes []model.Probe) []model.ProbeId {

	probeIds := []model.ProbeId{}
	for _, probe := range probes {
		probeIds = append(probeIds, probe.Id)
	}
	return probeIds
}

func (s *StatusPageService) getProbesByLabels(labels map[string]string) ([]model.Probe, error) {

	probeQuery := model.SearchProbeQuery{
		Labels: labels,
	}
	probeQuery.Size = &DEFAULT_STATUS_PAGE_SIZE

	probes, err := s.probeDao.Search(probeQuery)
	if err != nil {
		return nil, errors.Wrap(err, "Fail to get probe by labels")
	}

	return probes, nil
}

//Save the given probe
func (s *StatusPageService) GetStatus(id model.StatusPageId) (*model.StatusPageStatus, error) {

	statusPage, err := s.statusPageDao.GetByID(id)
	if err != nil {
		return nil, errors.Wrap(err, "Fail to get probe")
	}

	statusBoard := &model.StatusPageStatus{}
	statusBoard.StatusPage = statusPage

	// load probes
	probes, err := s.getProbesByLabels(statusPage.Labels)
	if err != nil {
		log.Error("Fail to get probes by labels", err)
		return nil, err
	}
	statusBoard.Probes = probes

	// load last results
	searchQuery := model.SearchLastResultCriteria{
		Labels: statusPage.Labels,
		Size:   &DEFAULT_STATUS_PAGE_SIZE,
	}
	probeResults, err := s.resultDao.GetLastProbesResult(searchQuery)
	statusBoard.Results = probeResults

	// load availability
	probeIds := s.extractProbeIds(probes)

	from := "now-8d"
	to := "now"
	for _, id := range probeIds {
		availability, err := s.GetProbeAvailabilityRate(id, &from, &to, nil)
		if err != nil {
			log.Error(err)
		} else {
			log.Error(availability)
			statusBoard.Availabilities = append(statusBoard.Availabilities, *availability)
		}
	}

	return statusBoard, nil

}

func (s *StatusPageService) Search(query model.SearchStatusPageQuery) ([]model.StatusPage, error) {

	return s.statusPageDao.Search(query)
}

// Give a probe availability for the given period.
// Sub detail can be provided with the interval
func (m *StatusPageService) GetProbeAvailabilityRate(probeId model.ProbeId, from *string, to *string, interval *string) (*model.ProbeAvailability, error) {

	query := new(model.SearchEventQuery)
	query.ProbeId = &probeId
	query.From = from
	query.To = to
	size := 1000
	query.Size = &size
	query.Sort = []model.SortValue{{Name: "start", Order: model.DESC}}

	var events, err = m.eventDao.Search(*query)
	if err != nil {
		log.Error("Fail to GetProbeAvailabilityRate", err)
		return nil, errors.Wrap(err, "Fail to GetProbeAvailabilityRate")
	}

	res := &model.ProbeAvailability{}
	res.ProbeId = probeId
	res.Ratio = m.calcRatio(events)

	// Computing bucket aggregation
	if interval != nil || true {

		interval := 24 * time.Hour
		fromTime := time.Now().Add(-6 * 24 * time.Hour)
		toTime := time.Now()

		bucketMap := m.splitEventsByInterval(events, fromTime, toTime, interval)

		for k, v := range bucketMap {
			bucket := &model.ProbeAvailabilityBucket{Key: k, Ratio: m.calcRatio(v)}
			res.Buckets = append(res.Buckets, bucket)
		}
		sort.Sort(res.Buckets)
	}

	return res, nil

}

func (s *StatusPageService) calcRatio(events []*model.StatusChangeEvent) float64 {

	var upTime time.Duration = 0
	var downTime time.Duration = 0

	for _, event := range events {

		fmt.Println(util.StructToString(event))

		begin := event.Start
		end := time.Now()

		if event.End != nil {
			end = *event.End
		}

		if event.Status == model.EVENT_STATUS_OK {
			upTime = upTime + end.Sub(begin)

		} else if event.Status == model.EVENT_STATUS_KO {

			if end.Before(begin) {
				fmt.Println("Invalid period " + event.Id)
			}
			downTime = downTime + end.Sub(begin)
		}
	}

	var totalTime = upTime.Seconds() + downTime.Seconds()

	if totalTime > 0 {
		// If it's the same, avoid useless round
		if totalTime == upTime.Seconds() {
			return 100.0
		} else {
			//round ratio
			ratio := float64(upTime.Seconds() * 100.0 / totalTime)
			return float64(int(ratio*1000)) / 1000.0
		}

	}

	return 0

}

//interval *string
func (s *StatusPageService) splitEventsByInterval(events []*model.StatusChangeEvent, from time.Time, to time.Time, interval time.Duration) map[time.Time][]*model.StatusChangeEvent {

	aggTimes := s.computeBucketKeys(from, to, interval)

	buckets := make(map[time.Time][]*model.StatusChangeEvent)

	for _, key := range aggTimes {
		fmt.Println(key)
		buckets[key] = []*model.StatusChangeEvent{}
	}

	for _, event := range events {
		if event.End == nil {
			now := time.Now()
			event.End = &now
		}
		s.splitEventByInterval(event, aggTimes, interval, buckets)
	}

	return buckets

}

//,
func (m *StatusPageService) splitEventByInterval(event *model.StatusChangeEvent, periods []time.Time, duration time.Duration, buckets map[time.Time][]*model.StatusChangeEvent) {

	lastDate := periods[len(periods)-1].Add(duration)

	newPeriods := append(periods, lastDate)

	for periodIdx, period := range periods {

		// Define period interval
		periodBegin := period
		periodEnd := newPeriods[periodIdx+1]

		if m.isEventInInterval(event, periodBegin, periodEnd) {

			beginInPeriod := m.isDateInInterval(event.Start, periodBegin, periodEnd)
			endInPeriod := m.isDateInInterval(*event.End, periodBegin, periodEnd)

			currentEvent := &model.StatusChangeEvent{
				Id:      event.Id,
				Type:    event.Type,
				Reason:  event.Reason,
				Start:   periodBegin,
				End:     &periodEnd,
				Status:  event.Status,
				ProbeId: event.ProbeId,
			}

			if beginInPeriod {
				currentEvent.Start = event.Start
			}

			if endInPeriod {
				currentEvent.End = event.End
			}
			duration := int(currentEvent.End.Sub(currentEvent.Start).Seconds())
			currentEvent.Duration = &duration

			if values, ok := buckets[period]; ok {
				buckets[period] = append(values, currentEvent)
			} else {
				buckets[period] = []*model.StatusChangeEvent{currentEvent}
			}
		}
	}

}

func (m *StatusPageService) computeBucketKeys(from time.Time, to time.Time, interval time.Duration) []time.Time {

	var res = make([]time.Time, 0, 0)

	// find the first bucket key
	var current = from.Truncate(interval)
	if current.After(from) {
		current = current.Add(-interval)
	}

	for current.Before(to) || current.Equal(to) {
		res = append(res, current)
		current = current.Add(interval)
	}

	return res
}

func (m *StatusPageService) isDateInInterval(date time.Time, from time.Time, to time.Time) bool {

	res := (from.Before(date) || from.Equal(date)) && (to.After(date) || to.Equal(date))

	return res
}

func (s *StatusPageService) isEventInInterval(event *model.StatusChangeEvent, from time.Time, to time.Time) bool {
	return s.isDateInInterval(event.Start, from, to) ||
		s.isDateInInterval(*event.End, from, to) ||
		(s.isDateInInterval(from, event.Start, *event.End) && s.isDateInInterval(to, event.Start, *event.End))
}

// Extract a event sub period from the given event
func (m *StatusPageService) extractEventBucket(event *model.StatusChangeEvent, from *time.Time, to *time.Time) (*model.StatusChangeEvent, error) {

	result := &model.StatusChangeEvent{
		Id:       event.Id,
		Type:     event.Type,
		Start:    event.Start,
		End:      event.End,
		Duration: nil,
		Reason:   event.Reason,
		Status:   event.Status,
		ProbeId:  event.ProbeId,
	}

	// Check if event is in period
	if !m.isEventInInterval(event, *from, *to) {
		return nil, errors.New("Event is not in the given period")
	}

	if event.Start.Before(*from) {
		result.Start = *from
	}
	if event.End.After(*to) {
		result.End = to
	}

	duration := int(result.End.Sub(result.Start).Seconds())
	result.Duration = &duration

	return result, nil
}

func (sps *StatusPageService) DeleteByID(id model.StatusPageId) error {

	err := sps.statusPageDao.DeleteByID(id)

	if err != nil {
		return errors.Wrap(err, "Fail to delete status page by id")
	}
	return nil
}
