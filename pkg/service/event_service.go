package service

import (
	"fmt"
	"math"
	"sort"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
)

type EventService struct {
	probeDao dao.ProbeDAO
	eventDao dao.EventDAO
}

/**
 * Constructor for probeDat
 */
func NewEventService(provider *dao.Provider) *EventService {

	return &EventService{
		eventDao: provider.GetEventDao(),
	}
}

type EventRepairStatus struct {
	Status string
	Diff   *EventsDiff
	Result []*model.StatusChangeEvent
}

// Extract a event sub period from the given event
func (m *EventService) ProbeEventRepair(id model.ProbeId) (*EventRepairStatus, error) {

	querySize := 10000
	autoClose := false

	var res = &EventRepairStatus{}

	events, err := m.eventDao.Search(model.SearchEventQuery{
		ProbeId:   &id,
		Size:      &querySize,
		AutoClose: &autoClose,
		Sort: []model.SortValue{
			{Name: "start", Order: model.DESC},
		},
	})

	if err != nil {
		return nil, errors.Wrap(err, "Fail to get events")
	}

	var diffClose *EventsDiff
	events, diffClose, err = m.autoCloseEvents(events)

	if err != nil {
		res.Status = "Fail to auto-close events"
		res.Diff = diffClose
		res.Result = events
		return res, errors.Wrap(err, "Fail to auto-close events")
	}
	fmt.Printf("%s", diffClose)

	var diffMerge *EventsDiff
	events, diffMerge, err = m.mergeEvents(events)
	if err != nil {
		return nil, errors.Wrap(err, "Fail to merge events")
	}
	fmt.Printf("%s", diffMerge)

	/*
		var diff = newEventsDiff();
		diff.removed = diffMerge.removed
		diff.updated = diffMerge.updated

		for id, event := range diffClose.updated {
			if _, ok := diffMerge.removed[id]; !ok {
				diff.updated[id] = event
			}
		}

		fmt.Printf("%s" , diff)
	*/

	res.Result = events
	res.Status = "OK"

	return res, nil

}

type EventsDiff struct {
	created map[string]*model.StatusChangeEvent
	updated map[string]*model.StatusChangeEvent
	removed map[string]*model.StatusChangeEvent
}

func (b EventsDiff) String() string {
	return fmt.Sprintf("created:%s, updated:%s, removed:%s", b.created, b.updated, b.removed)
}

func newEventsDiff() *EventsDiff {
	return &EventsDiff{
		created: make(map[string]*model.StatusChangeEvent),
		updated: make(map[string]*model.StatusChangeEvent),
		removed: make(map[string]*model.StatusChangeEvent),
	}
}

func (m *EventService) autoCloseEvents(events []*model.StatusChangeEvent) ([]*model.StatusChangeEvent, *EventsDiff, error) {

	nbEvents := len(events)

	diff := newEventsDiff()

	// If no events, nothing to do
	if nbEvents == 0 {
		return nil, diff, nil
	}

	// sort events in descending order
	sort.SliceStable(events, func(i, j int) bool {
		return events[i].Start.After(events[j].Start)
	})

	var previousStart = events[0].Start

	for i := 1; i < nbEvents; i++ {
		event := events[i]

		if event.End == nil {
			end := previousStart
			event.End = &end
			var duration = int(math.Round(event.End.Sub(event.Start).Seconds()))
			event.Duration = &duration

			diff.updated[event.Id] = event
		}
		previousStart = events[i].Start
	}

	if verfifErr := m.verifyEvents(events); verfifErr != nil {
		return events, diff, errors.Wrap(verfifErr, "Invalid event chronology")
	}

	return events, diff, nil

}

func (m *EventService) mergeEvents(events []*model.StatusChangeEvent) ([]*model.StatusChangeEvent, *EventsDiff, error) {

	nbEvents := len(events)
	var res []*model.StatusChangeEvent
	diff := &EventsDiff{}

	// If no events, nothing to do
	if nbEvents == 0 {
		return nil, nil, nil
	}

	var previousEvent = events[0]
	res = append(res, previousEvent)

	for i := 1; i < nbEvents; i++ {
		event := events[i]

		// Same status an contiguous event ? merge it
		if previousEvent.Start.Equal(*event.End) &&
			previousEvent.Status == event.Status {

			// Merge events
			previousEvent.Start = event.Start

			// Save changes
			//diff.removed[event.Id] = event
			//sdiff.updated[previousEvent.Id] = previousEvent
		} else {
			res = append(res, event)
			previousEvent = event
		}
	}

	if verfifErr := m.verifyEvents(res); verfifErr != nil {
		return events, diff, errors.Wrap(verfifErr, "Invalid event chronology")
	}
	return events, diff, nil
}

func (m *EventService) verifyEvents(events []*model.StatusChangeEvent) error {

	nbEvents := len(events)

	// If no events, nothing to do
	if nbEvents == 0 {
		return nil
	}

	var previousStart = events[0].Start

	for i := 1; i < nbEvents; i++ {

		event := events[i]

		if event.End == nil {
			// Event not finish and not the last
			return errors.New("Event with no end found")
		}
		if event.Start.After(*event.End) {
			// Illegal event status
			return errors.New(fmt.Sprintf("Illegal event state. Start %s is after end %s", event.Start.Format(time.RFC3339), event.End.Format(time.RFC3339)))
		}

		if event.Start.After(previousStart) {
			return errors.New(fmt.Sprintf("Illegal events chronology. Previous Start %s is after start %s", event.Start.Format(time.RFC3339), previousStart.Format(time.RFC3339)))
		}

		if previousStart.Before(*event.End) {
			return errors.New(fmt.Sprint("Illegal overlaping found"))
		}
		previousStart = events[i].Start
	}
	return nil
}
