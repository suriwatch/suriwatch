package client

import (
	"context"

	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

type Client interface {

	// Search probes by criteria
	SearchProbes(criteria *ProbeCriteria, ctx context.Context) ([]*model.Probe, error)

	// Get components
	GetProbe(id string, ctx context.Context) (*model.Probe, error)

	// Create a probe
	CreateProbe(probe model.Probe, ctx context.Context) (*model.Probe, error)


	// update a probe
	UpdateProbe(probe model.Probe, ctx context.Context) (*model.Probe, error)


	// delete probe
	DeleteProbe(id string, ctx context.Context) error



	// search results
	SearchResults(groups []string, criteria model.SearchAggregatedResultCriteria,  ctx context.Context) (*model.ProbeResultGroup, error)


}

type ProbeCriteria struct {

}


type ResultCriteria struct {

}