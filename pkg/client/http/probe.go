package http

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/suriwatch/suriwatch/pkg/client"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

func (c *Client) SearchProbes(criteria *client.ProbeCriteria, ctx context.Context) ([]*model.Probe, error) {

	probes := make([]*model.Probe, 0)

	resp, err := c.httpClient.Get(c.config.ApiUrl + "/probes")

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	jsonErr := json.Unmarshal(body, &probes)
	if jsonErr != nil {
		c.config.DebugLogger("Fail to unmarshall json probe: %s", jsonErr)
		return nil, err
	}

	return probes, nil
}

func (c *Client) GetProbe(id string, ctx context.Context) (*model.Probe, error) {

	probe := &model.Probe{}

	resp, err := c.httpClient.Get(c.config.ApiUrl + "/probes/" + id)

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	jsonErr := json.Unmarshal(body, &probe)
	if jsonErr != nil {
		c.config.DebugLogger("Fail to unmarshall json probe: %s", jsonErr)
		return nil, err
	}

	return probe, nil
}

// Create a probe
func (c *Client) CreateProbe(probe model.Probe, ctx context.Context) (*model.Probe, error) {

	dataBytes, err := json.Marshal(&probe)
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := c.httpClient.Post(c.config.ApiUrl+"/probes", "application/json", bytes.NewBuffer(dataBytes))

	c.traceResponse(resp)
	if err != nil {
		log.Fatalln(err)
	}

	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	var probeRes model.Probe
	jsonErr := json.Unmarshal(body, &probeRes)
	if jsonErr != nil {
		c.config.DebugLogger("Fail to unmarshall json probe: %s", jsonErr)
		return nil, jsonErr
	}

	return &probeRes, nil
}

func (c *Client) DeleteProbe(id string, ctx context.Context) error {

	// Create request
	req, err := http.NewRequest("DELETE", c.config.ApiUrl+"/probes/"+id, nil)
	if err != nil {
		c.config.DebugLogger("Fail to create delete request: %s", err)
		return err
	}

	// Fetch Request
	resp, err := c.httpClient.Do(req)
	if err != nil {
		c.config.DebugLogger("Fail to execute delete probe request: %s", err)
		return err
	}
	defer resp.Body.Close()

	// Read Response Body
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		c.config.DebugLogger("Fail to execute delete probe request: %s", err)
		return err
	}

	// Display Results
	c.config.DebugLogger("response Status : ", resp.Status)
	c.config.DebugLogger("response Headers : ", resp.Header)
	c.config.DebugLogger("Response body : ", string(respBody))

	// If not success family code
	if (resp.StatusCode < 200) || (300 <= resp.StatusCode) {
		return errors.New("Fail to delete probe. Http service respond with status code " + resp.Status)
	}

	return nil
}

func (c *Client) UpdateProbe(probe model.Probe, ctx context.Context) (*model.Probe, error) {

	dataBytes, err := json.Marshal(&probe)
	if err != nil {
		log.Fatalln(err)
	}

	url := c.config.ApiUrl + "/probes/" + string(probe.Id)
	request, err := http.NewRequest("PUT", url, bytes.NewBuffer(dataBytes))
	resp, err := c.httpClient.Do(request)

	c.traceResponse(resp)
	if err != nil {
		log.Fatalln(err)
	}

	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	var probeRes model.Probe
	jsonErr := json.Unmarshal(body, &probeRes)
	if jsonErr != nil {
		c.config.DebugLogger("Fail to unmarshall json probe: %s", jsonErr)
		return nil, jsonErr
	}

	return &probeRes, nil
}
