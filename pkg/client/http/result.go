package http

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

func (c *Client) SearchResults(groups []string, criteria model.SearchAggregatedResultCriteria, ctx context.Context) (*model.ProbeResultGroup, error) {

	// Results
	var resultGroups *model.ProbeResultGroup

	req, err := http.NewRequest("GET", c.config.ApiUrl+"/results/_search", nil)
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}
	q := req.URL.Query()

	if len(criteria.Labels) > 0 {
		q.Add("labels", c.serializeLabels(criteria.Labels))
	}
	if len(groups) > 0 {
		q.Add("groups", strings.Join(groups, ","))
	}

	req.URL.RawQuery = q.Encode()

	c.traceRequest(req)

	// Fetch Request
	resp, err := c.httpClient.Do(req)

	c.traceResponse(resp)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	jsonErr := json.Unmarshal(body, &resultGroups)
	if jsonErr != nil {
		return nil, err
	}

	return resultGroups, nil
}

func (c *Client) serializeLabels(labels map[string]string) string {

	var strLabels []string

	for k, v := range labels {
		strLabels = append(strLabels, k+":"+v)
	}
	return strings.Join(strLabels, ",")

}
