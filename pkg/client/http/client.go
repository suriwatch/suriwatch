package http

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"regexp"
	"strings"
	"time"

	suriwatch "gitlab.com/suriwatch/suriwatch/pkg/client"
)

const (
	HeaderNameContentType      = "Content-Type"
	HeaderNameAccept           = "Accept"
	HeaderValueApplicationJSON = "application/json"
)

type DebugLogger func(message string, args ...interface{})

type Config struct {
	ApiUrl             string
	DebugHttp          bool
	DebugLogger        DebugLogger
	InsecureSkipVerify bool
}

type Client struct {
	httpClient *http.Client
	config     *Config
}

func NewClient(config *Config) suriwatch.Client {

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: config.InsecureSkipVerify},
	}

	return &Client{
		httpClient: &http.Client{
			Timeout:   time.Second * 10,
			Transport: tr,
		},
		config: config,
	}
}

// handle common http code error messages
func (c *Client) handleInvalidhttpCode(resp http.Response) error {
	switch resp.StatusCode {
	case http.StatusUnauthorized:
		return errors.New("unauthorized by API server")
	case http.StatusForbidden:
		return errors.New("forbidden by API server (you are probably missing access rights)")
	case http.StatusBadRequest:
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		return errors.New(fmt.Sprintf("invalid request to API server (probably due to some input value)\nbody: %s", string(bodyBytes)))
	case http.StatusNotFound:
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		return errors.New(fmt.Sprintf("not found in API server\nbody: %s", string(bodyBytes)))
	case http.StatusServiceUnavailable:
		return errors.New("API server currently down")
	case http.StatusInternalServerError:
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		return errors.New(fmt.Sprintf("an error happened on the API server\nbody: %s", string(bodyBytes)))
	default:
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		return errors.New(fmt.Sprintf("an unknown error happened while requesting the API server\ncode: %d\nbody: %s", resp.StatusCode, string(bodyBytes)))
	}
}

// shortcut to exec and trace req and resp
func (c *Client) execAndTrace(reqTitle string, req *http.Request) (*http.Response, error) {
	if c.config.DebugHttp {
		c.config.DebugLogger("HTTP %s", reqTitle)
		c.traceRequest(req)
		start := time.Now()
		resp, err := c.httpClient.Do(req)
		elapsed := time.Since(start)
		c.config.DebugLogger("[ HTTP RESPONSE TIME: %s ]", elapsed)
		if resp != nil {
			c.traceResponse(resp)
		}
		return resp, err
	} else {
		return c.httpClient.Do(req)
	}
}

// trace request only if debug mode
func (c *Client) traceRequest(req *http.Request) {
	if c.config.DebugHttp {
		dump, err := httputil.DumpRequestOut(req, true)
		if err == nil {
			c.config.DebugLogger(dump2string(dump, "> "))
		} else {
			c.config.DebugLogger("ERROR: cannot trace request string: %s", err.Error())
		}
	}
}

// trace response only if debug mode
func (c *Client) traceResponse(resp *http.Response) {
	if c.config.DebugHttp {
		dump, err := httputil.DumpResponse(resp, true)
		if err == nil {
			c.config.DebugLogger(dump2string(dump, "< "))
		} else {
			c.config.DebugLogger("ERROR: cannot trace request string: %s", err.Error())
		}
	}
}

var (
	textBeginningRegex = regexp.MustCompile("^")
	newLineRegex       = regexp.MustCompile("\n")
)

func dump2string(dump []byte, prefix string) string {
	dumpStr := string(dump)
	dumpStr = strings.Trim(dumpStr, " \r\n\t")
	dumpStr = textBeginningRegex.ReplaceAllString(dumpStr, prefix)
	dumpStr = newLineRegex.ReplaceAllString(dumpStr, "\n"+prefix)
	return dumpStr
}
