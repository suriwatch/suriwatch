package model

import (
	"github.com/rs/xid"
	"time"
)

type DataSourceId string

func NewDataSourceId() DataSourceId {
	return DataSourceId(xid.New().String())
}

type DataSource struct {

	Id DataSourceId `json:"id,omitempty"`
	Name string		`json:"name,omitempty"`
	Type string		`json:"type,omitempty"`

	URL  string		`json:"url,omitempty"`

	Created time.Time
	Updated time.Time
}


type SearchDataSourceQuery struct {
	DataSourceId *DataSourceId `json:"dataSourceId,omitempty"`
	Name         string        `json:"name,omitempty"`
}