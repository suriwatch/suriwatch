package model

import (
	"github.com/rs/xid"
	"time"
)

type StatusPageId string

func NewStatusPageId() StatusPageId {
	return StatusPageId(xid.New().String())
}

type StatusPageStatus struct {
	StatusPage     *StatusPage         `json:"statusPage,omitempty"`
	Probes         []Probe             `json:"probes,omitempty"`
	Results        []ProbeResult       `json:"results"`
	Availabilities []ProbeAvailability `json:"availabilities"`
}

type StatusPage struct {
	Id          StatusPageId      `json:"id,omitempty"`
	Name        string            `json:"name,omitempty"`
	Description string            `json:"description,omitempty"`
	Creation    time.Time         `json:"creation,omitempty"`
	Update      time.Time         `json:"update,omitempty"`
	Labels      map[string]string `json:"labels,omitempty"`
	ProbeIds    []*ProbeId        `json:"probeIds,omitempty"`
}

type SearchStatusPageQuery struct {
	StatusPageId *StatusPageId `json:"statusPageId,omitempty"`
	Name         string        `json:"name,omitempty"`
}

type ProbeAvailability struct {
	ProbeId ProbeId `json:"probeId,omitempty"`
	Ratio   float64 `json:"ratio"`

	Buckets BucketSlice `json:"buckets"`
}

type ProbeAvailabilityBucket struct {
	Key   time.Time `json:"key"`
	Ratio float64   `json:"ratio"`
}

type BucketSlice []*ProbeAvailabilityBucket

func (p BucketSlice) Len() int {
	return len(p)
}

func (p BucketSlice) Less(i, j int) bool {
	return p[i].Key.After(p[j].Key)
}

func (p BucketSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
