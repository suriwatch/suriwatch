package model

type NetworkProbeConfig struct {
	Request NetworkRequest `json:"request"`
}

type NetworkRequest struct {
	Port     int    `json:"port,omitempty"`
	Target   string `json:"target,omitempty"`
	Protocol string `json:"protocol,omitempty"`
	Timeout  int    `json:"timeout,omitempty"`
}
