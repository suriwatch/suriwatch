package model

type VersionInfo struct {
	Version string `json:"version,omitempty"`

	BuildMetadata string `json:"buildMetadata,omitempty"`
	GitCommit     string `json:"gitCommit,omitempty"`
	GitTreeState  string `json:"gitTreeState,omitempty"`

	BuildDate string
}
