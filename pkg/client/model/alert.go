package model

import (
	"time"

	"github.com/rs/xid"
)

type NotifType string

const (
	UP_AND_DOWN NotifType = "UP_AND_DOWN"
)

type AlertConfigId string

func NewAlertId() AlertConfigId {
	return AlertConfigId(xid.New().String())
}

type AlertState string

const (
	ALERT_STARTED AlertState = "STARTED"
	ALERT_PAUSED             = "PAUSED"
)

type AlertConfig struct {
	Id        AlertConfigId `json:"id"`
	Name      string        `json:"name"`
	Type      AlertType     `json:"type"`
	NotifType NotifType     `json:"notifType"`
	Interval  int           `json:"interval,omitempty"`
	State     AlertState    `json:"state,omitempty"`

	Labels   map[string]string `json:"labels,omitempty"`
	Creation time.Time         `json:"creation,omitempty"`
	Update   time.Time         `json:"update,omitempty"`
}

type Alert struct {
	AlertConfig string
}

type AlertType string

const (
	EMAIL AlertType = "EMAIL"
	SLACK AlertType = "SLACK"
)

type Channel struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type SearchAlertConfigQuery struct {
	Size *int `json:"size,omitempty"`
}
