package model

type HTTPProbeConfig struct {
	Request  HTTPRequest  `json:"request,omitempty"`
	Response HTTPResponse `json:"response,omitempty"`
}

type HTTPRequest struct {
	URL     string       `json:"url,omitempty"`
	Verb    string       `json:"verb,omitempty"`
	Content string       `json:"content,omitempty"`
	Headers []HTTPHeader `json:"headers,omitempty"`
	Timeout int          `json:"timeout,omitempty"`
}

type HTTPResponse struct {
	HttpCode *int    `json:"httpCode,omitempty"`
	Keyword  *string `json:"keyword,omitempty"`
}

type HTTPHeader struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type HTTPStats struct {
	DNSLookup        int `json:"DNSLookup,omitempty"`
	TCPConnection    int `json:"TCPConnection,omitempty"`
	TLSHandshake     int `json:"TLSHandshake,omitempty"`
	ServerProcessing int `json:"serverProcessing,omitempty"`
	ContentTransfert int `json:"contentTransfert,omitempty"`
	Total            int `json:"total,omitempty"`
}
