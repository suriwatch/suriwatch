package model

import (
	"time"
)

type AppInfo struct {
	Name    string       `json:"name,omitempty"`
	Type    string       `json:"type,omitempty"`
	Version *VersionInfo `json:"version,omitempty"`
	Started time.Time    `json:"started,omitempty"`
}
