package model

import "time"

type StatusPacket struct {
	Node        string            `json:"node,omitempty"`
	Host        string            `json:"host,omitempty"`
	Date        time.Time         `json:"date,omitempty"`
	StartedAt   time.Time         `json:"startedAt,omitempty"`
	Mode        string            `json:"mode,omitempty"`
	Labels      map[string]string `json:"labels,omitempty"`
	ProbesCount int               `json:"probesCount,omitempty"`
}

type HistoNodeQuery struct {
	Size    *int       `json:"size,omitempty"`
	From    *time.Time `json:"from,omitempty"`
	To      *time.Time `json:"to,omitempty"`
	ProbeId *string    `json:"probeId,omitempty"`
}

type NodeInfo struct {
	Name string `json:"name,omitempty"`
}
