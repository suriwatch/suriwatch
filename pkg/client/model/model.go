package model

import (
	"bytes"
	"encoding/json"
	"github.com/rs/xid"
	"time"
)

type ProbeId string

func NewProbeId() ProbeId {
	return ProbeId(xid.New().String())
}

type ProbeType string

const (
	HTTP    ProbeType = "http"
	NETWORK           = "network"
	TSDB			  = "tsdb"
)

type ProbeState string

const (
	STARTED ProbeState = "STARTED"
	PAUSED             = "PAUSED"
)

type SortOrder string

const (
	ASC  SortOrder = "asc"
	DESC           = "desc"
)

type RunMode string

const (
	CLUSTER RunMode = "CLUSTER"
	AGENT           = "AGENT"
)

type Probe struct {
	Id          ProbeId             `json:"id"`
	Name        string              `json:"name"`
	Type        ProbeType           `json:"type"`
	Labels      map[string][]string `json:"labels,omitempty"`
	Creation    time.Time           `json:"creation,omitempty"`
	Update      time.Time           `json:"update,omitempty"`
	Interval    int                 `json:"interval,omitempty"`
	State       ProbeState          `json:"state,omitempty"`
	Description string              `json:"description,omitempty"`
	Mode        RunMode             `json:"mode,omitempty"`

	// HTTP probe configuration
	HttpProbeConfig *HTTPProbeConfig `json:"httpProbeConfig,omitempty"`

	// Network probe configuration
	NetworkProbeConfig *NetworkProbeConfig `json:"networkProbeConfig,omitempty"`

	// TimeSeries Database probe configuration
	TSDBProbeConfig *TSDBProbeConfig `json:"tsdbProbeConfig,omitempty"`


	// Http status probe
	HTTPStatusProbeConfig *HTTPStatusProbeConfig `json:"httpStatusProbeConfig,omitempty"`
}

type ProbeResultStatus string

const (
	OK ProbeResultStatus = "OK"
	KO ProbeResultStatus = "KO"
	FAIL ProbeResultStatus = "FAIL"
)

type EventStatus string

const (
	EVENT_STATUS_OK      EventStatus = "OK"
	EVENT_STATUS_KO      EventStatus = "KO"
	EVENT_STATUS_STOPPED EventStatus = "STOPPED"
)

type ProbeResult struct {
	ProbeId   ProbeId             `json:"probeId"`
	Name      string              `json:"name"`
	Type      ProbeType           `json:"type,omitempty"`
	Labels    map[string][]string `json:"labels,omitempty"`
	Status    ProbeResultStatus   `json:"status,omitempty"`
	Infos     string              `json:"infos,omitempty"`
	Duration  int                 `json:"duration,omitempty"`
	Date      time.Time           `json:"date,omitempty"`
	Host      string              `json:"host,omitempty"`
	Target    string              `json:"target,omitempty"`
	Node      string              `json:"node,omitempty"`
	Mode      RunMode             `json:"mode,omitempty"`
	HTTPStats HTTPStats           `json:"HTTPStats,omitempty"`
	Message   *string             `json:"message,omitempty"`
}

type ProbeResultGroup struct {
	Name   string              `json:"name,omitempty"`
	Status ProbeResultStatus   `json:"status,omitempty"`
	Groups []*ProbeResultGroup `json:"groups,omitempty"`
	Datas  []*ProbeResult      `json:"datas,omitempty"`
}

type LabelReference struct {
	Name   string   `json:"name,omitempty"`
	Values []string `json:"values,omitempty"`
}

type SearchProbeQuery struct {
	Name   *string           `json:"name,omitempty"`
	Type   *ProbeType        `json:"type,omitempty"`
	Size   *int              `json:"size,omitempty"`
	State  *ProbeState       `json:"state,omitempty"`
	Query  *string           `json:"query,omitempty"`
	Labels map[string]string `json:"labels,omitempty"`
	Mode   *string           `json:"mode,omitempty"`
}

type SearchLastResultCriteria struct {
	ProbeIds []ProbeId          `json:"probeIds,omitempty"`
	Name     *string            `json:"name,omitempty"`
	Type     *ProbeType         `json:"type,omitempty"`
	Size     *int               `json:"size,omitempty"`
	Status   *ProbeResultStatus `json:"status,omitempty"`
	Query    *string            `json:"query,omitempty"`
	Labels   map[string]string  `json:"labels,omitempty"`
	Host     *string            `json:"host,omitempty"`
	From     *time.Time         `json:"from,omitempty"`
	To       *time.Time         `json:"to,omitempty"`
}

type SearchResultQuery struct {
	Ids    []string           `json:"ids,omitempty"`
	Name   *string            `json:"name,omitempty"`
	Type   *ProbeType         `json:"type,omitempty"`
	Size   *int               `json:"size,omitempty"`
	Status *ProbeResultStatus `json:"status,omitempty"`
	Query  *string            `json:"query,omitempty"`
	Labels map[string]string  `json:"labels,omitempty"`
	From   *time.Time         `json:"from,omitempty"`
	To     *time.Time         `json:"to,omitempty"`
}

type SearchAggregatedResultCriteria struct {
	ProbeIds []ProbeId          `json:"probeIds,omitempty"`
	Name     *string            `json:"name,omitempty"`
	Type     *ProbeType         `json:"type,omitempty"`
	Size     *int               `json:"size,omitempty"`
	Status   *ProbeResultStatus `json:"status,omitempty"`
	State    *ProbeState        `json:"state,omitempty"`
	Query    *string            `json:"query,omitempty"`
	Labels   map[string]string  `json:"labels,omitempty"`
	Host     *string            `json:"host,omitempty"`
	From     *time.Time         `json:"from,omitempty"`
	To       *time.Time         `json:"to,omitempty"`
}

type ProbeResultHistogram struct {
	Data []HistoDateBucket `json:"data,omitempty"`
}

type HistoDateBucket struct {
	Status    map[ProbeResultStatus]int64 `json:"status,omitempty"`
	TotalTime *StatsMesure                `json:"totalTime,omitempty"`
	Date      *time.Time                  `json:"date,omitempty"`
}

type StatsMesure struct {
	Avg *float64 `json:"avg,omitempty"`
	Max *float64 `json:"max,omitempty"`
}

type StatusChangeEvent struct {
	Id       string      `json:"id,omitempty"`
	Type     string      `json:"type,omitempty"`
	Start    time.Time   `json:"start,omitempty"`
	End      *time.Time  `json:"end,omitempty"`
	Duration *int        `json:"duration,omitempty"`
	Reason   *string     `json:"reason,omitempty"`
	Status   EventStatus `json:"status,omitempty"`
	ProbeId  ProbeId     `json:"probeId,omitempty"`
	Node     string      `json:"node,omitempty"`
	Mode     string      `json:"mode,omitempty"`
	Labels   map[string][]string `json:"labels,omitempty"`
}

func (c StatusChangeEvent) String() string {
	buffer := new(bytes.Buffer)
	enc := json.NewEncoder(buffer)
	enc.SetIndent("", "\t")
	if err := enc.Encode(c); err != nil {
		panic(err)
	}
	return buffer.String()
}

type PeriodStatus struct {
	Type     string              `json:"type,omitempty"`
	Start    time.Time           `json:"start,omitempty"`
	End      time.Time           `json:"end,omitempty"`
	Reason   *string             `json:"reason,omitempty"`
	Status   *string             `json:"status,omitempty"`
	ProbeId  ProbeId             `json:"probeId,omitempty"`
	Duration time.Duration       `json:"duration,omitempty"`
	Labels   map[string][]string `json:"labels,omitempty"`
}

type SearchEventQuery struct {
	ProbeId   *ProbeId          `json:"probeId,omitempty"`
	Size      *int              `json:"size,omitempty"`
	From      *string           `json:"from,omitempty"`
	To        *string           `json:"to,omitempty"`
	Labels    map[string]string `json:"labels,omitempty"`
	Sort      []SortValue       `json:"sort,omitempty"`
	Node      *string           `json:"node,omitempty"`
	Mode      *RunMode          `json:"mode,omitempty"`
	AutoClose *bool             `json:"autoClose,omitempty"`
}

type SortValue struct {
	Name  string
	Order SortOrder
}

type SearchPeriodQuery struct {
	ProbeId *ProbeId          `json:"probeId,omitempty"`
	Size    *int              `json:"size,omitempty"`
	From    *time.Time        `json:"from,omitempty"`
	To      *time.Time        `json:"to,omitempty"`
	Labels  map[string]string `json:"labels,omitempty"`
}
