package model


type TSDBProbeConfig struct {
	Condition Condition `json:"condition,omitempty"`
}

type Condition struct {
	Query    *Query    `json:"query,omitempty"`
	Reduce   *Reduce   `json:"reduce,omitempty"`
	Evaluate *Evaluate `json:"evaluate,omitempty"`
}

type Query struct {
	DataSource string `json:"dataSource,omitempty"`
	From       string `json:"from,omitempty"`
	To         string `json:"to,omitempty"`
	Step       string `json:"step,omitempty"`
	Raw        string `json:"raw,omitempty"`
}

type ReduceType string

type Reduce struct {
	Type ReduceType `json:"type,omitempty"`
}

type EvaluateType string

type Evaluate struct {
	Type EvaluateType `json:"type,omitempty"`

	Config map[string]interface{} `json:"config,omitempty"`
}
