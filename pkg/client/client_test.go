// +build integration_tests

package client_test

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	suriwatch "gitlab.com/suriwatch/suriwatch/pkg/client/http"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

func Printf(format string, args ...interface{}) {
	fmt.Printf(format, args...)
}

func TestGetProbe(t *testing.T) {

	config := &suriwatch.Config{
		ApiUrl: "https://suri.watch",
	}

	client := suriwatch.NewClient(config)

	probes, err := client.SearchProbes(nil, nil)
	fmt.Println(err)

	out, err := json.Marshal(probes)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(out))
}

func TestSearchProbes(t *testing.T) {

	config := &suriwatch.Config{
		ApiUrl: "https://suri.watch",
	}

	client := suriwatch.NewClient(config)

	ctxt := context.Background()
	probeId := "bjdkjeh3q75a48l5erdg"

	probes, err := client.GetProbe(probeId, ctxt)
	fmt.Println(err)

	out, err := json.Marshal(probes)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(out))
}

func TestCreateProbes(t *testing.T) {

	config := &suriwatch.Config{
		ApiUrl:      "https://suri.watch",
		DebugHttp:   true,
		DebugLogger: Printf,
	}

	client := suriwatch.NewClient(config)

	ctxt := context.Background()

	httpRequest := model.HTTPRequest{
		URL:  "http://www.demo.com",
		Verb: "GET",
	}

	httpProbeConfig := model.HTTPProbeConfig{
		Request: httpRequest,
	}

	probeIn := model.Probe{
		Name:            "turbine-poc01",
		Type:            model.HTTP,
		Interval:        10,
		Description:     "toto",
		State:           model.PAUSED,
		HttpProbeConfig: &httpProbeConfig,
		Labels: map[string][]string{
			"debug": {"true"},
		},
	}

	probeOut, err := client.CreateProbe(probeIn, ctxt)
	if err != nil {
		panic(err)
	}
	print(probeOut)

}


func TestSearchResults(t *testing.T){

	config := &suriwatch.Config{
		ApiUrl:"https://suriwatch-api.prod.demo.fr",
		DebugHttp: true,
		DebugLogger: logrus.Printf,
	}

	client := suriwatch.NewClient(config);

	//groups = []string{ "turbine-component" }
	var groups = []string{ "labels.env" }
	criteria := model.SearchAggregatedResultCriteria{}
	criteria.Labels = make(map[string]string)
	//criteria.Labels["turbine-component"] = "demo"


	//criteria.Labels["application"] = "site-degraded"

	ctxt := context.Background()
	labelGroups, err :=  client.SearchResults( groups, criteria, ctxt )
	if err != nil {
		panic (err)
	}
	print(labelGroups)

}


func print(probe interface{}){

	out, err := json.Marshal(probe)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(out))
}
