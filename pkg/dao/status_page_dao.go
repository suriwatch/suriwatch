package dao

import "gitlab.com/suriwatch/suriwatch/pkg/client/model"

type StatusPageDAO interface {

	// Get a probe by identifier
	GetByID(id model.StatusPageId) (*model.StatusPage, error)

	//Save the given probe
	Save(probe model.StatusPage) (*model.StatusPage, error)

	// Delete a probe by identifier
	DeleteByID(id model.StatusPageId) error

	// Search dashboard method
	Search(query model.SearchStatusPageQuery) ([]model.StatusPage, error)
}
