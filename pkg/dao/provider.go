package dao

import (
	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/config"
	daoElastic6 "gitlab.com/suriwatch/suriwatch/pkg/dao/elastic6"
	daoInfluxdb "gitlab.com/suriwatch/suriwatch/pkg/dao/influxdb"
)

type Provider struct {
	Config *Config
}

func (m *Provider) GetConfig() *Config {
	return m.Config
}


func (m *Provider) GetAlertConfigDao() AlertConfigDAO {

	switch storage := m.Config.ConfigStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.ConfigStorage.Elastic
		return AlertConfigDAO(daoElastic6.NewAlertConfigDAO(config))

	default:
		log.Fatalf("Unsupported config storage provider [%s]", storage)
		return nil
	}

}

func (m *Provider) GetLabelDao() LabelDAO {

	switch storage := m.Config.ConfigStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.ConfigStorage.Elastic
		return LabelDAO(daoElastic6.NewLabelDAO(config))

	default:
		log.Fatalf("Unsupported config storage provider [%s]", storage)
		return nil
	}

}
func (m *Provider) GetStatusPageDao() StatusPageDAO {

	switch storage := m.Config.ConfigStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.ConfigStorage.Elastic
		return StatusPageDAO(daoElastic6.NewStatusPageDAO(config))
	default:
		log.Fatalf("Unsupported config storage provider [%s]", storage)
		return nil
	}
}

func (m *Provider) GetProbeDao() ProbeDAO {

	switch storage := m.Config.ConfigStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.ConfigStorage.Elastic
		return ProbeDAO(daoElastic6.NewProbeDAO(config))

	case INFLUXDB:

		config := m.Config.ConfigStorage.Influxdb
		return ProbeDAO(daoInfluxdb.NewInfluxProbeDAO(config))
	default:
		log.Fatalf("Unsupported config storage provider [%s]", storage)
		return nil
	}
}

func (m *Provider) GetProbeResultDao() ProbeResultDAO {

	switch storage := m.Config.DataStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.DataStorage.Elastic
		return ProbeResultDAO(daoElastic6.NewProbeResultDAO(config))

	case INFLUXDB:

		config := m.Config.DataStorage.Influxdb
		return ProbeResultDAO(daoInfluxdb.NewInfluxProbeResultDAO(config))
	default:
		log.Fatalf("Unsupported data storage provider [%s]", storage)
		return nil
	}

}

func (m *Provider) GetNodeDao() NodeDAO {

	switch storage := m.Config.DataStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.DataStorage.Elastic
		return NodeDAO(daoElastic6.NewNodeDAO(config))

	default:
		log.Fatalf("Unsupported data storage provider [%s]", storage)
		return nil
	}
}

func (m *Provider) GetEventDao() EventDAO {

	switch storage := m.Config.DataStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.DataStorage.Elastic
		return EventDAO(daoElastic6.NewEventDAO(config))

	default:
		log.Fatalf("Unsupported data storage provider [%s]", storage)
		return nil
	}

}

func (m *Provider) GetDataSourceDao() DataSourceDAO {

	switch storage := m.Config.DataStorage.Provider; storage {
	case ELASTIC:
		config := m.Config.DataStorage.Elastic
		return DataSourceDAO(daoElastic6.NewDataSourceDAO(config))

	default:
		log.Fatalf("Unsupported data storage provider [%s]", storage)
		return nil
	}

}

