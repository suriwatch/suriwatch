package dao

import (
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

type ProbeDAO interface {

	// Get a probe by identifier
	GetByID(id ProbeId) (*Probe, error)

	//Save the given probe
	Save(probe Probe) (*Probe, error)

	//Save multiple probes
	SaveBulk(probes []Probe) ([]Probe, error)

	// Delete a probe by identifier
	DeleteByID(id ProbeId) error

	// Search probes by matching labels.
	// The labels maps contain label as key en expected label value as map value
	Search(query SearchProbeQuery) ([]Probe, error)
}
