package dao

import (
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

type AlertConfigDAO interface {

	// Save alert config
	Save(alert model.AlertConfig) (*model.AlertConfig, error)

	// Get a probe by identifier
	GetByID(id model.AlertConfigId) (*model.AlertConfig, error)

	// Delete a probe by identifier
	DeleteByID(id model.AlertConfigId) error

	// Search dashboard method
	Search(query model.SearchAlertConfigQuery) ([]model.AlertConfig, error)
}
