package dao

import (
	"time"

	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

type NodeDAO interface {

	// Send current agent "alive" status
	SendStatus(packet model.StatusPacket) error

	SearchStatus(size *int, from *time.Time, to *time.Time) ([]model.StatusPacket, error)

	SearchNodeHisto(query model.HistoNodeQuery) ([]model.NodeInfo, error)
}
