package dao

import (
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

type EventDAO interface {

	// Send current agent "alive" status
	Save(event model.StatusChangeEvent) error

	SaveBulk(result []model.StatusChangeEvent) ([]model.StatusChangeEvent, error)

	Search(query model.SearchEventQuery) ([]*model.StatusChangeEvent, error)
}
