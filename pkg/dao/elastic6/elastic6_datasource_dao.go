package dao

import (
	"context"
	"fmt"
	"github.com/olivere/elastic"
	"reflect"

	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type Elastic6DataSourceDAO struct {
	Elastic6DAO
}

/**
 * Constructor for probeDat
 */
func NewDataSourceDAO(config config.Elastic) *Elastic6DataSourceDAO {

	dao := &Elastic6DataSourceDAO{
		Elastic6DAO{
			Url:    config.Url,
			Type:   "datasource",
			Index:  "suriwatch-datasource",
			client: nil,
			debug:  config.Debug,
		},
	}

	dao.Elastic6DAO.GetId = dao.GetId
	dao.Elastic6DAO.GetIndex = dao.GetIndex
	dao.Elastic6DAO.GetEntityType = dao.GetEntityType

	return dao
}

func (dao *Elastic6DataSourceDAO) GetIndex(entity interface{}) string {
	return dao.Index
}

func (dao *Elastic6DataSourceDAO) GetId(entity interface{}) *string {
	id := entity.(model.DataSource).Id

	strId := string(id)
	return &strId
}
func (dao *Elastic6DataSourceDAO) GetEntityType() interface{} {
	return &model.DataSource{}
}

func (dao *Elastic6DataSourceDAO) GetByID(id model.DataSourceId) (*model.DataSource, error) {

	entity, err := dao.Elastic6DAO.GetById(string(id))

	if entity == nil {
		logrus.Info(fmt.Sprintf("Status page with id %s not found", string(id)))
		return nil, nil
	}
	return entity.(*model.DataSource), err

}

//Save the given probe
func (dao *Elastic6DataSourceDAO) Save(ds model.DataSource) (*model.DataSource, error) {
	id, error := dao.Elastic6DAO.Save(ds, false)

	if error != nil {
		return nil, error
	}

	ds.Id = model.DataSourceId(*id)
	return &ds, nil

}

// Delete a probe by identifier
func (dao *Elastic6DataSourceDAO) DeleteByID(id model.DataSourceId) error {
	return dao.Delete(string(id), false)
}



// Search dashboard method
func (dao *Elastic6DataSourceDAO) Search(query model.SearchDataSourceQuery) ([]model.DataSource, error) {


	client, err := dao.getClient()

	if err != nil {
		logrus.Error("Fail to get elastic client", err)
		return nil, err
	}

	indexSuffix := "" //-*"

	searchService := client.Search().Index(dao.Index + indexSuffix)

	var queries []elastic.Query


	if query.Name != "" {
		queries = append(queries, elastic.NewTermQuery("name", query.Name))
	}
	booleanQuery := elastic.NewBoolQuery()
	booleanQuery.Must(queries...)


	searchService.AllowNoIndices(true)
	searchService.IgnoreUnavailable(true)

	// Executing query
	ctx := context.Background()
	searchResult, err := searchService.Query(booleanQuery).Do(ctx) // execute


	if err != nil {
		return nil, err
	}

	var result = make([]model.DataSource, len(searchResult.Hits.Hits))

	var ttype model.DataSource

	// Extract results (see: https://olivere.github.io/elastic/)
	for i, item := range searchResult.Each(reflect.TypeOf(ttype)) {
		if t, ok := item.(model.DataSource); ok {
			result[i] = t
		}
	}

	return result, nil
}


