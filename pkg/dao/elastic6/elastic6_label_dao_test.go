// +build integration_tests

package dao

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

func init() {
	deleteIndex("suriwatch")
	createIndex("suriwatch")
}

func TestGetLabelKeys(t *testing.T) {

	var dao = NewProbeDAO(config.Elastic{
		Url:   getEnv("ELASTICSEARCH_URL", "http://localhost:9200"),
		Index: "suriwatch",
	})

	var labelDao = NewLabelDAO(config.Elastic{
		Url:   getEnv("ELASTICSEARCH_URL", "http://localhost:9200"),
		Index: "suriwatch",
	})

	probeLabels := map[string][]string{
		"app":   {"demo"},
		"group": {"demo"},
	}

	probeName := "suriwatch-demo"
	dao.Save(model.Probe{
		Id:     model.NewProbeId(),
		Name:   probeName,
		Labels: probeLabels,
	})
	time.Sleep(1000 * time.Millisecond) // wait for indexation

	keys, err := labelDao.getLabelKeys()
	assert.NoError(t, err)

	assert.NotEmpty(t, keys, "Labels not founds")

}

func TestGetLabelReference(t *testing.T) {

	var dao = NewProbeDAO(config.Elastic{
		Url:   getEnv("ELASTICSEARCH_URL", "http://localhost:9200"),
		Index: "suriwatch",
	})

	var labelDao = NewLabelDAO(config.Elastic{
		Url:   getEnv("ELASTICSEARCH_URL", "http://localhost:9200"),
		Index: "suriwatch",
	})

	probeLabels := map[string][]string{
		"app":   {"demo"},
		"group": {"test"},
	}

	probeName := "suriwatch-demo"
	dao.Save(model.Probe{
		Id:     model.NewProbeId(),
		Name:   probeName,
		Labels: probeLabels,
	})

	time.Sleep(1000 * time.Millisecond) // wait for indexation
	labels, err := labelDao.GetLabels(true)

	assert.NoError(t, err)
	assert.NotEmpty(t, labels, "Reference Labels not founds")
}
