package model

import "gitlab.com/suriwatch/suriwatch/pkg/client/model"

/**
 * Generated with : https://mholt.github.io/json-to-go/
 */
type SearchResponse struct {
	Took     int  `json:"took"`
	TimedOut bool `json:"timed_out"`

	Shards struct {
		Total      int `json:"total"`
		Successful int `json:"successful"`
		Failed     int `json:"failed"`
	} `json:"_shards"`

	Hits struct {
		Total    int     `json:"total"`
		MaxScore float64 `json:"max_score"`
		Hits     []struct {
			Index  string      `json:"_index"`
			Type   string      `json:"_type"`
			ID     string      `json:"_id"`
			Score  float64     `json:"_score"`
			Source model.Probe `json:"_source"`
		} `json:"hits"`
	} `json:"hits"`
}

type SearchRequest struct {
	Size  int `json:"size"`
	Query struct {
		Match struct {
			Tags string `json:"tags"`
		} `json:"match"`
	} `json:"query"`
}

type GetResponse struct {
	Index   string      `json:"_index"`
	Type    string      `json:"_type"`
	ID      string      `json:"_id"`
	Version int         `json:"_version"`
	Found   bool        `json:"found"`
	Source  model.Probe `json:"_source"`
}
