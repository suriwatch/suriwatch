// +build integration_tests

package dao

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

func init() {
	deleteIndex("suriwatch")
	createIndex("suriwatch")
}

func TestGetAll(t *testing.T) {

	var dao *Elastic6ProbeDAO = NewProbeDAO(config.Elastic{
		Url:   getEnv("ELASTICSEARCH_URL", "http://localhost:9200"),
		Index: "suriwatch",
	})

	id := model.NewProbeId()

	dao.Save(model.Probe{
		Id:   id,
		Name: "test",
	})

	savedProbe, err := dao.GetByID(id)
	assert.Nil(t, err)
	assert.Equal(t, savedProbe.Id, id)
	assert.Equal(t, savedProbe.Name, "test")

	time.Sleep(1000 * time.Millisecond) // wait for indexation

	criteria := model.SearchProbeQuery{}
	var size = 250
	criteria.Size = &size
	probes, err := dao.Search(criteria)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(probes))

	err = dao.DeleteByID(id)
	assert.Nil(t, err)

	_, err = dao.GetByID(id)
	assert.Equal(t, "Probe not found", err.Error())

	time.Sleep(1000 * time.Millisecond) // wait for indexation

	probes, err = dao.Search(criteria)

	assert.Nil(t, err)
	assert.Equal(t, 0, len(probes))

}
