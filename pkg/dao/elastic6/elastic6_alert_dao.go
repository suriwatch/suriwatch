package dao

import (
	"fmt"
	"reflect"

	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type Elastic6AlertConfigDAO struct {
	Elastic6DAO
}

/**
 * Constructor for probeDat
 */
func NewAlertConfigDAO(config config.Elastic) *Elastic6AlertConfigDAO {

	dao := &Elastic6AlertConfigDAO{
		Elastic6DAO{
			Url:    config.Url,
			Type:   "alert-config",
			Index:  "suriwatch-alert-config",
			client: nil,
			debug:  config.Debug,
		},
	}

	dao.Elastic6DAO.GetId = dao.GetId
	dao.Elastic6DAO.GetIndex = dao.GetIndex
	dao.Elastic6DAO.GetEntityType = dao.GetEntityType

	return dao
}

func (dao *Elastic6AlertConfigDAO) GetIndex(entity interface{}) string {
	return dao.Index
}

func (dao *Elastic6AlertConfigDAO) GetId(entity interface{}) *string {
	id := entity.(model.AlertConfig).Id

	strId := string(id)
	return &strId
}
func (dao *Elastic6AlertConfigDAO) GetEntityType() interface{} {
	return &model.AlertConfig{}
}

func (dao *Elastic6AlertConfigDAO) GetByID(id model.AlertConfigId) (*model.AlertConfig, error) {

	entity, err := dao.Elastic6DAO.GetById(string(id))

	if entity == nil {
		logrus.Info(fmt.Sprintf("Alert config with id %s not found", string(id)))
		return nil, nil
	}
	return entity.(*model.AlertConfig), err

}

//Save the given probe
func (dao *Elastic6AlertConfigDAO) Save(statusPage model.AlertConfig) (*model.AlertConfig, error) {
	id, error := dao.Elastic6DAO.Save(statusPage, false)

	if error != nil {
		return nil, error
	}

	statusPage.Id = model.AlertConfigId(*id)
	return &statusPage, nil

}

// Delete a probe by identifier
func (dao *Elastic6AlertConfigDAO) DeleteByID(id model.AlertConfigId) error {
	return dao.Delete(string(id), false)
}

// Search dashboard method
func (dao *Elastic6AlertConfigDAO) Search(query model.SearchAlertConfigQuery) ([]model.AlertConfig, error) {

	searchResult, err := dao.Elastic6DAO.Search(SearchQuery{})
	if err != nil {
		return nil, err
	}

	var result = make([]model.AlertConfig, len(searchResult.Hits.Hits))

	var ttype model.AlertConfig

	// Extract results (see: https://olivere.github.io/elastic/)
	for i, item := range searchResult.Each(reflect.TypeOf(ttype)) {
		if t, ok := item.(model.AlertConfig); ok {
			result[i] = t
		}
	}

	return result, nil

}
