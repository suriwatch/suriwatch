package dao

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"time"

	"github.com/olivere/elastic"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	. "gitlab.com/suriwatch/suriwatch/pkg/util"
)

type ProbeResultDAO struct {
	Url    string
	Index  string
	Type   string
	client *elastic.Client
	debug  bool
}

/**
 * Constructor for probeDao
 */
func NewProbeResultDAO(config config.Elastic) *ProbeResultDAO {

	return &ProbeResultDAO{
		Url:   config.Url,
		Type:  "probe-data",
		Index: config.Index,
		debug: config.Debug,
	}
}

/**
 * Save a probe result
 */
func (m *ProbeResultDAO) Save(result ProbeResult) error {

	log.Infof("Saving probe result for [%s]", result.ProbeId)

	httpClient := http.Client{
		Timeout: time.Second * 2,
	}

	out, err := json.Marshal(result)
	if err != nil {
		log.Error("Fail to marshal probe result", err)
		return errors.Wrap(err, "Fail to marshall result")
	}

	var indexSuffix = result.Date.Format("-2006.01.02")

	req, err := http.NewRequest(http.MethodPost, m.Url+"/"+m.Index+indexSuffix+"/"+m.Type, bytes.NewBufferString(string(out)))
	if err != nil {
		log.Error("Fail to initialise request", err)
		return errors.Wrap(err, "Fail to initialize request")
	}

	req.Header.Add("Content-Type", "application/json")

	res, err := httpClient.Do(req)
	if err != nil {
		log.Error("Fail to execute request", err)
		return errors.Wrap(err, "Fail to execute request")
	}

	defer res.Body.Close()

	return nil
}

/**
 * Delete a probe result
 */
func (obj *ProbeResultDAO) DeleteByProbeId(id ProbeId) error {

	log.Infof("Deleting probe  [%s]", id)
	ctx := context.Background()

	client, err := obj.getClient()

	if err != nil {
		log.Errorf("Fail to delete probe with id [%s]", id)
		return err
	}

	var temQueries []elastic.Query
	temQueries = append(temQueries, elastic.NewTermQuery("probeId", id))

	booleanQuery := elastic.NewBoolQuery()
	booleanQuery.Must(temQueries...)

	deleteResult, err := client.DeleteByQuery().
		Index(obj.Index + "-*"). // search in index "twitter"
		Query(booleanQuery).     // specify the query
		Size(-1).
		Do(ctx) // execute

	if err != nil {
		// Handle error
		log.Errorf("Error deleting by query probe [%s] : %s", id, err)
		log.Debug(StructToString(deleteResult))

		return err
	}
	return nil

}

func (obj *ProbeResultDAO) getClient() (*elastic.Client, error) {

	if obj.client == nil {

		var err error = nil

		if obj.debug {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
				elastic.SetErrorLog(log.StandardLogger()),
				elastic.SetInfoLog(log.StandardLogger()),
				elastic.SetTraceLog(log.StandardLogger()),
			)
		} else {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
			)
		}

		if err != nil {
			log.Error("Fail to get elastic client", err)
			return nil, err
		}
	}

	return obj.client, nil
}

func (obj ProbeResultDAO) GetProbeResult(id ProbeId, size int, from *time.Time, to *time.Time) ([]ProbeResult, error) {

	log.Debugf("GetProbeResult for [%s]", id)
	ctx := context.Background()

	client, err := obj.getClient()

	if err != nil {
		log.Errorf("Fail to get probe [%s] results : %s", id, err)
		return nil, err
	}

	var temQueries []elastic.Query
	temQueries = append(temQueries, elastic.NewTermQuery("probeId", id))

	booleanQuery := elastic.NewBoolQuery()
	booleanQuery.Must(temQueries...)

	if from != nil || to != nil {
		rangeFilter := elastic.NewRangeQuery("date")

		if from != nil {
			rangeFilter.Gte(from)
		}
		if to != nil {
			rangeFilter.Lte(to)
		}

		booleanQuery.Filter(rangeFilter)
	}

	searchResult, err := client.Search().
		Index(obj.Index+"-*"). // search in index "twitter"
		Query(booleanQuery).
		Size(size). // specify the query
		Sort("date", false).
		Do(ctx) // execute

	var probeResults = []ProbeResult{}

	var ttyp ProbeResult
	for _, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if t, ok := item.(ProbeResult); ok {
			probeResults = append(probeResults, t)
		}
	}

	return probeResults, nil

}

func (obj ProbeResultDAO) getIndexesNames(indexPrefix string, from *time.Time, to *time.Time) string {

	//Todo list indexes
	return indexPrefix + "-*"

}

func (obj ProbeResultDAO) GetLastProbesResult(criteria SearchLastResultCriteria) ([]ProbeResult, error) {

	log.Debug("GetLastProbesResult")
	ctx := context.Background()

	client, err := obj.getClient()

	if err != nil {
		log.Error("Fail to get last probe results", err)
		return nil, err
	}

	searchService := client.Search()

	var queries []elastic.Query
	var filters []elastic.Query

	for k, v := range criteria.Labels {
		filters = append(filters, elastic.NewTermQuery("labels."+k, v))
	}
	if criteria.Host != nil {
		filters = append(filters, elastic.NewTermQuery("host", *criteria.Host))
	}
	if criteria.Status != nil {
		filters = append(filters, elastic.NewTermQuery("status", *criteria.Status))
	}

	if criteria.ProbeIds != nil {
		var interfaceSlice = make([]interface{}, len(criteria.ProbeIds))
		for i, d := range criteria.ProbeIds {
			interfaceSlice[i] = d
		}
		filters = append(filters, elastic.NewTermsQuery("probeId", interfaceSlice...))
	}
	booleanQuery := elastic.NewBoolQuery()

	if criteria.From != nil || criteria.To != nil {
		rangeFilter := elastic.NewRangeQuery("date")

		if criteria.From != nil {
			rangeFilter.Gte(criteria.From)
		}
		if criteria.To != nil {
			rangeFilter.Lte(criteria.To)
		}

		booleanQuery.Filter(rangeFilter)
	}

	if criteria.Query != nil {

		var strQuery = *criteria.Query
		simpleQuery := elastic.NewQueryStringQuery("*" + strQuery + "*")
		simpleQuery.Field("name")
		simpleQuery.Field("description")
		simpleQuery.Field("labels.*")

		queries = append(queries, simpleQuery)
	}

	if len(queries) > 0 {
		booleanQuery.Must(queries...)
	}
	booleanQuery.Filter(filters...)

	searchService.Query(booleanQuery)

	// Build aggregation
	probeAggregation := elastic.NewTermsAggregation()
	probeAggregation.Field("probeId")
	if criteria.Size != nil {
		probeAggregation.Size(*criteria.Size)
	}

	topHitAggregation := elastic.NewTopHitsAggregation()
	topHitAggregation.Size(1)
	topHitAggregation.Sort("date", false)

	probeAggregation.SubAggregation("agg_last", topHitAggregation)

	searchResult, err := searchService.
		Index(obj.getIndexesNames(obj.Index, criteria.From, criteria.To)). // search in index "twitter"
		Query(booleanQuery).                                               // specify the query
		Aggregation("agg_probes", probeAggregation).
		Size(0).
		Do(ctx) // execute

	if err != nil {
		// Handle error
		log.Error("ES6Dao.Error getting searchResult", err)
		fmt.Println(StructToString(searchResult))
		return nil, err
	}

	var ttyp ProbeResult
	var probeResults = []ProbeResult{}

	resAggregation, found := searchResult.Aggregations.Terms("agg_probes")

	if found {
		for _, bucket := range resAggregation.Buckets {
			topHitAggregationResult, topHitAggregationFounded := bucket.TopHits("agg_last")

			if topHitAggregationFounded {

				var slices = obj.each(reflect.TypeOf(ttyp), topHitAggregationResult)

				for _, slice := range slices {
					probeResults = append(probeResults, slice.(ProbeResult))
				}
			}
		}
	}

	return probeResults, nil
}

func (obj *ProbeResultDAO) each(typ reflect.Type, r *elastic.AggregationTopHitsMetric) []interface{} {
	if r.Hits == nil || r.Hits.Hits == nil || len(r.Hits.Hits) == 0 {
		return nil
	}
	var slice []interface{}
	for _, hit := range r.Hits.Hits {
		v := reflect.New(typ).Elem()
		if hit.Source == nil {
			slice = append(slice, v.Interface())
			continue
		}
		if err := json.Unmarshal(*hit.Source, v.Addr().Interface()); err == nil {
			slice = append(slice, v.Interface())
		}
	}
	return slice

}

func (obj *ProbeResultDAO) GetAggregatedLastProbesResult(groups []string, criteria SearchAggregatedResultCriteria) (*ProbeResultGroup, error) {

	log.Debug("GetAggregatedLastProbesResult")
	ctx := context.Background()

	client, err := obj.getClient()

	if err != nil {
		log.Error("Fail to GetAggregatedLastProbesResult", err)
		return nil, err
	}

	var queries []elastic.Query
	for k, v := range criteria.Labels {
		queries = append(queries, elastic.NewTermQuery("labels."+k, v))
	}

	if criteria.Name != nil {
		queries = append(queries, elastic.NewTermQuery("name.keyword", *criteria.Name))
	}
	if criteria.Type != nil {
		queries = append(queries, elastic.NewTermQuery("type", *criteria.Type))
	}
	if criteria.Status != nil {
		queries = append(queries, elastic.NewTermQuery("status", *criteria.Status))
	}
	if criteria.State != nil {
		queries = append(queries, elastic.NewTermQuery("state", *criteria.State))
	}
	if criteria.ProbeIds != nil {
		var interfaceSlice = make([]interface{}, len(criteria.ProbeIds))
		for i, d := range criteria.ProbeIds {
			interfaceSlice[i] = d
		}
		queries = append(queries, elastic.NewTermsQuery("probeId", interfaceSlice...))
	}

	if criteria.Query != nil {
		queries = append(queries, elastic.NewMultiMatchQuery(*criteria.Query, "name", "description", "labels.*"))
	}

	booleanQuery := elastic.NewBoolQuery()
	booleanQuery.Must(queries...)

	if criteria.From != nil || criteria.To != nil {
		rangeFilter := elastic.NewRangeQuery("date")

		if criteria.From != nil {
			rangeFilter.Gte(criteria.From)
		}
		if criteria.To != nil {
			rangeFilter.Lte(criteria.To)
		}

		booleanQuery.Filter(rangeFilter)
	}

	agg, err := obj.buildProbeStatusGroupsAggregate(groups, *criteria.Size, nil)
	if err != nil {
		log.Error("ES6Dao.Error getting GetAggregatedLastProbesResult", err)
		return nil, err
	}

	rootAggName := groups[0]
	searchResult, err := client.Search().
		Index(obj.Index+"-*"). // search in index "twitter"
		Query(booleanQuery).   // specify the query
		Aggregation(rootAggName, agg).
		Size(0).
		//Sort("name", true).
		Do(ctx) // execute

	if err != nil {
		// Handle error
		log.Error("Error getting GetLastProbesStatus", err)
		log.Debug(StructToString(searchResult))
		return nil, err
	}

	resAggregation, found := searchResult.Aggregations.Terms(rootAggName)

	// If result, map it
	if found {
		res := &ProbeResultGroup{}

		obj.buildResGrpFromAggItems(resAggregation, res, groups)
		return res, nil
	}
	// TODO deal with no result
	return nil, nil
}

func (obj *ProbeResultDAO) buildResGrpFromAggItems(agg *elastic.AggregationBucketKeyItems, parentGrp *ProbeResultGroup, termsGroups []string) ProbeResultStatus {

	status := ProbeResultStatus("")

	if len(termsGroups) >= 0 {

		for _, bucket := range agg.Buckets {
			grp := obj.buildResGrpFromAggItem(bucket, parentGrp, termsGroups[1:])
			status = obj.computeStatus(status, grp.Status)
		}
	}
	return status
}

func (obj *ProbeResultDAO) buildResGrpFromAggItem(agg *elastic.AggregationBucketKeyItem, parentGrp *ProbeResultGroup, termsGroups []string) *ProbeResultGroup {

	group := &ProbeResultGroup{}
	group.Name = agg.Key.(string)
	group.Status = ProbeResultStatus("")
	parentGrp.Groups = append(parentGrp.Groups, group)

	if len(termsGroups) >= 1 {

		aggName := termsGroups[0]
		childAgg, _ := agg.Terms(aggName)

		statusChilds := obj.buildResGrpFromAggItems(childAgg, group, termsGroups)
		group.Status = obj.computeStatus(statusChilds, group.Status)
	} else {

		topHitAggregationResult, _ := agg.TopHits("agg_last")

		var ttyp ProbeResult

		var slices = obj.each(reflect.TypeOf(ttyp), topHitAggregationResult)
		group.Datas = []*ProbeResult{}

		for _, slice := range slices {
			pr := slice.(ProbeResult)
			group.Datas = append(group.Datas, &pr)

			group.Status = obj.computeStatus(pr.Status, group.Status)
		}
	}
	return group
}

func (obj *ProbeResultDAO) computeStatus(probeStatus ProbeResultStatus, groupStatus ProbeResultStatus) ProbeResultStatus {

	// group is already down, can't be worst
	if groupStatus == KO {
		return KO
	}

	if probeStatus == "" {
		return groupStatus
	}

	return probeStatus

}

//
func (obj *ProbeResultDAO) buildProbeStatusGroupsAggregate(groups []string, limit int, parent *elastic.TermsAggregation) (*elastic.TermsAggregation, error) {

	if groups == nil || len(groups) <= 0 {
		return nil, fmt.Errorf("At least one group is required")
	}

	var rootAgg *elastic.TermsAggregation = nil
	var childAgg *elastic.TermsAggregation = nil

	rootAgg, childAgg = obj.buildGroupsAggregate(groups, limit, nil, nil)

	topHitAggregation := elastic.NewTopHitsAggregation()
	topHitAggregation.Size(1)
	topHitAggregation.Sort("date", false)
	childAgg.SubAggregation("agg_last", topHitAggregation)

	return rootAgg, nil
}

// Build cascade team aggregates
func (obj *ProbeResultDAO) buildGroupsAggregate(groups []string, limit int, root *elastic.TermsAggregation, parent *elastic.TermsAggregation) (*elastic.TermsAggregation, *elastic.TermsAggregation) {

	if len(groups) <= 0 {
		return root, parent
	}

	var term = groups[0]
	aggregation := elastic.NewTermsAggregation()
	aggregation.Field(term)
	aggregation.Size(limit)

	if root == nil {
		root = aggregation
	}
	if parent != nil {
		parent.SubAggregation(term, aggregation)
	}

	return obj.buildGroupsAggregate(groups[1:], limit, root, aggregation)
}

func (obj *ProbeResultDAO) GetDefaultInterval(from *string, to *string) string {

	var interval string = "30s"
	return interval
}

//Give the result date histogram for the given probe
func (obj *ProbeResultDAO) GetProbeHistogram(id ProbeId, node *string, from *string, to *string, interval *string) (*ProbeResultHistogram, error) {

	log.Debugf("GetProbeHistogram for [%s]", id)
	ctx := context.Background()

	client, err := obj.getClient()

	if err != nil {
		log.Error("Error getting client ", err)
		return nil, err
	}

	date_agg := elastic.NewDateHistogramAggregation()
	date_agg.Field("date")
	date_agg.MinDocCount(0)
	date_agg.Missing(0)

	// Aggregate status
	status_agg := elastic.NewTermsAggregation()
	status_agg.Field("status")
	date_agg.SubAggregation("agg_status", status_agg)

	// Aggregate response time
	total_time_agg := elastic.NewStatsAggregation()
	total_time_agg.Field("HTTPStats.total")
	date_agg.SubAggregation("agg_total_time", total_time_agg)

	var temQueries []elastic.Query
	temQueries = append(temQueries, elastic.NewTermQuery("probeId", id))

	if node != nil {
		//TODO refactor host to node
		temQueries = append(temQueries, elastic.NewTermQuery("host", &node))
	}

	booleanQuery := elastic.NewBoolQuery()
	booleanQuery.Must(temQueries...)

	if from != nil || to != nil {
		rangeFilter := elastic.NewRangeQuery("date")

		if from != nil {
			rangeFilter.Gte(from)
		}
		if to != nil {
			rangeFilter.Lte(to)
		}

		booleanQuery.Filter(rangeFilter)
	}

	// set aggregation interval
	if interval == nil {
		date_agg.Interval(obj.GetDefaultInterval(from, to))
	} else {
		date_agg.Interval(*interval)
	}

	searchResult, err := client.Search().
		Index(obj.Index+"-*"). // search in index "twitter"
		Query(booleanQuery).   // specify the query
		Aggregation("agg_date", date_agg).
		Size(0).
		Do(ctx) // execute

	if err != nil {
		// Handle error
		log.Error("ES6Dao.Error getting histogram", err)

		log.Error(StructToString(searchResult))

		return nil, err
	}

	dateAggregation, found := searchResult.Aggregations.Terms("agg_date")

	// If result, map it
	if found {
		res := &ProbeResultHistogram{}

		for _, dateBucket := range dateAggregation.Buckets {

			histoDateBucket := HistoDateBucket{}

			keyNumber, _ := dateBucket.KeyNumber.Int64()
			var sec = keyNumber / 1000           // Get date second
			var nsec = (keyNumber % 1000) * 1000 // Get date millis and convert to nano
			var time = time.Unix(sec, nsec)
			histoDateBucket.Date = &time

			// Build status aggragation
			histoDateBucket.Status = make(map[ProbeResultStatus]int64)
			statusAggregationResult, statusAggregationFounded := dateBucket.Terms("agg_status")
			if statusAggregationFounded {
				for _, statusBucket := range statusAggregationResult.Buckets {

					histoDateBucket.Status[ProbeResultStatus(statusBucket.Key.(string))] = statusBucket.DocCount
					//histoDateBucket.Status = append(histoDateBucket.Status, histoStatusBucket)
				}
			}

			// Build status aggragation
			totalTimeAggResult, totalTimeAggFounded := dateBucket.Stats("agg_total_time")

			if totalTimeAggFounded {
				histoDateBucket.TotalTime = &StatsMesure{}
				histoDateBucket.TotalTime.Avg = totalTimeAggResult.Avg
				histoDateBucket.TotalTime.Max = totalTimeAggResult.Max
			}

			res.Data = append(res.Data, histoDateBucket)

		}
		return res, nil
	}

	return nil, nil
}

// Save probe result using bulk mode
// Return error if fail or just fail results
func (obj *ProbeResultDAO) SaveBulk(results []ProbeResult) ([]ProbeResult, error) {

	ctx := context.Background()

	// Get elastic client
	client, err := obj.getClient()
	if err != nil {
		log.Error("Fail to get elastic client", err)
		return nil, err
	}

	bulkService := client.Bulk()

	// Compose bulk request
	for _, result := range results {

		var indexSuffix = result.Date.Format("-2006.01.02")
		var request *elastic.BulkIndexRequest = elastic.NewBulkIndexRequest()

		request.Index(obj.Index + indexSuffix)
		request.Type(obj.Type)
		request.Doc(result)
		bulkService.Add(request)
	}

	// processing request
	resp, err := bulkService.Do(ctx)
	if err != nil {
		log.Error("Fail to execute bulk request", err)
		return nil, err
	}

	// TODO deal with bluk errors
	for _, resp := range resp.Failed() {
		log.Error("SaveBulk error", err)
		log.Error(StructToString(resp))
	}

	return nil, nil
}
