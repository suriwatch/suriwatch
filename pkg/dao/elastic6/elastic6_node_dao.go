package dao

import (
	"context"
	"reflect"
	"time"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type Elastic6NodeDAO struct {
	Url    string
	Index  string
	Type   string
	client *elastic.Client
	debug  bool
}

/**
 * Constructor for probeDat
 */
func NewNodeDAO(config config.Elastic) *Elastic6NodeDAO {

	return &Elastic6NodeDAO{
		Url:    config.Url,
		Type:   "node",
		Index:  "suriwatch-node",
		client: nil,
		debug:  config.Debug,
	}
}
func (obj *Elastic6NodeDAO) getClient() (*elastic.Client, error) {

	if obj.client == nil {

		var err error = nil

		if obj.debug {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
				elastic.SetErrorLog(log.StandardLogger()),
				elastic.SetInfoLog(log.StandardLogger()),
				elastic.SetTraceLog(log.StandardLogger()),
			)
		} else {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
			)
		}

		if err != nil {
			print(err)
			return nil, err
		}
	}

	return obj.client, nil
}

func (obj *Elastic6NodeDAO) SendStatus(packet model.StatusPacket) error {

	client, err := obj.getClient()

	if err != nil {
		log.Error("Fail to get elastic client", err)
		return err
	}

	ctx := context.Background()
	indexService := client.Index().
		Index(obj.Index).
		Id(packet.Node).
		Type(obj.Type).
		BodyJson(packet)

	_, err = indexService.Do(ctx)

	if err != nil {
		log.Error("Fail to save ping", err)
		return err
	}

	return nil
}

func (obj *Elastic6NodeDAO) SearchStatus(size *int, from *time.Time, to *time.Time) ([]model.StatusPacket, error) {

	client, err := obj.getClient()

	if err != nil {
		log.Error("Fail to get elastic client", err)
		return nil, err
	}

	booleanQuery := elastic.NewBoolQuery()

	ctx := context.Background()
	searchService := client.Search().
		Index(obj.Index).
		Type(obj.Type).
		Sort("node.keyword", true).
		Query(elastic.NewMatchAllQuery())

	// Setting max result
	if size != nil {
		searchService.Size(*size)
	}

	if from != nil || to != nil {
		rangeFilter := elastic.NewRangeQuery("date")

		if from != nil {
			rangeFilter.Gte(from)
		}
		if to != nil {
			rangeFilter.Lte(to)
		}

		booleanQuery.Filter(rangeFilter)
	}

	searchService.Query(booleanQuery)

	res, err := searchService.Do(ctx)

	if err != nil {
		log.Error("Fail to get pings", err)
		return nil, err
	}

	var pings = make([]model.StatusPacket, len(res.Hits.Hits))

	var ttyp model.StatusPacket
	for i, item := range res.Each(reflect.TypeOf(ttyp)) {
		if t, ok := item.(model.StatusPacket); ok {
			pings[i] = t
		}
	}
	return pings, nil

}

func (obj *Elastic6NodeDAO) SearchNodeHisto(query model.HistoNodeQuery) ([]model.NodeInfo, error) {

	client, err := obj.getClient()
	if err != nil {
		log.Error("Fail to get elastic client", err)
		return nil, err
	}

	var filters []elastic.Query

	if query.ProbeId != nil {
		filters = append(filters, elastic.NewTermQuery("id", *query.ProbeId))
	}

	if query.From != nil || query.To != nil {
		rangeQuery := elastic.NewRangeQuery("date")

		if query.From != nil {
			rangeQuery.Gte(query.From)
		}
		if query.To != nil {
			rangeQuery.Lte(query.To)
		}

		filters = append(filters, rangeQuery)
	}

	var esQuery elastic.Query = nil

	if len(filters) > 0 {
		booleanQuery := elastic.NewBoolQuery()
		booleanQuery.Filter(filters...)
		esQuery = booleanQuery
	} else {
		esQuery = elastic.NewMatchAllQuery()
	}

	probeAggregation := elastic.NewTermsAggregation()
	probeAggregation.Field("host")

	ctx := context.Background()
	searchService := client.Search().
		Index("suriwatch-data-*").
		Type("probe-data").
		Query(esQuery).
		Size(0).
		Aggregation("agg_nodes", probeAggregation)

	searchResult, err := searchService.Do(ctx)
	if err != nil {
		log.Error("Fail to get nodes", err)
		return nil, err
	}

	resAggregation, found := searchResult.Aggregations.Terms("agg_nodes")

	var results []model.NodeInfo = nil

	if found {
		results = make([]model.NodeInfo, len(resAggregation.Buckets))
		for idx, bucket := range resAggregation.Buckets {
			node := model.NodeInfo{}
			node.Name = bucket.Key.(string)
			results[idx] = node
		}
	}
	return results, nil
}
