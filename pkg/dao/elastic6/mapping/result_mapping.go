package mapping

const RESULT_INDEX_MAPPING = `
{
  "index_patterns": ["suriwatch-data-*"],
  "settings": {
    "number_of_shards": 1
  },
  "mappings": {
    "probe-data": {
      "_source": {
        "enabled": true
      },

         "dynamic_templates":[
            {
               "labels":{
                  "path_match":"labels.*",
                  "mapping":{
                     "type":"keyword"
                  }
               }
            }
         ],
         "properties":{
            "HTTPStats":{
               "properties":{
                  "DNSLookup":{
                     "type":"long"
                  },
                  "TCPConnection":{
                     "type":"long"
                  },
                  "TLSHandshake":{
                     "type":"long"
                  },
                  "serverProcessing":{
                     "type":"long"
                  },
                  "total":{
                     "type":"long"
                  }
               }
            },
            "date":{
               "type":"date"
            },
            "duration":{
               "type":"long"
            },
            "host":{
               "type":"keyword"
            },
            "infos":{
               "type":"text",
               "fields":{
                  "keyword":{
                     "type":"keyword",
                     "ignore_above":256
                  }
               }
            },
            "probeId":{
               "type":"keyword"
            },
            "name":{
               "type":"keyword"
            },
            "status":{
               "type":"keyword"
            },
            "type":{
               "type":"keyword"
            }
         }
 

 
    }
  }
}
`
