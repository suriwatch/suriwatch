package dao

import (
	"context"
	"fmt"
	"math"
	"reflect"
	"time"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type Elastic6EventDAO struct {
	Url    string
	Index  string
	Type   string
	client *elastic.Client
	debug  bool
}

/**
 * Constructor for probeDat
 */
func NewEventDAO(config config.Elastic) *Elastic6EventDAO {

	return &Elastic6EventDAO{
		Url:    config.Url,
		Type:   "event",
		Index:  "suriwatch-event",
		client: nil,
		debug:  config.Debug,
	}
}

func (obj *Elastic6EventDAO) getClient() (*elastic.Client, error) {

	if obj.client == nil {

		var err error = nil

		if obj.debug {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
				elastic.SetErrorLog(log.StandardLogger()),
				elastic.SetInfoLog(log.StandardLogger()),
				elastic.SetTraceLog(log.StandardLogger()),
			)
		} else {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
			)
		}

		if err != nil {
			log.Error("Fail to get elastic client", err)
			return nil, err
		}
	}

	return obj.client, nil
}

func (obj *Elastic6EventDAO) Save(event model.StatusChangeEvent) error {

	client, err := obj.getClient()

	if err != nil {
		log.Error("Fail to get elastic client", err)
		return err
	}

	var index = obj.Index + event.Start.Format("-2006.01.02")

	ctx := context.Background()
	indexService := client.Index().
		Index(index).
		Type(obj.Type).
		Id(event.Id).
		BodyJson(event)

	_, err = indexService.Do(ctx)

	if err != nil {
		log.Error("Fail to save event", err)
		return err
	}

	return nil

}

func (obj *Elastic6EventDAO) SaveBulk(events []model.StatusChangeEvent) ([]model.StatusChangeEvent, error) {

	ctx := context.Background()

	// Get elastic client
	client, err := obj.getClient()
	if err != nil {
		log.Error("Fail to get elastic client", err)
		return nil, err
	}

	bulkService := client.Bulk()

	// Compose bulk request
	for _, event := range events {

		var indexSuffix = event.Start.Format("-2006.01.02")

		var request = elastic.NewBulkIndexRequest()
		request.Index(obj.Index + indexSuffix)
		request.Type(obj.Type)
		request.Doc(event)
		request.Id(event.Id)
		bulkService.Add(request)

	}

	// processing request
	resp, err := bulkService.Do(ctx)
	if err != nil {
		log.Error("Fail to invoke bulk", err)
		return nil, err
	}

	// TODO deal with bluk errors
	for _, resp := range resp.Failed() {
		log.Warning("Bulk errors", resp)
	}

	return nil, nil
}

func (m *Elastic6EventDAO) Search(query model.SearchEventQuery) ([]*model.StatusChangeEvent, error) {

	client, err := m.getClient()

	if err != nil {
		log.Error("Fail to get elastic client", err)
		return nil, err
	}

	indexSuffix := "-*"

	searchService := client.Search().Index(m.Index + indexSuffix)

	// Setting max result
	if query.Size != nil {
		searchService.Size(*query.Size)
	}

	var shoulds []elastic.Query
	var filters []elastic.Query

	// Adding labels filtering
	if query.Labels != nil {
		for k, v := range query.Labels {
			filters = append(filters, elastic.NewTermQuery("labels."+k, v))
		}
	}

	if query.ProbeId != nil {
		filters = append(filters, elastic.NewTermQuery("probeId", *query.ProbeId))
	}
	if query.Mode != nil {
		filters = append(filters, elastic.NewTermQuery("mode", *query.Mode))
	}
	if query.Node != nil {
		filters = append(filters, elastic.NewTermQuery("node", *query.Node))
	}

	if query.To != nil {
		rangeFilter := elastic.NewRangeQuery("start")
		rangeFilter.Lte(query.To)
		filters = append(filters, rangeFilter)
	}

	if query.From != nil {

		rangeFilter := elastic.NewRangeQuery("end")
		rangeFilter.Gte(query.From)
		shoulds = append(shoulds, rangeFilter)

		// deal with no end event
		endBooleanQuery := elastic.NewBoolQuery()
		endBooleanQuery.MustNot(elastic.NewExistsQuery("end"))
		shoulds = append(shoulds, endBooleanQuery)
	}

	booleanQuery := elastic.NewBoolQuery()

	if len(shoulds) > 0 {
		booleanQuery.MinimumNumberShouldMatch(1)
		booleanQuery.Should(shoulds...)
	}

	if len(filters) > 0 {
		booleanQuery.Filter(filters...)
	}

	// if no filtering condition, select all
	if len(shoulds) == 0 && len(filters) == 0 {
		matchAllQuery := elastic.NewMatchAllQuery()
		searchService.Query(matchAllQuery)
	} else {
		searchService.Query(booleanQuery)
	}

	var timeSort *model.SortOrder = nil

	// Add fields sorters
	var sorters []elastic.Sorter
	for _, sortValue := range query.Sort {

		s := elastic.NewFieldSort(sortValue.Name)
		if sortValue.Order == model.DESC {
			s.Desc()
		}
		sorters = append(sorters, s)
	}

	// Default sorters
	if len(sorters) == 0 {
		s := elastic.NewFieldSort("start")
		sorters = append(sorters, s)
		order := model.SortOrder("asc")
		timeSort = &order
	} else {
		if query.Sort[0].Name == "start" {
			timeSort = &query.Sort[0].Order
		}

	}

	if query.Size != nil {
		searchService.Size(*query.Size)
	}
	searchService.AllowNoIndices(true)
	searchService.IgnoreUnavailable(true)
	searchService.SortBy(sorters...)

	// Executing query
	ctx := context.Background()
	searchResult, err := searchService.Do(ctx) // execute

	if err != nil {
		// Handle error
		print(err)
		return nil, err
	}

	var events = make([]*model.StatusChangeEvent, len(searchResult.Hits.Hits))

	// Extract results (see: https://olivere.github.io/elastic/)
	var ttyp model.StatusChangeEvent
	for i, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if t, ok := item.(model.StatusChangeEvent); ok {
			events[i] = &t
		}
	}

	// Auto close wrong state
	if query.AutoClose != nil && *query.AutoClose && timeSort != nil {
		ascending := *timeSort == model.ASC
		events = m.autoCloseEvents(events, ascending)
	}

	return events, nil

}

func (m *Elastic6EventDAO) autoCloseEvents(events []*model.StatusChangeEvent, ascending bool) []*model.StatusChangeEvent {

	nbEvents := len(events)

	// If no events, nothing to do
	if nbEvents == 0 {
		return events
	}

	if !ascending {
		var previousStart = events[0].Start

		for i := 1; i < nbEvents; i++ {
			event := events[i]

			if event.End == nil {
				event.End = &previousStart
				var duration = int(math.Round(event.End.Sub(event.Start).Seconds()))
				event.Duration = &duration
			}
			previousStart = events[i].Start
		}

	} else {
		var previousStart = events[nbEvents-1].Start

		for i := nbEvents - 2; i >= 0; i-- {
			event := events[i]
			fmt.Println(event.Start.Format(time.RFC3339) + " s -  " + previousStart.Format(time.RFC3339))

			if event.End == nil {
				end := previousStart
				event.End = &end
				var duration = int(math.Round(event.End.Sub(event.Start).Seconds()))
				event.Duration = &duration
			}
			previousStart = events[i].Start
		}

	}
	return events
}
