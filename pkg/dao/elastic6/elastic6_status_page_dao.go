package dao

import (
	"fmt"
	"reflect"

	"github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type Elastic6StatusPageDAO struct {
	Elastic6DAO
}

/**
 * Constructor for probeDat
 */
func NewStatusPageDAO(config config.Elastic) *Elastic6StatusPageDAO {

	dao := &Elastic6StatusPageDAO{
		Elastic6DAO{
			Url:    config.Url,
			Type:   "status-page",
			Index:  "suriwatch-status-page",
			client: nil,
			debug:  config.Debug,
		},
	}

	dao.Elastic6DAO.GetId = dao.GetId
	dao.Elastic6DAO.GetIndex = dao.GetIndex
	dao.Elastic6DAO.GetEntityType = dao.GetEntityType

	return dao
}

func (dao *Elastic6StatusPageDAO) GetIndex(entity interface{}) string {
	return dao.Index
}

func (dao *Elastic6StatusPageDAO) GetId(entity interface{}) *string {
	id := entity.(model.StatusPage).Id

	strId := string(id)
	return &strId
}
func (dao *Elastic6StatusPageDAO) GetEntityType() interface{} {
	return &model.StatusPage{}
}

func (dao *Elastic6StatusPageDAO) GetByID(id model.StatusPageId) (*model.StatusPage, error) {

	entity, err := dao.Elastic6DAO.GetById(string(id))

	if entity == nil {
		logrus.Info(fmt.Sprintf("Status page with id %s not found", string(id)))
		return nil, nil
	}
	return entity.(*model.StatusPage), err

}

//Save the given probe
func (dao *Elastic6StatusPageDAO) Save(statusPage model.StatusPage) (*model.StatusPage, error) {
	id, error := dao.Elastic6DAO.Save(statusPage, false)

	if error != nil {
		return nil, error
	}

	statusPage.Id = model.StatusPageId(*id)
	return &statusPage, nil

}

// Delete a probe by identifier
func (dao *Elastic6StatusPageDAO) DeleteByID(id model.StatusPageId) error {
	return dao.Delete(string(id), false)
}

// Search dashboard method
func (dao *Elastic6StatusPageDAO) Search(query model.SearchStatusPageQuery) ([]model.StatusPage, error) {

	searchResult, err := dao.Elastic6DAO.Search(SearchQuery{})
	if err != nil {
		return nil, err
	}

	var result = make([]model.StatusPage, len(searchResult.Hits.Hits))

	var ttype model.StatusPage

	// Extract results (see: https://olivere.github.io/elastic/)
	for i, item := range searchResult.Each(reflect.TypeOf(ttype)) {
		if t, ok := item.(model.StatusPage); ok {
			result[i] = t
		}
	}

	return result, nil

}
