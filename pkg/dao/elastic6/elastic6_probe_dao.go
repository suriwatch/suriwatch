package dao

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"reflect"
	"time"

	"github.com/olivere/elastic"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
	. "gitlab.com/suriwatch/suriwatch/pkg/dao/elastic6/model"
	"gitlab.com/suriwatch/suriwatch/pkg/util"
)

type Elastic6ProbeDAO struct {
	Url    string
	Index  string
	Type   string
	client *elastic.Client
	debug  bool
}

/**
 * Constructor for probeDat
 */
func NewProbeDAO(config config.Elastic) *Elastic6ProbeDAO {

	return &Elastic6ProbeDAO{
		Url:    config.Url,
		Type:   "probe",
		Index:  config.Index,
		client: nil,
		debug:  config.Debug,
	}
}

func (obj *Elastic6ProbeDAO) getClient() (*elastic.Client, error) {

	if obj.client == nil {

		var err error = nil

		if obj.debug {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
				elastic.SetErrorLog(log.StandardLogger()),
				elastic.SetInfoLog(log.StandardLogger()),
				elastic.SetTraceLog(log.StandardLogger()),
			)
		} else {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
			)
		}

		if err != nil {
			print(err)
			return nil, err
		}
	}

	return obj.client, nil
}

func (obj *Elastic6ProbeDAO) Save(probe Probe) (*Probe, error) {

	log.Infof("Saving probe [%s]", probe.Id)

	if probe.Id == "" {
		probe.Id = NewProbeId()
	}

	var postURL = obj.Url + "/" + obj.Index + "/probe/" + string(probe.Id) + "?refresh=wait_for"

	httpClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	// Set object as byte buffer
	out, err := obj.serialize(probe)
	if err != nil {
		log.Errorf("Fail to serialize: %s", err)
		return nil, err
	}

	// Initialise request
	req, err := http.NewRequest(http.MethodPost, postURL, out)
	if err != nil {
		log.Errorf("Fail to get configuration: %s", err)
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Errorf("Fail to save probes: %s", getErr)
		return nil, err
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Errorf("Fail to read probes body: %s", getErr)
		return nil, err
	}

	defer res.Body.Close()

	response := SearchResponse{}

	jsonErr := json.Unmarshal(body, &response)
	if jsonErr != nil {
		log.Errorf("Fail to unmarshall json probe: %s", jsonErr)
		return nil, err
	}
	return &probe, nil
}

/**
 * Give all probes
 */
func (m *Elastic6ProbeDAO) GetByID(id ProbeId) (*Probe, error) {

	log.Infof("Getting by Id [%s] probe", id)

	httpClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	var getURL = m.Url + "/" + m.Index + "/" + m.Type + "/" + string(id)

	// Initialise request
	req, err := http.NewRequest(http.MethodGet, getURL, nil)
	if err != nil {
		log.Errorf("Fail to get configuration: %s", err)
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Errorf("Fail to get probes: %s", getErr)
		return nil, getErr
	}
	defer res.Body.Close()

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Errorf("Fail to read probes body: %s", readErr)
		return nil, readErr
	}

	response := GetResponse{}
	jsonErr := json.Unmarshal(body, &response)

	if jsonErr != nil {
		log.Errorf("Fail to unmarshall json probe: %s", jsonErr)
		return nil, jsonErr
	}

	if !response.Found {
		return nil, errors.New("Probe not found")
	}

	return &response.Source, nil

}

/**
 * Give all probes
 */
func (m *Elastic6ProbeDAO) Search(query SearchProbeQuery) ([]Probe, error) {

	client, err := m.getClient()

	if err != nil {
		log.Errorf("Fail to get elastic client : %s", err)
		return nil, err
	}

	searchService := client.Search().Index(m.Index)

	// Setting max result
	if query.Size != nil {
		searchService.Size(*query.Size)
	}
	var queries []elastic.Query
	var filters []elastic.Query

	// Adding labels filtering
	if query.Labels != nil {
		for k, v := range query.Labels {
			filters = append(filters, elastic.NewTermQuery("labels."+k, v))
		}
	}

	if query.Name != nil {
		filters = append(filters, elastic.NewTermQuery("name.keyword", *query.Name))
	}
	if query.Type != nil {
		filters = append(filters, elastic.NewTermQuery("type", *query.Type))
	}
	if query.State != nil {
		filters = append(filters, elastic.NewTermQuery("state", *query.State))
	}
	if query.Mode != nil {
		filters = append(filters, elastic.NewTermQuery("mode", *query.Mode))
	}

	if query.Query != nil {

		var strQuery = *query.Query
		simpleQuery := elastic.NewQueryStringQuery("*" + strQuery + "*")
		simpleQuery.Field("name")
		simpleQuery.Field("description")
		simpleQuery.Field("labels.*")

		queries = append(queries, simpleQuery)
	}

	if len(queries) > 0 || len(filters) > 0 {
		booleanQuery := elastic.NewBoolQuery()

		booleanQuery.Must(queries...)
		booleanQuery.Filter(filters...)
		searchService.Query(booleanQuery)

	} else {
		matchAllQuery := elastic.NewMatchAllQuery()
		searchService.Query(matchAllQuery)
	}

	searchService.Sort("name.keyword", true)

	// Executing query
	ctx := context.Background()
	searchResult, err := searchService.Do(ctx) // execute

	if err != nil {
		// Handle error
		print(err)
		return nil, err
	}

	var probes = make([]Probe, len(searchResult.Hits.Hits))

	// Extract results (see: https://olivere.github.io/elastic/)
	var ttyp Probe
	for i, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if t, ok := item.(Probe); ok {
			probes[i] = t
		}
	}

	return probes, nil

}

func (m *Elastic6ProbeDAO) structToString(obj interface{}) string {
	out, err := json.Marshal(obj)
	if err != nil {
		panic(err)
	}
	return string(out)
}

func (m *Elastic6ProbeDAO) serialize(object interface{}) (*bytes.Buffer, error) {

	out, err := json.Marshal(object)
	if err != nil {

		log.Errorf("Fail to marshal request: %s", err)
		return nil, err
	}

	return bytes.NewBufferString(string(out)), nil
}

func (obj *Elastic6ProbeDAO) DeleteByID(id ProbeId) error {

	client, err := obj.getClient()

	if err != nil {
		log.Errorf("Fail to get elastic client: %s", err)
		return err
	}

	ctx := context.Background()
	_, err = client.Delete().Refresh("wait_for").Id(string(id)).Index(obj.Index).Type(obj.Type).Do(ctx)

	if err != nil {
		// Handle error
		print(err)
		return err
	}

	return nil

}

// Save probe result using bulk mode
// Return error if fail or just fail results
func (obj *Elastic6ProbeDAO) SaveBulk(probes []Probe) ([]Probe, error) {

	ctx := context.Background()

	// Get elastic client
	client, err := obj.getClient()
	if err != nil {
		return nil, err
	}

	bulkService := client.Bulk()

	// Compose bulk request
	for _, probe := range probes {

		var request = elastic.NewBulkIndexRequest()
		request.Index(obj.Index)
		request.Type(obj.Type)
		request.Doc(probe)
		request.Id(string(probe.Id))
		bulkService.Add(request)

	}

	// processing request
	resp, err := bulkService.Do(ctx)
	if err != nil {
		return nil, err
	}

	// TODO deal with bluk errors
	for _, resp := range resp.Failed() {
		log.Error("SaveBulk error", err)
		log.Error(util.StructToString(resp))
	}

	return nil, nil
}
