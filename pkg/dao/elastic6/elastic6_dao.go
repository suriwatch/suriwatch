package dao

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/olivere/elastic"
	"github.com/pkg/errors"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/util"
)

type Elastic6DAO struct {
	Url    string
	Index  string
	Type   string
	client *elastic.Client
	debug  bool

	GetIndex      func(entity interface{}) string
	GetId         func(entity interface{}) *string
	GetEntityType func() interface{}
}

func (obj *Elastic6DAO) getClient() (*elastic.Client, error) {

	if obj.client == nil {

		var err error = nil

		if obj.debug {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
				elastic.SetErrorLog(log.StandardLogger()),
				elastic.SetInfoLog(log.StandardLogger()),
				elastic.SetTraceLog(log.StandardLogger()),
			)
		} else {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
			)
		}

		if err != nil {
			print(err)
			return nil, err
		}
	}

	return obj.client, nil
}

func (dao *Elastic6DAO) GenerateId() string {
	return xid.New().String()
}

func (dao *Elastic6DAO) Save(entity interface{}, async bool) (*string, error) {

	client, err := dao.getClient()

	if err != nil {
		log.Error("Fail to get elastic client", err)
		return nil, err
	}

	id := dao.GetId(entity)
	if id == nil {
		strId := dao.GenerateId()
		id = &strId
	}

	ctx := context.Background()
	indexService := client.Index().
		Index(dao.GetIndex(entity)).
		Type(dao.Type).
		Id(*id).
		BodyJson(entity)

	if !async {
		indexService = indexService.Refresh("wait_for")
	}

	_, err = indexService.Do(ctx)

	if err != nil {
		log.Error("Fail to save entity", err)
		return nil, err
	}

	return id, nil
}

func (obj *Elastic6DAO) GetById(id string) (interface{}, error) {

	client, err := obj.getClient()
	if err != nil {
		log.Errorf("Fail to get elastic client: %s", err)
		return nil, err
	}

	ctx := context.Background()
	getService := client.Get().
		Id(id).
		Index(obj.Index).
		Type(obj.Type)

	resp, err := getService.Do(ctx)
	if err != nil {
		msg := fmt.Sprintf("Fail to get entity with id %s", id)
		return nil, errors.Wrap(err, msg)
	}

	if !resp.Found {
		return nil, nil
	}

	//styp := reflect.TypeOf(obj.GetEntityType())
	//entity := reflect.New(typ).Elem().Interface()
	entity := obj.GetEntityType()

	if err := json.Unmarshal(*resp.Source, entity); err != nil {
		log.Error(err)
		return nil, errors.Wrap(err, "Fail to deserialize entity")
	}

	return entity, nil

}

func (obj *Elastic6DAO) Delete(id string, async bool) error {

	client, err := obj.getClient()

	if err != nil {
		log.Errorf("Fail to get elastic client: %s", err)
		return err
	}

	ctx := context.Background()
	deleteService := client.Delete().
		Refresh("wait_for").
		Id(id).
		Index(obj.Index).
		Type(obj.Type)

	if !async {
		deleteService = deleteService.Refresh("wait_for")
	}

	_, err = deleteService.Do(ctx)

	if err != nil {
		msg := fmt.Sprintf("Fail to delete entity with id %s", id)
		return errors.Wrap(err, msg)
	}

	return nil

}

// Save probe result using bulk mode
// Return error if fail or just fail results
func (dao *Elastic6DAO) SaveBulk(results []interface{}) ([]interface{}, error) {

	ctx := context.Background()

	// Get elastic client
	client, err := dao.getClient()
	if err != nil {
		return nil, err
	}

	bulkService := client.Bulk()

	// Compose bulk request
	for _, result := range results {

		var request *elastic.BulkIndexRequest = elastic.NewBulkIndexRequest()

		request.Index(dao.GetIndex(result))
		request.Type(dao.Type)
		request.Doc(result)
		bulkService.Add(request)
	}

	// processing request
	resp, err := bulkService.Do(ctx)
	if err != nil {
		msg := fmt.Sprintf("Fail to save bulk entities")
		return nil, errors.Wrap(err, msg)
	}

	// TODO deal with bluk errors
	for _, resp := range resp.Failed() {
		log.Error("SaveBulk error", err)
		log.Error(util.StructToString(resp))
	}

	return nil, nil
}

type SearchQuery struct {
	Size *int

	Sort []model.SortValue
}

// Save probe result using bulk mode
// Return error if fail or just fail results
func (dao *Elastic6DAO) Search(query SearchQuery) (*elastic.SearchResult, error) {

	client, err := dao.getClient()

	if err != nil {
		log.Error("Fail to get elastic client", err)
		return nil, err
	}

	indexSuffix := "" //-*"

	searchService := client.Search().Index(dao.Index + indexSuffix)

	// Setting max result
	if query.Size != nil {
		searchService.Size(*query.Size)
	}

	// Add fields sorters
	var sorters []elastic.Sorter
	for _, sortValue := range query.Sort {
		s := elastic.NewFieldSort(sortValue.Name)
		if sortValue.Order == model.DESC {
			s.Desc()
		}
		sorters = append(sorters, s)
	}

	searchService.AllowNoIndices(true)
	searchService.IgnoreUnavailable(true)
	searchService.SortBy(sorters...)

	// Executing query
	ctx := context.Background()
	searchResult, err := searchService.Do(ctx) // execute

	return searchResult, nil
}
