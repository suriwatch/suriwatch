// +build integration_tests

package dao

import (
	"bytes"
	"gitlab.com/suriwatch/suriwatch/pkg/dao/elastic6/mapping"
	"io/ioutil"
	"net/http"
	"os"
)

func createIndex(index string) error {

	url := getEnv("ELASTICSEARCH_URL", "http://localhost:9200")
	client := &http.Client{}

	var err error = nil
	var resp *http.Response = nil
	var req *http.Request = nil

	// Build create index query
	req, err = http.NewRequest("PUT", url+"/"+index, bytes.NewBuffer([]byte(mapping.PROBE_INDEX_MAPPING)))
	if err != nil {
		return nil
	}
	req.Header.Add("Content-Type", "application/json")

	// Execute creation
	resp, err = client.Do(req)
	defer resp.Body.Close()

	_, err = ioutil.ReadAll(resp.Body)

	return err
}

func deleteIndex(index string) error {

	url := getEnv("ELASTICSEARCH_URL", "http://localhost:9200")
	client := &http.Client{}

	var err error = nil
	var resp *http.Response = nil
	var req *http.Request = nil

	// Build Delete index query
	req, err = http.NewRequest("DELETE", url+"/"+index, nil)
	if err != nil {
		return nil
	}

	// Execute deletion
	resp, err = client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
