package dao

import (
	"context"
	"strings"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type Elastic6LabelDAO struct {
	Url    string
	Index  string
	Type   string
	client *elastic.Client
	debug  bool
}

/**
 * Constructor for probeDat
 */
func NewLabelDAO(config config.Elastic) *Elastic6LabelDAO {

	return &Elastic6LabelDAO{
		Url:    config.Url,
		Type:   "probe",
		Index:  config.Index,
		client: nil,
		debug:  config.Debug,
	}
}

func (obj *Elastic6LabelDAO) getClient() (*elastic.Client, error) {

	if obj.client == nil {

		var err error = nil

		obj.debug = true
		if obj.debug {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
				elastic.SetErrorLog(log.StandardLogger()),
				elastic.SetInfoLog(log.StandardLogger()),
				elastic.SetTraceLog(log.StandardLogger()),
			)
		} else {
			obj.client, err = elastic.NewClient(
				elastic.SetURL(obj.Url),
				elastic.SetSniff(false),
			)
		}

		if err != nil {
			print(err)
			return nil, err
		}
	}

	return obj.client, nil
}

func (obj *Elastic6LabelDAO) getLabelKeys() ([]string, error) {

	client, err := obj.getClient()

	if err != nil {
		log.Errorf("Fail to get elastic client %s", err)
		return nil, err
	}

	ctx := context.Background()
	res, err := client.GetFieldMapping().
		Type(obj.Type).
		Index(obj.Index).
		Field("labels*").
		AllowNoIndices(true).
		Do(ctx)

	if err != nil {
		log.Errorf("Fail to get mapping for label keys resolution : %s", err)
		return nil, err
	}

	var keys = []string{}

	index := res[obj.Index]
	mapIndex := index.(map[string]interface{})

	// Check if mapping is in map
	if _, exist := mapIndex["mappings"]; !exist {
		return keys, nil
	}
	mappings := mapIndex["mappings"]
	mapMapping := mappings.(map[string]interface{})

	// Check if mapping is in probe
	if _, exist := mapMapping["probe"]; !exist {
		return keys, nil
	}
	probe := mapMapping["probe"]
	mapProbe := probe.(map[string]interface{})

	for k, _ := range mapProbe {
		splitsVal := strings.SplitAfter(k, ".")
		keys = append(keys, splitsVal[1])
	}

	return keys, nil
}

func (obj *Elastic6LabelDAO) GetLabels(includeEmpty bool) ([]LabelReference, error) {

	labels, err := obj.getLabelKeys()

	if err != nil {
		return nil, err
	}

	client, err := obj.getClient()

	if err != nil {
		log.Errorf("Fail to get elastic client %s", err)
		return nil, err
	}

	ctx := context.Background()
	searchService := client.Search().
		Type(obj.Type).
		Index(obj.Index).
		AllowNoIndices(true).
		IgnoreUnavailable(true).
		Query(elastic.NewMatchAllQuery())

	for _, label := range labels {
		agg := elastic.NewTermsAggregation().Field("labels." + label)
		searchService.Aggregation(label, agg)
	}
	res, err := searchService.Do(ctx)

	if err != nil {
		log.Error(err)
		return nil, err
	}

	var labelReferences = []LabelReference{}

	for _, label := range labels {
		resAgg, found := res.Aggregations.Terms(label)

		// If result, map it
		if found {

			labelRef := LabelReference{}
			labelRef.Name = label
			labelRef.Values = []string{}

			// Adding key associated values
			for _, bucket := range resAgg.Buckets {
				labelRef.Values = append(labelRef.Values, bucket.Key.(string))
			}

			if len(labelRef.Values) > 0 || (len(labelRef.Values) == 0 && includeEmpty) {
				labelReferences = append(labelReferences, labelRef)
			}

		}
	}

	return labelReferences, nil
}

func (obj *Elastic6LabelDAO) GetLabel(label string) (*LabelReference, error) {

	client, err := obj.getClient()

	if err != nil {
		log.Errorf("Fail to get elastic client %s", err)
		return nil, err
	}

	ctx := context.Background()
	searchService := client.Search().
		Index(obj.Index).
		Type(obj.Type).
		Query(elastic.NewMatchAllQuery()).
		Aggregation(label, elastic.NewTermsAggregation().Field("labels."+label))

	res, err := searchService.Do(ctx)

	if err != nil {
		log.Error("Fail to get labels", err)
		return nil, err
	}

	resAgg, found := res.Aggregations.Terms(label)

	// If result, map it
	if found {

		labelRef := LabelReference{}
		labelRef.Name = label
		labelRef.Values = []string{}

		// Adding key associated values
		for _, bucket := range resAgg.Buckets {
			labelRef.Values = append(labelRef.Values, bucket.Key.(string))
		}

		return &labelRef, nil
	}

	return nil, nil
}
