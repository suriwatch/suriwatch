package dao

import (
	"bytes"
	"net/http"
	"strconv"
	"time"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type InfluxProbeResultDAO struct {
	Url      string
	Database string
}

/**
 * Constructor for probeDao
 */
func NewInfluxProbeResultDAO(config config.Influxdb) *InfluxProbeResultDAO {

	return &InfluxProbeResultDAO{
		Url:      config.Url,
		Database: config.Database,
	}
}

/**
 * Save a probe result
 */
func (m *InfluxProbeResultDAO) Save(result ProbeResult) error {

	// https://docs.influxdata.com/influxdb/v1.6/tools/api/
	// https://docs.influxdata.com/influxdb/v1.6/write_protocols/line_protocol_reference/

	log.Infof("Saving probe result for [%s]", result.ProbeId)

	httpClient := http.Client{
		Timeout: time.Second * 2,
	}

	var url = m.Url + "/write?db=" + m.Database
	var currentTimestamp = strconv.FormatInt(result.Date.UnixNano(), 10)
	//	var data = "duration,probe="+ result.Probe + ",host="+ result.Host + ",status=" +result.Status+ " value=" + strconv.Itoa(result.Duration) + " " + currentTimestamp
	var data = "test,probe=" + string(result.ProbeId) + ",host=" + result.Host + ",status=" + string(result.Status) + " value=\"" + string(result.Status) + "\" " + currentTimestamp

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBufferString(data))
	if err != nil {
		log.Errorf("Fail to initialise request: %s", err)
		return errors.Wrap(err, "Fail to initialize request")
	}

	req.Header.Add("Content-Type", "application/json")

	res, err := httpClient.Do(req)
	if err != nil {
		log.Errorf("Fail to execute request %s", err)
		return errors.Wrap(err, "Fail to execute result")
	}

	defer res.Body.Close()

	return nil
}

func (obj *InfluxProbeResultDAO) SaveBulk(result []ProbeResult) ([]ProbeResult, error) {
	return nil, nil
}

func (obj *InfluxProbeResultDAO) DeleteByProbeId(id ProbeId) error {

	// TODO
	log.Warning("Not yet implemented method")
	return nil
}

func (obj InfluxProbeResultDAO) GetProbeResult(id ProbeId, size int, from *time.Time, to *time.Time) ([]ProbeResult, error) {

	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil

}

func (obj InfluxProbeResultDAO) GetLastProbesResult(criteria SearchLastResultCriteria) ([]ProbeResult, error) {

	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil

}

func (obj *InfluxProbeResultDAO) GetAggregatedLastProbesResult(groups []string, criteria SearchAggregatedResultCriteria) (*ProbeResultGroup, error) {

	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil
}

//Give the result date histogram for the given probe
func (obj *InfluxProbeResultDAO) GetProbeHistogram(id ProbeId, agent *string, from *string, to *string, interval *string) (*ProbeResultHistogram, error) {
	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil
}

func (obj *InfluxProbeResultDAO) GetProbeAvailabilityRate(id ProbeId, from *string, to *string, interval *string) (*ProbeAvailability, error) {

	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil

}
