package dao

import (
	log "github.com/sirupsen/logrus"
	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
	"gitlab.com/suriwatch/suriwatch/pkg/config"
)

type InfluxProbeDAO struct {
}

/**
 * Constructor for probeDat
 */
func NewInfluxProbeDAO(influxdb config.Influxdb) *InfluxProbeDAO {

	return &InfluxProbeDAO{}
}

func (m *InfluxProbeDAO) GetByID(id ProbeId) (*Probe, error) {
	log.Infof("Get probe by id %s", id)

	return nil, nil

}

func (m *InfluxProbeDAO) Save(probe Probe) (*Probe, error) {

	log.Infof("Saving probe")
	return &probe, nil
}

func (m *InfluxProbeDAO) DeleteByID(id ProbeId) error {
	// TODO
	log.Warning("Not yet implemented method")
	return nil
}

func (obj *InfluxProbeDAO) GetLabels(includeEmpty bool) ([]LabelReference, error) {

	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil
}

func (obj *InfluxProbeDAO) GetLabel(label string) (*LabelReference, error) {

	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil
}

func (obj *InfluxProbeDAO) Search(query SearchProbeQuery) ([]Probe, error) {

	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil
}

func (obj *InfluxProbeDAO) SaveBulk(results []Probe) ([]Probe, error) {
	// TODO
	log.Warning("Not yet implemented method")
	return nil, nil
}
