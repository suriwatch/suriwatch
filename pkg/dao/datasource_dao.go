package dao

import "gitlab.com/suriwatch/suriwatch/pkg/client/model"

type DataSourceDAO interface {

	Save(ds model.DataSource) (*model.DataSource, error)

	// Get a probe by identifier
	GetByID(id model.DataSourceId) (*model.DataSource, error)

	// Delete a probe by identifier
	DeleteByID(id model.DataSourceId) error

	Search(query model.SearchDataSourceQuery) ([]model.DataSource, error)
}
