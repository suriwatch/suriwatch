package dao

import . "gitlab.com/suriwatch/suriwatch/pkg/client/model"

type LabelDAO interface {

	// Give all existing probe labels and their associated values
	GetLabels(includeEmpty bool) ([]LabelReference, error)

	// Give a label with this associated values
	GetLabel(label string) (*LabelReference, error)
}
