package dao

import (
	"time"

	. "gitlab.com/suriwatch/suriwatch/pkg/client/model"
)

type ProbeResultDAO interface {

	// Save a probe execution result
	Save(result ProbeResult) error

	SaveBulk(result []ProbeResult) ([]ProbeResult, error)

	// Give all probes datas for the given probe
	GetProbeResult(id ProbeId, size int, from *time.Time, to *time.Time) ([]ProbeResult, error)

	// Give the last probe status for probes matching with the given labels
	GetLastProbesResult(criteria SearchLastResultCriteria) ([]ProbeResult, error)

	//Give the last probe status and group status by given groups
	GetAggregatedLastProbesResult(groups []string, criteria SearchAggregatedResultCriteria) (*ProbeResultGroup, error)

	//Give the result date histogram for the given probe
	GetProbeHistogram(id ProbeId, agent *string, from *string, to *string, interval *string) (*ProbeResultHistogram, error)

	//Delete a probe results
	DeleteByProbeId(id ProbeId) error
}
