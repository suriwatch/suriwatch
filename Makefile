DOCKER_REGISTRY      ?= registry.hub.docker.com
IMAGE_PREFIX         ?= suriwatch
DEV_IMAGE            ?= golang:1.11
SHORT_NAME_API       ?= suriwatch-api
SHORT_NAME_SENTINEL  ?= suriwatch-sentinel
TARGETS              ?= darwin/amd64 linux/amd64
DIST_DIRS            = find * -type d -exec

# go option
GO        ?= go
PKG       := $(shell glide novendor)
TAGS      :=
TESTS     := .
TESTFLAGS :=
LDFLAGS   := -w -s
GOFLAGS   :=
BINDIR    := $(CURDIR)/bin
BINARIES  := alerter api node


# Required for globs to work correctly
SHELL=/usr/bin/env bash


.PHONY: build
build:
	GOBIN=$(BINDIR) $(GO) install $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' gitlab.com/suriwatch/suriwatch/cmd/...



.PHONY: check-docker
check-docker:
	@if [ -z $$(which docker) ]; then \
	  echo "Missing \`docker\` client which is required for development"; \
	  exit 2; \
	fi

.PHONY: docker-binary
docker-binary: BINDIR = ./build/docker
docker-binary: GOFLAGS += -a -installsuffix cgo
docker-binary:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 $(GO) build -o $(BINDIR)/api/suriwatch-api $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' gitlab.com/suriwatch/suriwatch/cmd/api
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 $(GO) build -o $(BINDIR)/sentinel/suriwatch-sentinel $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' gitlab.com/suriwatch/suriwatch/cmd/sentinel

.PHONY: docker-build
docker-build: check-docker 
	docker build --rm -t ${IMAGE_API} ./build/docker/api
	docker tag ${IMAGE_API} ${MUTABLE_IMAGE_API}
	docker build --rm -t ${IMAGE_SENTINEL} ./build/docker/sentinel
	docker tag ${IMAGE_SENTINEL} ${MUTABLE_IMAGE_SENTINEL}
	

.PHONY: docker-build-scratch
docker-build-scratch: check-docker docker-binary docker-build


.PHONY: docker-deploy
docker-deploy: docker-build
	docker push ${MUTABLE_IMAGE_API}
	docker push ${MUTABLE_IMAGE_SENTINEL}

.PHONY: run-api
run-api: build
run-api:
	./bin/api -config=./configs/config-api.toml

.PHONY: run-sentinel
run-sentinel: build
run-sentinel:
	./bin/sentinel -config=./configs/config-sentinel.toml


test: ## Run unit tests
	go test ./...

it: ## Run integration tests
	go test ./... -tags=integration_tests


include versioning.mk


