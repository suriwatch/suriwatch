module gitlab.com/suriwatch/suriwatch

replace gitlab.com/suriwatch/suriwatch/pkg/client => ./pkg/client

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/gorilla/mux v1.7.0
	github.com/mailru/easyjson v0.0.0-20190221075403-6243d8e04c3f // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/olivere/elastic v6.2.16+incompatible
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v1.1.0
	github.com/prometheus/common v0.6.0
	github.com/rs/cors v1.6.0
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0
	github.com/tcnksm/go-httpstat v0.2.0
	gitlab.com/suriwatch/suriwatch/pkg/client v0.0.0-00010101000000-000000000000
)
