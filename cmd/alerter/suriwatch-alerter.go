package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/alerter"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	. "gitlab.com/suriwatch/suriwatch/pkg/config"
	. "gitlab.com/suriwatch/suriwatch/pkg/dao"
)

var (
	// DAO provider
	provider *Provider
)

func usage() {
	fmt.Fprintf(os.Stderr, "usage: suriwatch-alerter -config=[string]\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func initAppInfos() {

	var config = Get()
	config.AppInfo = model.AppInfo{}

	config.AppInfo.Type = config.Node.Mode
	config.AppInfo.Started = time.Now()
	if config.Node.Name != "" {
		config.AppInfo.Name = config.Node.Name
	} else {
		config.Node.Name, _ = os.Hostname()
		config.AppInfo.Name = config.Node.Name
	}
}

func initLogger() {
	// Log as JSON instead of the default ASCII formatter.
	//log.SetFormatter(&log.JSONFormatter{})

	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	Formatter.DisableColors = false
	log.SetFormatter(Formatter)

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	//log.SetReportCaller(true)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)

}

func main() {

	initLogger()
	log.Infof("Starting suriwatch alerter..")

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	dir = "/Users/ldassonville/go/src/gitlab.com/suriwatch/suriwatch/configs/"
	var configFile = flag.String("config", dir+"/config.toml", "TOML Config file path")

	flag.Parse()

	var config = Get()
	config.Read(*configFile)

	initAppInfos()

	provider := &Provider{Config: config}

	alerter := alerter.NewAlerter(provider)
	log.Infof("Starting with configuration %s", config.String())
	alerter.Start()

	log.Infof("Suriwatch Alerter running")

	// Wait sign term for close agent
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()
	<-done
	fmt.Println("Stopping alerter.")

}
