package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	. "gitlab.com/suriwatch/suriwatch/pkg/config"
	. "gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint"
	. "gitlab.com/suriwatch/suriwatch/pkg/sentinel/engine"
	"gitlab.com/suriwatch/suriwatch/pkg/version"
)

var (

	/** Engine **/
	executor    *ExecutorEngine
	helloEngine *HelloEngine

	// DAO provider
	provider *Provider
)

func usage() {
	fmt.Fprintf(os.Stderr, "usage: suriwatch-sentinel -config=[string]\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func initAppInfos() {

	var config = Get()
	config.AppInfo = model.AppInfo{}

	config.AppInfo.Type = config.Node.Mode
	config.AppInfo.Started = time.Now()
	config.AppInfo.Version = version.GetVersion()
	if config.Node.Name != "" {
		config.AppInfo.Name = config.Node.Name
	} else {
		config.Node.Name, _ = os.Hostname()
		config.AppInfo.Name = config.Node.Name
	}
}

func initLogger() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	log.SetFormatter(Formatter)

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)

}

func main() {

	initLogger()
	log.Infof("Starting suriwatch sentinel")

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	var configFile = flag.String("config", dir+"/config.toml", "TOML Config file path")
	flag.Parse()
	var config = Get()
	log.Infof("Loading config file %s", *configFile)
	config.Read(*configFile)

	initAppInfos()

	provider := &Provider{Config: config}

	// Start probe executor engine
	if config.Executor.Enabled {
		executor = NewExecutorEngine(provider)
		log.Infof("Starting with configuration %s", config.String())
		executor.Start(config.Selectors, model.RunMode(strings.ToUpper(config.Node.Mode)))
	}

	// Start Hello engine
	if config.HelloService.Enabled {
		helloEngine = NewHelloEngine(provider, config.Selectors, executor)
		log.Infof("Starting hello engine")
		helloEngine.Start()
	}

	// Start API
	if config.API.Enabled {
		api(executor, provider, config)
	}

	// Wait sign term for close agent
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()
	<-done
	fmt.Println("Stopping node.")
}

func api(executor *ExecutorEngine, provider *Provider, config *Config) {

	router := mux.NewRouter()

	endpoint.RegisterSentinel(router, provider, executor)

	handler := cors.AllowAll().Handler(router)
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(config.API.Port), handler))

}
