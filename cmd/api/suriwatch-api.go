package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/suriwatch/suriwatch/pkg/client/model"
	. "gitlab.com/suriwatch/suriwatch/pkg/config"
	"gitlab.com/suriwatch/suriwatch/pkg/dao"
	"gitlab.com/suriwatch/suriwatch/pkg/endpoint"
	versionInfo "gitlab.com/suriwatch/suriwatch/pkg/version"
)

var (
	// Configuration for API
	config *Config = Get()

	// Provide an instance of DAO interface.
	// Resolve instance of DAO according the configuration
	provider *dao.Provider
)

func usage() {
	fmt.Fprintf(os.Stderr, "usage: suriwatch-api -config=[string]\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func initAppInfos() {

	var config = Get()
	config.AppInfo = model.AppInfo{}

	config.AppInfo.Type = "api"
	config.AppInfo.Started = time.Now()
	config.AppInfo.Version = versionInfo.GetVersion()
	if config.Node.Name != "" {
		config.AppInfo.Name = config.Node.Name
	} else {
		config.AppInfo.Name, _ = os.Hostname()
	}
}

func initLogger() {
	// Log as JSON instead of the default ASCII formatter.
	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	Formatter.DisableColors = true
	log.SetFormatter(Formatter)

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)

}

func main() {

	initLogger()
	log.Infof("Starting suriwatch API..")

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	var configFile = flag.String("config", dir+"/config.toml", "TOML Config file path")
	flag.Parse()

	var config = Get()
	config.Read(*configFile)

	initAppInfos()

	provider := &dao.Provider{Config: config}
	registerAPI(provider)

	log.Infof("Suriwatch API running")

}

// Register the REST API resources
func registerAPI(provider *dao.Provider) {

	log.Infof("Starting api on port " + strconv.Itoa(config.API.Port))
	router := mux.NewRouter()

	endpoint.RegisterAPI(router, provider)

	handler := cors.AllowAll().Handler(router)
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(config.API.Port), handler))

}
